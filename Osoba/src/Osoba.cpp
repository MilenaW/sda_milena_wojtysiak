//============================================================================
// Name        : Osoba.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Data.cpp"

using namespace std;
enum Plec
{
	kobieta, mezczyzna, nieznana
};

class Osoba
{
private:
	string mImie;
	string mNazwisko;
	unsigned int* mPesel;
	Plec mPlec;

public:
	Data mData;



	Osoba() :
	mImie("imie nieznane"), mNazwisko("nazwisko nieznane"), mPesel(0),mData(0,0,0), mPlec(nieznana)
	{

	}

	Osoba(string imie, string nazwisko, unsigned int* pesel, Plec plec) :
			mNazwisko(nazwisko), mImie(imie), mPesel(pesel), mPlec(plec)
	{
		setData();
	}

	const Data& getDataUrodzenia() const
	{
		return mData;
	}

	void setData()
	{
		if (mPesel != 0)

		{
			mData = Data(mPesel[4] * 10 + mPesel[5], mPesel[2] * 10 + mPesel[3],
					1900 + (mPesel[0] * 10 + mPesel[1]));
		}
		else
			mData = Data(1, 1, 1900);
	}

	const string& getImie() const
	{
		return mImie;
	}

	void setImie(const string& imie)
	{
		mImie = imie;
	}

	const string& getNazwisko() const
	{
		return mNazwisko;
	}

	void setNazwisko(const string& nazwisko)
	{
		mNazwisko = nazwisko;
	}

	Plec getPlec() const
	{
		return mPlec;
	}

	void setPlec(Plec plec)
	{
		this->mPlec = plec;
	}


	unsigned int* getPesel() const
	{
		return mPesel;
	}

	void setPesel(unsigned int* pesel)
	{
		mPesel = pesel;
	}
	void wypiszOsoba()
	{
		cout << getImie() << "\n" << getNazwisko() << "\n";
		for (int i = 0; i < 11; ++i)
		{
			cout << mPesel[i];
		}
		cout<<endl;
		cout<<getPeselStr()<<endl;
		cout << "\n";
		mData.wypiszData();
		cout << getPlec();
	}
	string getPeselStr()
	{
		if (mPesel !=0)
		{	string tmp;
		for (int i = 0; i < 11; ++i)
			{
				tmp+='0' + mPesel[i];
			}
		return tmp;
		}

	}
	;
};

int main()
{
	unsigned int psl[11] =
	{ 9, 1, 1, 2, 1, 2, 7, 8, 9, 10, 11 };
	Osoba osoba("mietek", "miecinski", psl, mezczyzna);
	osoba.wypiszOsoba();

	return 0;
}
