#include <iostream>
#include <string>

const std::string RPN[] =
{ "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -",
		"1 1 + 2 + 3 1 1 + 7 - 9 / * -" };
const int wyniki[] =
{ 1, 4, 14, 4 };

//struct Stos
//{
//	int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisać nowy element
//	int dane[10];
//};

int wylicz(const std::string wyrazenie)
{
//	Stos liczby;
//	liczby.wierzcholek = 0;

	int stos[15];
	int wierzcholek = 0;

	int wynik = 0;
	for (unsigned i = 0; i < wyrazenie.length(); ++i)
	{

		char znak = wyrazenie[i];
		if (znak == ' ')
		{
			continue;
		}

		if (znak >= '0' && znak <= '9')
		{
			stos[wierzcholek] = znak - '0';
			wierzcholek += 1;
		}

		else
		{
			int plytsza, glebsza;

			switch (znak)
			{
			case '+':
				plytsza = stos[wierzcholek - 1];
				wierzcholek -= 1;
				glebsza = stos[wierzcholek - 1];
				wierzcholek -= 1;
				wynik = plytsza + glebsza;
				stos[wierzcholek] = wynik;
				wierzcholek += 1;
				break;

			case '-':
				stos[wierzcholek - 2] = stos[wierzcholek - 2]
						- stos[wierzcholek - 1];
				wierzcholek -= 1;
				break;

			case '*':
				stos[wierzcholek - 2] = stos[wierzcholek - 2]
						* stos[wierzcholek - 1];
				wierzcholek -= 1;
				break;

			case '/':
				stos[wierzcholek - 2] = stos[wierzcholek - 2]
						/ stos[wierzcholek - 1];
				wierzcholek -= 1;
				break;
			}

//			if (wyrazenie[i] == '+')
//			{
//
//				int zdjete = liczby.dane[liczby.wierzcholek]; // odczytaj dane z wierzchu stosu
//				liczby.wierzcholek -= 1; // przesuwam wierzchołek na pierwszy istniejący element
//				int zdjete2 = liczby.dane[liczby.wierzcholek]; // odczytaj dane z wierzchu stosu
//				liczby.wierzcholek -= 1; // przesuwam wierzchołek na pierwszy istniejący element
//				wynik = zdjete + zdjete2;
//				liczby.dane[liczby.wierzcholek] = wynik;
//				return liczby.wierzcholek;
//
//			}
//			{
//				liczby.dane[liczby.wierzcholek] = (wyrazenie[i]) - '0';
//				liczby.wierzcholek += 1;
//
//			}
		}
	}
	return stos[0];
}

void sprawdz(const std::string rpn, int oczekiwanyWynik)
{
	int wynik = wylicz(rpn);
	if (wynik == oczekiwanyWynik)
	{
		std::cout << "Cacy ";
	}
	else
	{
		std::cout << "Be   ";
	}
	std::cout << rpn << " => " << wynik << std::endl;
}

int main(int argc, char* argv[])
{
	for (int i = 0; i < 4; ++i)
	{
		sprawdz(RPN[i], wyniki[i]);
	}

	return 0;
}
