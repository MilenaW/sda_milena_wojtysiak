/*
 * Wektor.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef WEKTOR_HPP_
#define WEKTOR_HPP_
#include <iostream>
#include "Punkt.hpp"

class Wektor
{
private:
	Punkt mP1;
	Punkt mP2;
public:
	Wektor();
	Wektor(Punkt p1, Punkt p2);
	virtual ~Wektor();
	void wypisz();
	void operator!();
	bool operator==(const Wektor&w);
	const Punkt& getP1() const;
	void setP1(const Punkt& p1);
	const Punkt& getP2() const;
	void setP2(const Punkt& p2);
};

#endif /* WEKTOR_HPP_ */
