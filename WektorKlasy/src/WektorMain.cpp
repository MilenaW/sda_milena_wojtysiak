//============================================================================
// Name        : Wektor.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Punkt.hpp"
#include "Wektor.hpp"
using namespace std;

int main()
{
	Wektor w1(Punkt(3, 4), Punkt(6, 6));
	Wektor w2(Punkt(-4, 10), Punkt(8, -7));

	w1.wypisz();
	w2.wypisz();

	Punkt pkt(3, 6);
	Wektor w3(pkt, pkt * 3);
	w3.wypisz();

	if (w1 == w2)
	{
		cout << "sa rowne" << endl;
	}
	else
	{
		cout << "jednak nie sa rowne" << endl;
	}

	Wektor w4(Punkt(3, 4), Punkt(6, 6));

	if (w1 == w4)
	{
		cout << "sa rowne" << endl;
	}
	else
	{
		cout << "jednak nie sa rowne" << endl;
	}
	return 0;
}
