/*
 * Wektor.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Wektor.hpp"

Wektor::Wektor()
{
	// TODO Auto-generated constructor stub

}

Wektor::Wektor(Punkt p1, Punkt p2) :
		mP1(p1), mP2(p2)
{
	// TODO Auto-generated constructor stub

}

Wektor::~Wektor()
{
	// TODO Auto-generated destructor stub
}

void Wektor::wypisz()
{
	std::cout << "P1.x = " << mP1.getX() << " P1.y = " << mP1.getY()
			<< " P2.x = " << mP2.getX() << " P2.y = " << mP2.getY()
			<< std::endl;
}

const Punkt& Wektor::getP1() const
{
	return mP1;
}

void Wektor::setP1(const Punkt& p1)
{
	mP1 = p1;
}

const Punkt& Wektor::getP2() const
{
	return mP2;
}

void Wektor::setP2(const Punkt& p2)
{
	mP2 = p2;
}

void Wektor::operator!()
{
	Punkt pomocniczy = mP1;
	mP1 = mP2;
	mP2 = pomocniczy;
}

bool Wektor::operator==(const Wektor&w2)
{
	if (w2.getP1() == this->getP1() && w2.getP2() == this->getP2())
	{
		return true;
	}
	else
	{
		return false;
	}
}
