/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
private:
	int mX;
	int mY;

public:
	Punkt();
	Punkt(int x, int y);
	virtual ~Punkt();
	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
//	_zwracany_typ_ operator *( const _typ_ & );
	Punkt operator *(const int multi);
	bool operator==(const Punkt&pkt) const;
};

//_zwracany_typ_ operator *( const _typ1_ &, const _typ2_ & );
Punkt operator *(const int multi, Punkt &pkt);
#endif /* PUNKT_HPP_ */
