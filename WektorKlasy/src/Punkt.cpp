/*
 * Punkt.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Punkt.hpp"

Punkt::Punkt()
{
	// TODO Auto-generated constructor stub

}

int Punkt::getX() const
{
	return mX;
}

void Punkt::setX(int x)
{
	mX = x;
}

int Punkt::getY() const
{
	return mY;
}

void Punkt::setY(int y)
{
	mY = y;
}

Punkt::~Punkt()
{
	// TODO Auto-generated destructor stub
}

Punkt::Punkt(int x, int y) :
		mX(x), mY(y)
{

}

Punkt Punkt::operator*(const int multi)
{
	Punkt wynik(0, 0);
	wynik.setX(mX * multi);
	wynik.setY(mY * multi);
	return wynik;
}

Punkt operator *(const int multi, Punkt pkt)
{
	return pkt * multi;
}
bool Punkt::operator==(const Punkt&pkt) const
{
	if (pkt.getX() == this->getX() && pkt.getY() == this->getY())
	{
		return true;
	}
	else
	{
		return false;
	}
}
