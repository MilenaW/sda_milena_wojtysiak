/*
 * Hour.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef HOUR_HPP_
#define HOUR_HPP_
#include <iostream>
class Hour
{
public:
	Hour(int h);
	virtual ~Hour();
	int mHour;
	int mMin;
	bool operator< (const Hour &hour) const;
	friend std::ostream& operator <<(std::ostream& stream, const Hour& hour);
};

#endif /* HOUR_HPP_ */
