/*
 * Weekday.cpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#include "Weekday.hpp"

Weekday::Weekday(int d)
{

	switch(d)
		{
		case 0:
			mDay=weekdayNotIndicated;
			break;
		case 1:
			mDay=PN;
			break;
		case 2:
			mDay=WT;
			break;
		case 3:
			mDay=SR;
			break;
		case 4:
			mDay=CZW;
			break;
		case 5:
			mDay=PT;
			break;
		case 6:
			mDay=SB;
			break;
		case 7:
			mDay=ND;
			break;
		}
}

Weekday::~Weekday()
{
	// TODO Auto-generated destructor stub
}

bool Weekday::operator< (const Weekday &weekday) const
{
    return Weekday::mDay < weekday.Weekday::mDay;
}
bool Weekday::operator== (const Weekday &weekday) const
{
    return Weekday::mDay == weekday.Weekday::mDay;
}
bool Weekday::operator!= (const Weekday &weekday) const
{
    return Weekday::mDay != weekday.Weekday::mDay;
}

std::ostream& operator <<(std::ostream& stream, const Weekday& weekday)
{

	switch(weekday.Weekday::mDay)
	{
	case Weekday::weekdayNotIndicated:
		stream<<"Not Indicated";
		break;
	case Weekday::PN:
		stream<<"PN";
		break;
	case Weekday::WT:
		stream<<"WT";
		break;
	case Weekday::SR:
		stream<<"SR";
		break;
	case Weekday::CZW:
		stream<<"CZW";
		break;
	case Weekday::PT:
		stream<<"PT";
		break;
	case Weekday::SB:
		stream<<"SB";
		break;
	case Weekday::ND:
		stream<<"ND";
		break;
	}
	return stream;
}
