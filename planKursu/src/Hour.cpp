/*
 * Hour.cpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#include "Hour.hpp"
#include <iostream>
Hour::Hour(int h)
{
	mHour=h/100;
	mMin=h%100;

}

Hour::~Hour()
{
	// TODO Auto-generated destructor stub
}

bool Hour::operator< (const Hour &hour) const
{
    return mHour < hour.mHour;
}


std::ostream& operator <<(std::ostream& stream, const Hour& hour)
{
	stream<<hour.mHour<<":"<<hour.mMin;
	return stream;
}
