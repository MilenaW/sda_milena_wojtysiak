/*
 * Kurs.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef KURS_HPP_
#define KURS_HPP_

#include <iostream>
#include "Hour.hpp"
#include "Weekday.hpp"


using namespace std;


class Kurs
{
public:
	Kurs(string name, int weekday,int start, int finish, int classroom);
	virtual ~Kurs();
	string mName;
	Weekday mWeekday;
	Hour mStartHour;
	Hour mFinishHour;
	int mClassroom;
	bool operator== (const string name) const;



};

#endif /* KURS_HPP_ */
