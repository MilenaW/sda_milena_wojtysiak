/*
 * Weekday.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef WEEKDAY_HPP_
#define WEEKDAY_HPP_
#include <iostream>
class Weekday
{
public:
	Weekday(int day);
	virtual ~Weekday();
	enum day
	{
		weekdayNotIndicated,
		PN,
		WT,
		SR,
		CZW,
		PT,
		SB,
		ND
	};
	day mDay;
	bool operator< (const Weekday &weekday) const;
	bool operator== (const Weekday &weekday) const;
	bool operator!= (const Weekday &weekday) const;
	friend std::ostream& operator <<(std::ostream& stream, const Weekday& weekday);
};

#endif /* WEEKDAY_HPP_ */
