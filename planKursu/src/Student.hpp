/*
 * Student.hpp
 *
 *  Created on: 30.05.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include <iostream>
#include <vector>
#include "Kurs.hpp"
class Student
{
public:
	Student(string name, int classAmount, vector<string> schedule);
	virtual ~Student();
	string mName;
	int mClassAmount;
	vector<string> mSchedule;


};

#endif /* STUDENT_HPP_ */
