//============================================================================
// Name        : planKursu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include "Kurs.hpp"
#include "Student.hpp"

using namespace std;

bool sortWeekday(Kurs first, Kurs second)
{
	if(first.mWeekday < second.mWeekday)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool sortHour(Kurs first, Kurs second)
{
	if(first.mStartHour < second.mStartHour)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main() {

    vector<Kurs> plan; //vector przechowujacy dane z pliku

    ifstream ifs; // obiekt streamu wczytujace z pliku
    ifs.open("planKursuZad15.txt"); //otwarcie pliku

    if (!ifs.good()) //sprawdzenie czy plik jest poprawny
    {
        cerr << "Error reading file"<<endl; //komunikat o bledzie
        return 1;
    }

	string name;
	int weekday;
	int start;
	int finish;
	int classroom;

    while(!ifs.eof())
    {
        ifs>>name>>weekday>>start>>finish>>classroom; //wczytawanie operatorem >>
        plan.push_back(Kurs(name,weekday,start,finish,classroom)); // wrzucenie do kontenera
    }

    ifs.close(); // zamkniecie pliku

    ifs.open("students.txt"); //otwarcie pliku

    if (!ifs.good()) //sprawdzenie czy plik jest poprawny
    {
        cerr << "Error reading file"<<endl; //komunikat o bledzie
        return 1;
    }

    for(vector<Kurs>::iterator it = plan.begin(); it!= plan.end(); ++it)
    {
        cout<<it->mName<<" - "<<it->mWeekday<<" - "<<it->mStartHour<<" - "<<it->mFinishHour<<" - "<< it->mClassroom<<endl;
    }

    cout<<endl;

    int classAmount;
    vector<string> schedule;
    vector<Student> students;
    string tmp;

    while(!ifs.eof())
    {

        ifs>>name>>classAmount; //wczytawanie operatorem >>

        for(int i =0; i<classAmount;i++)
        {
        	ifs>>tmp;
        	schedule.push_back(tmp);
        }
        students.push_back(Student(name,classAmount,schedule)); // wrzucenie do kontenera
    }


    ifs.close();



    for(vector<Student>::iterator it = students.begin(); it!= students.end(); ++it)
    {
        cout<<it->mName<<" - "<<it->mClassAmount<<" - ";
        for (int i = 0; i<it->mClassAmount;i++)
        {
        	cout<<it->mSchedule[i]<<" - ";
        }
        cout<<endl;
    }

    cout<<endl;

    sort(plan.begin(),plan.end(), sortHour);
    sort(plan.begin(),plan.end(), sortWeekday);

    for(vector<Kurs>::iterator it = plan.begin(); it!= plan.end(); ++it)
    {
        cout<<it->mName<<" - "<<it->mWeekday<<" - "<<it->mStartHour<<" - "<<it->mFinishHour<<" - "<< it->mClassroom<<endl;
    }

    cout<<endl;

    for(vector<Kurs>::iterator it = plan.begin(); it!= plan.end(); ++it)
    {
    	if(it->mWeekday!=(it+1)->mWeekday)
    	{
    		continue;
    	}
    	if(it->mWeekday==(it+1)->mWeekday)
    	{
    		if(it->mClassroom==(it+1)->mClassroom)
    		{
    			if((it+1)->mStartHour<it->mFinishHour)
    			{
    				cout<<"Conflict "<<endl;
    				cout<<it->mName<<" - "<<it->mWeekday<<" - "<<it->mStartHour<<" - "<<it->mFinishHour<<" - "<< it->mClassroom<<endl;
    				cout<<(it+1)->mName<<" - "<<(it+1)->mWeekday<<" - "<<(it+1)->mStartHour<<" - "<<(it+1)->mFinishHour<<" - "<< it->mClassroom<<endl;
    			}
    		}
    	}
    }


    	for(vector<Student>::iterator itt = students.begin(); itt!= students.end(); ++itt)
    	{
    		for(vector<string>::iterator its = itt->mSchedule.begin(); its!= itt->mSchedule.end(); ++its)
    		{
    			vector<Kurs>::iterator it;
    			it = find (plan.begin(), plan.end(), *its);

    			for (vector<string>::iterator itsch = its+1; itsch!=itt->mSchedule.end();++itsch)
    			{

        			vector<Kurs>::iterator it2;
        			it2 = find (plan.begin(), plan.end(), *itsch);



        			if(it->mWeekday!=it2->mWeekday)
        			{
        				continue;
        			}
        			if(it->mWeekday==it2->mWeekday)
        			{

    	    			if(it2->mStartHour<it->mFinishHour)
    	    			{
    	    				cout<<"Conflict "<<endl;
    	    				cout<<itt->mName<<endl;
    	    				cout<<it->mName<<" - "<<it->mWeekday<<" - "<<it->mStartHour<<" - "<<it->mFinishHour<<" - "<< it->mClassroom<<endl;
    	    				cout<<it2->mName<<" - "<<it2->mWeekday<<" - "<<it2->mStartHour<<" - "<<it2->mFinishHour<<" - "<< it2->mClassroom<<endl;

    	    			}

    	    	}
    			}

//    			  if (it != plan.end())
//    			    {
//    				  cout<<*its<<endl;
//    			    }
//    			  else
//    			    std::cout << "Element not found in myvector\n";
    		}
    	}



	return 0;
}
