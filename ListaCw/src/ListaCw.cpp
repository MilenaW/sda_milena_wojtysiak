//============================================================================
// Name        : ListaCw.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <list>
using namespace std;

void printIntegerPart(float x)
{
	cout<<static_cast<int>(x)<<" ";
}

void print(float x)
{
	cout<<x<<" ";
}

void castIntegerPart(float& x)
{
	x=static_cast<int>(x);
}

struct Sum
{
	Sum():sum(0){}
	float sum;
	void operator()(float x)
	{
		sum+=x;
	}

};
int main() {

	list<float> num;
	num.push_back(1.1);
	num.push_back(2.1);
	num.push_back(3.1);
	num.push_back(1.4);
	num.push_back(1.5);
	num.push_back(1.6);
	for_each(num.begin(), num.end(), print);
	cout<<endl;
	for_each(num.begin(), num.end(), printIntegerPart);
	cout<<endl;
	for_each(num.begin(), num.end(), print);
	for_each(num.begin(), num.end(), castIntegerPart);
	cout<<endl;
	for_each(num.begin(), num.end(), print);
	cout<<endl;

	Sum suma = for_each(num.begin(), num.end(), Sum());
//dwa sposobu przekazania, jako funkcja gora, dol jako obiekt
	Sum suma2;
	for_each(num.begin(), num.end(), suma2);

	cout<<"suma "<<suma.sum;

	return 0;
}
