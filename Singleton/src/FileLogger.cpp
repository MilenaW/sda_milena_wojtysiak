/*
 * FileLogger.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "FileLogger.hpp"


FileLogger* FileLogger::mInstance = 0;
FileLogger::FileLogger()
{
	mFile.open("log.txt");

}


void FileLogger::log(string dataToLog)
{
	time_t t;
	t = time(NULL);
	struct tm* current = localtime(&t);

	mFile<<"[ "<<current->tm_year +1900<<"-"
			<<current->tm_mon+1<<"-"
			<<current->tm_mday<<" "
			<<current->tm_hour<<":"
			<<current->tm_min<<":"
			<<current->tm_sec<<" ] "<<dataToLog<<endl;
}
