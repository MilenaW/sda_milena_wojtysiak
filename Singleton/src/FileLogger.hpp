/*
 * FileLogger.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef FILELOGGER_HPP_
#define FILELOGGER_HPP_

#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;


class FileLogger
{
private:
	ofstream mFile;
	static FileLogger* mInstance;
	FileLogger();

public:
	void log(string dataToLog);
	static FileLogger* getInstance()
	{
	    if (!mInstance)
	      mInstance = new FileLogger;
	    return mInstance;
	}


};

#endif /* FILELOGGER_HPP_ */
