//============================================================================
// Name        : Singleton.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "FileLogger.hpp"
#include "Kalk.hpp"

using namespace std;

int main()
{

	FileLogger::getInstance()->log("terefere");
	FileLogger::getInstance()->log("kuku");

	Kalk kalk;
	kalk.podziel(10, 2);
	kalk.podziel(2, 0);

	return 0;
}
