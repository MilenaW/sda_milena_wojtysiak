/*
 * Kalk.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALK_HPP_
#define KALK_HPP_
#include "FileLogger.hpp"
class Kalk
{
public:
	Kalk();
	virtual ~Kalk();
	int podziel (int a, int b)
	{

		FileLogger::getInstance()->log ("podziel ");

		if (b==0)
		{
			FileLogger::getInstance()->log("Nie dziel przez 0!!!");
			return 0;
		}
		else
		{
			return a/b;
		}
	}
};

#endif /* KALK_HPP_ */
