/*
 * SingletonC.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SINGLETONC_HPP_
#define SINGLETONC_HPP_

class SingletonC
{
private:
	SingletonC();

public:

	virtual ~SingletonC();
};

#endif /* SINGLETONC_HPP_ */
