/*
 * CalculatorTest.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Calculator.hpp"

TEST(CalculatorTest, AddTest) {
      Calculator c;
      EXPECT_EQ(4, c.add(2,2));
      EXPECT_EQ(1, c.add(0,1));
      EXPECT_EQ(0, c.add(-2,2));
      EXPECT_EQ(-4, c.add(-2,-2));
}

TEST(CalculatorTest, SubstrTest) {
      Calculator c;
      EXPECT_EQ(4, c.substr(8,4));
      EXPECT_EQ(0, c.substr(2,2));
      EXPECT_EQ(-1, c.substr(5,6));
      EXPECT_EQ(-4, c.substr(-8,-4));
}

TEST(CalculatorTest, Add2Test) {
      Calculator c(2,3);
      EXPECT_EQ(0, c.getResult());

}

TEST(CalculatorTest, Add3Test) {
      Calculator c(2,3);
      EXPECT_EQ(5, c.add());
      EXPECT_EQ(5, c.getResult());

}

TEST(CalculatorTest, Add4Test) {
      Calculator c(2,3);
      c.add();
      EXPECT_EQ(5, c.getResult());

}

TEST(CalculatorTest, Add5Test) {
      Calculator c(-2,-3);
      EXPECT_EQ(-5, c.add());
      EXPECT_EQ(-5, c.getResult());

}
TEST(CalculatorTest, substr2Test) {
      Calculator c(6,3);
      c.substr();
      EXPECT_EQ(3, c.getResult());

}
TEST(CalculatorTest, substr3Test) {
      Calculator c(-6,3);
      c.substr();
      EXPECT_EQ(-9, c.getResult());

}


TEST(CalculatorTest, powTest) {
      Calculator c;
      EXPECT_EQ(4, c.pow(2,2));
      EXPECT_EQ(1, c.pow(2,0));
      EXPECT_EQ(5, c.pow(5,1));
      EXPECT_EQ(6.25, c.pow(2.5,2));
}

int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}



