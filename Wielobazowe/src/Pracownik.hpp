/*
 * Pracownik.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACOWNIK_HPP_
#define PRACOWNIK_HPP_
#include <string>
#include <iostream>
#include "Osoba.hpp"

class CPracownik: public virtual COsoba
{
protected:
	int mPensja;
	std::string mZawod;

public:
	CPracownik (int pensja, std::string zawod, long pesel);
	CPracownik (int pensja, std::string zawod);
	void virtual pracuj();
};




#endif /* PRACOWNIK_HPP_ */
