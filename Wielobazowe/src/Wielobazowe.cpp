//============================================================================
// Name        : Wielobazowe.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Student.hpp"
#include "Pracownik.hpp"
#include "PracStud.hpp"
#include "Osoba.hpp"

using namespace std;

int main()
{
	CPracStud mietek(1234, "lans", "kelner", 1600, 12345);
	CPracownik pracownik (1200, "korpoludek", 901234567);
	CStudent student  (123,"filolog", 12333333);

	cout<<"Pracownik pesel "<<pracownik.podajPesel()<<endl;
	cout<<"Student pesel "<<student.podajPesel()<<endl;
	cout<<"Mietek pesel "<<mietek.podajPesel()<<endl;

	cout<<"-------------------------------"<<endl;

	COsoba osoba(990418044);
	cout << osoba.podajPesel() << endl;
	mietek.pracuj();
	return 0;
}
