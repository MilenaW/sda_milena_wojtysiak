/*
 * Student.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include <iostream>
#include <string>
#include "Osoba.hpp"
class CStudent: public virtual COsoba
{

protected:
	int mNrIndeksu;
	std::string mKierunek;

public:
	int podajNumer();
	void virtual uczSie();
	CStudent (int nrIndeksu, std::string kierunek, long pesel);
	CStudent (int nrIndeksu, std::string kierunek);
	virtual ~CStudent();
};


#endif /* STUDENT_HPP_ */
