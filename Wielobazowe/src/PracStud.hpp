/*
 * PracStud.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACSTUD_HPP_
#define PRACSTUD_HPP_
#include "Pracownik.hpp"
#include "PracStud.hpp"
#include "Osoba.hpp"
#include "Student.hpp"

#include <iostream>
#include <string>

using namespace std;

class CPracStud: public CStudent , public CPracownik
{
public:
	CPracStud(int nrIndeksu, std::string kierunek, std::string zawod, int pensja, long pesel);

	void uczSie();

	void pracuj();
};




#endif /* PRACSTUD_HPP_ */
