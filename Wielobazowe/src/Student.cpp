/*
 * Student.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include <iostream>
#include <string>
#include "Student.hpp"
using namespace std;

int CStudent::podajNumer()
{
	return mNrIndeksu;
}

void CStudent::uczSie()
{
	cout<<"ucze sie"<<endl;
}

CStudent::CStudent (int nrIndeksu, string kierunek, long pesel)
: COsoba(pesel)
, mNrIndeksu (nrIndeksu)
, mKierunek(kierunek)
{
};
CStudent::CStudent (int nrIndeksu, string kierunek)
: mNrIndeksu (nrIndeksu)
, mKierunek(kierunek)
{
};
CStudent::~CStudent()
{
};
