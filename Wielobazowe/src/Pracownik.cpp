/*
 * Pracownik.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include <iostream>
#include <string>
#include "Pracownik.hpp"
using namespace std;

void CPracownik::pracuj(){
	cout<<"pracuje"<<endl;
}

CPracownik::CPracownik (int pensja, string zawod, long pesel)
: COsoba(pesel)
, mPensja(pensja)
, mZawod(zawod)
{
};
CPracownik::CPracownik (int pensja, string zawod)
: mPensja(pensja)
, mZawod(zawod)
{
};
