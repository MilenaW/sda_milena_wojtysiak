/*
 * PracStud.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include "Pracownik.hpp"
#include "Student.hpp"
#include "PracStud.hpp"
#include "Osoba.hpp"

#include <iostream>
#include <string>

using namespace std;

CPracStud::CPracStud(int nrIndeksu, std::string kierunek, std::string zawod, int pensja, long pesel)
: COsoba(pesel)
, CStudent (nrIndeksu, kierunek)
, CPracownik(pensja, zawod)
{
}
void uczSie()
{
	cout << "trudno sie uczyc, jak sie pracuje..." << endl;
}

void pracuj()
{
	cout << "trudno pracowac, gdy sie studiuje.." << endl;
}


