/*
 * LiczbaNieujemna.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef LICZBANIEUJEMNA_HPP_
#define LICZBANIEUJEMNA_HPP_
#include <string>
#include <iostream>
#include <exception>
#include "BladKonstrukcji.hpp"
#include "ZleArgumenty.hpp"
class LiczbaNieujemna
{
private:
	int mValue;
public:
	LiczbaNieujemna();
	LiczbaNieujemna(int value);
	virtual ~LiczbaNieujemna();
	unsigned int getValue() const;
	void setValue(unsigned int value);
	LiczbaNieujemna operator- (const int minus) const;
	LiczbaNieujemna operator- (LiczbaNieujemna& druga) const;
	LiczbaNieujemna operator/ (const int dziel) const;
	LiczbaNieujemna operator/ (LiczbaNieujemna& druga) const;
	LiczbaNieujemna operator* (const int razy) const;
	LiczbaNieujemna operator* (LiczbaNieujemna& druga) const;
 	friend std::ostream& operator<< (std::ostream& stream, const LiczbaNieujemna& liczba);




};
//std::ostream& operator<< (std::ostream& stream, const LiczbaNieujemna& liczba)
//{
//	stream << liczba.mValue;
////	liczba.wypisz(stream);
//	return stream;
//}
#endif /* LICZBANIEUJEMNA_HPP_ */
