/*
 * ZleArgumenty.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef ZLEARGUMENTY_HPP_
#define ZLEARGUMENTY_HPP_

#include <exception>

class ZleArgumenty: public std::exception
{
public:
	ZleArgumenty();

	virtual const char* what () const throw();
};

#endif /* ZLEARGUMENTY_HPP_ */
