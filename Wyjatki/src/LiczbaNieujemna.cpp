/*
 * LiczbaNieujemna.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#include "LiczbaNieujemna.hpp"


using namespace std;

LiczbaNieujemna::LiczbaNieujemna()
:mValue(0)
{

}

LiczbaNieujemna::LiczbaNieujemna(int value)

{
	if (value<0)
	{
		throw BladKonstrukcji(value);

	}
mValue = value;
}

unsigned int LiczbaNieujemna::getValue() const
{
	return mValue;
}

void LiczbaNieujemna::setValue(unsigned int value)
{
	mValue = value;
}

LiczbaNieujemna::~LiczbaNieujemna()
{
	// TODO Auto-generated destructor stub
}

LiczbaNieujemna LiczbaNieujemna::operator- (const int minus) const
{
	if (minus>mValue)
	{
		throw invalid_argument ( "Pamietaj cholero odjemnik nie moze byc wiekszy od odjemnej!!!");

	}
	else
	{
		return mValue - minus;
	}
}

LiczbaNieujemna LiczbaNieujemna::operator- (LiczbaNieujemna& druga) const
{
	if (druga.getValue()>this->getValue())
	{
		throw ZleArgumenty();
	}
	else
	{
		return LiczbaNieujemna(this->getValue()-druga.getValue());
	}
}

LiczbaNieujemna LiczbaNieujemna::operator/ (const int dziel) const
{
	if (dziel==0)
	{
		throw ZleArgumenty();

	}
	else
	{
		return mValue/dziel;
	}
}

LiczbaNieujemna LiczbaNieujemna::operator/ (LiczbaNieujemna& druga) const
{
	if (druga.getValue()==0)
	{
		throw ZleArgumenty();
	}
	else
	{
		return LiczbaNieujemna(this->getValue()/druga.getValue());
	}
}
LiczbaNieujemna LiczbaNieujemna::operator* (const int razy) const
{
	return mValue*razy;
}

LiczbaNieujemna LiczbaNieujemna::operator* (LiczbaNieujemna& druga) const
{
	return this->mValue*druga.getValue();
}


