/*
 * BladKonstrukcji.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef BLADKONSTRUKCJI_HPP_
#define BLADKONSTRUKCJI_HPP_

#include <exception>
#include <iostream>
#include <sstream>

class BladKonstrukcji: public std::exception
{
public:

	int blednaLiczba;
	
	BladKonstrukcji(int liczba);

	virtual const char* what () const throw();
	int getBlednaLiczba() const;
};

#endif /* BLADKONSTRUKCJI_HPP_ */
