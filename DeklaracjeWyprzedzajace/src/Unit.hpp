/*
 * Unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef UNIT_HPP_
#define UNIT_HPP_
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>


using namespace std;

class Group;
class Unit
{
private:
	string mId;
	Group* mGroupPtr;

public:
	Unit(string id);
	virtual ~Unit();
	void printId();
	void replicate();
	void addToGroup(Group* group);

};

#endif /* UNIT_HPP_ */
