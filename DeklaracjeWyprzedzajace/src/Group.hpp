/*
 * Group.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef GROUP_HPP_
#define GROUP_HPP_


class Unit;

class Group
{
private:
	Unit** mUnits;
	unsigned int mSize;
	void resize(int newSize);
public:

	Group();
	virtual ~Group();
	void add(Unit*unit);
	void clear();
	void replicateGroup();
	void printUnits();
	void resize();

};

#endif /* GROUP_HPP_ */
