/*
 * Group.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "Group.hpp"
#include "Unit.hpp"

Group::Group() :
		mUnits(0), mSize(0)
{
	// TODO Auto-generated constructor stub

}

Group::~Group()
{
	clear();
	// TODO Auto-generated destructor stub
}
void Group::resize()
{
	if (mSize == 0)
	{
		mSize++;
		mUnits = new Unit*[mSize];
	}
	else
	{
		mSize++;
		Unit** pomocnicza = new Unit*[mSize];
		for (unsigned int i = 0; i < mSize - 1; i++)
		{
			pomocnicza[i] = mUnits[i];
		}
		delete[] mUnits;
		mUnits = pomocnicza;

	}
}

void Group::add(Unit*unit)
{
	resize();
	mUnits[mSize - 1] = unit;

}
void Group::clear()
{
	for (int i = 0; i < mSize; i++)
	{
		delete mUnits[i];
	}
	delete[] mUnits;
	mSize = 0;
	mUnits = 0;
}
void Group::replicateGroup()
{
	unsigned int currentSize = mSize;
	for (int i = 0; i < currentSize; i++)
	{
		mUnits[i]->replicate();
	}
}
void Group::printUnits()
{
	for (int i = 0; i < mSize; i++)
	{
		mUnits[i]->printId();
	}
}
