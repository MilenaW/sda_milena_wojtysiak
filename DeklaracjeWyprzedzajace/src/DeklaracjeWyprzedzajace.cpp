//============================================================================
// Name        : DeklaracjeWyprzedzajace.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Unit.hpp"
#include "Group.hpp"
using namespace std;

int main()
{
	srand(time( NULL));
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	Group g;
	Unit* u = new Unit("5");

	u->addToGroup(&g);
	u->printId();
	u->replicate();
	g.printUnits();
	g.replicateGroup();
	g.printUnits();
	return 0;
}
