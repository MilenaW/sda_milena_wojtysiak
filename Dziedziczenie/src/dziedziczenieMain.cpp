//============================================================================
// Name        : dziedziczenieMain.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class CPoczatek
{
public:
	void przedstawSie()
	{
		cout << "Jestem Poczatek" << endl;

	}
	CPoczatek() :
			mX(0)
	{
		cout << "Jestem Konstruktor Poczatek" << endl;
		zmienX();
		wypiszX();
	}
	CPoczatek(int x) :
			mX(x)
	{
		cout << "Jestem KonstruktorXXX Poczatek" << endl;

		wypiszX();
	}
	~CPoczatek()
	{
		cout << "Jestem Destruktor Poczatek" << endl;
	}

	void zmienX()
	{
		mX = 1;
	}

	void wypiszX()
	{
		cout << mX << endl;
	}

protected:
	int mX;
};

class CSrodek: protected CPoczatek
{
public:
	void przedstawSie()
	{
		cout << "Jestem Srodek" << endl;

	}
	CSrodek()
	: CPoczatek(111111)
	{
		cout << "Jestem Konstruktor Srodek" << endl;
		zmienX();
		wypiszX();
	}
	CSrodek(int x)
	{
		mX = x;
		cout << "Jestem KonstruktorXXX Srodek" << endl;

		wypiszX();
	}

	~CSrodek()
	{
		cout << "Jestem Destruktor Srodek" << endl;
	}

	void zmienX()
	{
		CPoczatek::mX = 11;
		mX = 12;
	}
	void wypiszX()
	{
		cout << "Poczatek " << CPoczatek::mX << " Srodek " << mX << endl;
	}

protected:
	int mX;
};

class CKoniec: protected CSrodek
{
public:
	void przedstawSie()
	{
		cout << "Jestem Koniec" << endl;

	}
	CKoniec()
	{
		cout << "Jestem Konstruktor Koniec" << endl;
		zmienX();
		wypiszX();
	}
	CKoniec(int x)
	{
		mX = x;
		cout << "Jestem Konstruktor XXX Koniec" << endl;

		wypiszX();
	}

	~CKoniec()
	{
		cout << "Jestem Destruktor Koniec" << endl;
	}

	void zmienX()
	{

		CSrodek::mX = 4;
		CSrodek::CPoczatek::mX = 1904;
		mX = 123;
	}
	void wypiszX()
	{
		cout << "poczatek " << CPoczatek::mX << " Srodek "
				<< CSrodek::mX << " Koniec " << mX << endl;
	}

protected:
	int mX;
};

int main()
{
	CPoczatek poczatek;
	CSrodek srodek;
	CKoniec koniec;
	poczatek.wypiszX();
	CPoczatek poczatek2(999);
	CSrodek srodek2(888);
	CKoniec koniec2(777);
	return 0;
}
