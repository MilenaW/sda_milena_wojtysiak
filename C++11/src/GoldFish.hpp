/*
 * GoldFish.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef GOLDFISH_HPP_
#define GOLDFISH_HPP_

#include "Fish.hpp"

class GoldFish final: public Fish
{
public:
	GoldFish();
	virtual ~GoldFish();
	void swim(int speed) override;
};

#endif /* GOLDFISH_HPP_ */
