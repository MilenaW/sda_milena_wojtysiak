/*
 * KontoBankowe.hpp
 *
 *  Created on: 08.06.2017
 *      Author: RENT
 */

#ifndef KONTOBANKOWE_HPP_
#define KONTOBANKOWE_HPP_
#include <iostream>

class KontoBankowe
{
public:
	std::string mImie;
	float stanKonta;
	float oprocentowanie;

	KontoBankowe(std::string imie, float stan, float procent);
	KontoBankowe(std::string imie);
	KontoBankowe();
	virtual ~KontoBankowe();
};

#endif /* KONTOBANKOWE_HPP_ */
