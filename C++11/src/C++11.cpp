//============================================================================
// Name        : C++11.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <tuple>
#include <memory>
#include "Fish.hpp"
#include "GoldFish.hpp"
#include "Octopus.hpp"
#include "MaszynMnozaca.hpp"
#include "KontoBankowe.hpp"
#include "KontoBankoweVip.hpp"
#include "Kurczak.hpp"
#include "Produkt.hpp"

using namespace std;

#define POT(x) pow(2,x)

constexpr int power(int n)
{
	return pow(2,n);
}

constexpr int silnia(int x)
{
	return (x<=1)?1:x*silnia(x-1);
}

void print (int x)
{
	cout<<x<<" ";
}


enum class KolorKota
{
	Bialy,
	Czarny,
	Tricolor
};

enum class KolorPsa
{
	Bialy,
	Czarny
};

tuple<string,float,int>pobierzPacjenta(int id)
{
	if (id==1)
	{
		return make_tuple("Tomek", 177, 80);
	}
	else
	{
		return make_tuple("Kasia", 150, 55);
	}
}

void fun()
{
	Kurczak* ptr = new Kurczak;
	shared_ptr<Kurczak> ptru = make_shared<Kurczak>();
}


int main() {

fun();

shared_ptr<Produkt> ptr1 = make_shared<Produkt>();
shared_ptr<Produkt> ptr2 = make_shared<Produkt>(12, "sok");
shared_ptr<Produkt> ptr3 (new Produkt(12, "sok"));
cout<<ptr1->mCena<<ptr1->mNazwa<<endl;
cout<<ptr2->mCena<<ptr2->mNazwa<<endl;

shared_ptr<Kurczak> ptr  (new Kurczak());
shared_ptr<Kurczak> ptru = make_shared<Kurczak>();
vector<shared_ptr<Kurczak>> v;
v.push_back(ptr);



//auto krotka = pobierzPacjenta(2);
//
//cout<<get<0>(krotka)<<endl;





//KontoBankoweVip k;
//KontoBankowe kk;
//KontoBankowe kkk;
//k.bezKolejki();
//
//
//vector<int> v = {1,2,3};
//
//cout<<endl;
//
//vector<KontoBankowe> vk = {{"Kaziu", 12,11}, {"Zbyniu", 45,66} };
//for_each(vk.begin(), vk.end(), [](KontoBankowe& konto){cout<<konto.stanKonta;});
//
//
//KolorKota kolor = KolorKota::Bialy;
//
////
////for(int i = 0; i<=100; i++)
////{
////	v.push_back(i);
////}
////
////
//for_each(v.begin(), v.end(), print);
//
//cout<<endl;

//for_each(v.begin(), v.end(), [](int x){cout<<x;});
//
//cout<<endl;
//
//cout<<count_if(v.begin(), v.end(), [](int x){return x>4;})<<endl;
//
//cout<<endl;
//
//for_each(v.begin(), v.end(), [](int x){if(x<20)cout<<x<<" ";});
//cout<<endl;
//
//int dzielnik = 13;
//
//for_each(v.begin(), v.end(), [&dzielnik](int x){if(x%dzielnik==0)cout<<x<<" ";});
//cout<<endl;
//int sum = 0;
//
//for_each(v.begin(), v.end(), [&sum](int x){if(x%2==0)sum+=x;});
//
//cout<<sum;
//sort(v.begin(), v.end(), [](int x, int y){return x>y;});
//
//for_each(v.begin(), v.end(), [](int x){cout<<x;});
//cout<<endl;
//for (auto it = v.begin(); it!=v.end(); advance(it,2))
//{
//	cout<<*it<<endl;
//}

//for (auto& val : v)
//{
//	cout<<val<<endl;
//}
//
//float tab[10] = {1.1,2.2,3.3,4.4,5.5,6.6,7.7,8.8,9.9,10.1};
//
//for(int i = 0;i<10;i++)
//{
//	cout<<tab[i]<<" ";
//}
//cout<<endl;
//for (auto& val : tab)
//{
//	val=val*2;
//	cout<<val<<endl;
//}
//for(int i = 0;i<10;i++)
//{
//	cout<<tab[i]<<" ";
//}
//
//cout<<endl;
//auto lambda=[](){cout<<"hello world"<<endl;};
//lambda();
//auto lambda2=[](int x, int y){return x+y;};
//cout<<lambda2(2,5)<<endl;




	return 0;
}
