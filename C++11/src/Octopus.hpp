/*
 * Octopus.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef OCTOPUS_HPP_
#define OCTOPUS_HPP_

#include <iostream>

class Octopus
{
public:
	Octopus() = default;
	Octopus(const Octopus& ) = delete;
	Octopus& operator=(const Octopus&) = delete;
	virtual ~Octopus() = default;
	void swim()
	{
		std::cout<<"plummmmm"<<std::endl;
	}

};

#endif /* OCTOPUS_HPP_ */
