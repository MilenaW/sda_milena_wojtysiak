/*
 * KontoBankowe.cpp
 *
 *  Created on: 08.06.2017
 *      Author: RENT
 */

#include "KontoBankowe.hpp"

KontoBankowe::KontoBankowe(std::string imie, float stan, float procent)
: mImie(imie)
, stanKonta(stan)
, oprocentowanie(procent)
{

}
KontoBankowe::KontoBankowe(std::string imie)
:KontoBankowe(imie, 0,0)
{

}

KontoBankowe::KontoBankowe()
:KontoBankowe(" ", 0,0)
{

}

KontoBankowe::~KontoBankowe()
{
	// TODO Auto-generated destructor stub
}

