/*
 * MaszynMnozaca.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef MASZYNMNOZACA_HPP_
#define MASZYNMNOZACA_HPP_

#include <iostream>

class MaszynMnozaca
{
protected:
	float mX;

public:
	MaszynMnozaca(float x);
	virtual ~MaszynMnozaca();
	virtual float pomnoz(float x) final;
	float pomnoz(int) = delete;
};

#endif /* MASZYNMNOZACA_HPP_ */
