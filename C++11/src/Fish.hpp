/*
 * Fish.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef FISH_HPP_
#define FISH_HPP_
#include <iostream>
class Fish
{
public:
	Fish();
	virtual ~Fish();
	virtual void swim(int speed);
};

#endif /* FISH_HPP_ */
