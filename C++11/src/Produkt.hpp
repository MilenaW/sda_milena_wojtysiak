/*
 * Produkt.hpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */

#ifndef PRODUKT_HPP_
#define PRODUKT_HPP_

#include <iostream>

class Produkt
{
public:
	float mCena;
	std::string mNazwa;

	Produkt ()
	: mCena(0)
	, mNazwa("brak nazwy")
	{};


	Produkt (float cena, std::string nazwa)
	: mCena(cena)
	, mNazwa(nazwa)
	{};
	virtual ~Produkt();

	void setCena(float cena)
	{
		this->mCena = cena;
	}
};

//Produkt stworz(int klasa, std::string nazwa)
//{
//	switch (klasa)
//	{
//	case 1:
//		return Produkt(1000.99, nazwa);
//		break;
//	case 2:
//		return Produkt(500, nazwa);
//		break;
//	case 3:
//		return Produkt(19.99, nazwa);
//		break;
//	default:
//		return Produkt(0," ");
//
//	}
//}

#endif /* PRODUKT_HPP_ */
