/*
 * MaszynaMnozacoDodajaca.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef MASZYNAMNOZACODODAJACA_HPP_
#define MASZYNAMNOZACODODAJACA_HPP_

#include "MaszynMnozaca.hpp"

class MaszynaMnozacoDodajaca: public MaszynMnozaca
{
public:
	MaszynaMnozacoDodajaca(float x);
	virtual ~MaszynaMnozacoDodajaca();
	float dodaj(float x)
	{
		return mX+x;
	}
//	float pomnoz(float x)
//	{
//		return x*x;
//	}
};

#endif /* MASZYNAMNOZACODODAJACA_HPP_ */
