#include <iostream>

int main()
{
	std::cout << "Podaj ilo�� liczb: " << std::endl;
	int ilosc;
	std::cin >> ilosc;

	// dynamiczna tablica o rozmiarze ilosc
	int* t = new int[ilosc];

	// wczytanie liczb do tej tablicy

	for (int i = 0; i < ilosc; i++)
	{
		std::cout << "Podaj "<<i+1<< " liczbe: " << std::endl;
		int liczba;
		std::cin >> liczba;
		t[i] = liczba;
	}
	// obliczenie sumy elementow tablicy

	int suma = 0;
	for (int i = 0; i < ilosc; i++)
	{
		suma += t[i];
	}

	// wyswietlenie wyniku

	std::cout << "Suma: " << suma << std::endl;
	// zwolnienie tablicy
	delete[] t;
	return 0;
}
