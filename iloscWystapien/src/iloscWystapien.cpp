//============================================================================
// Name        : 03_6.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>

int main()
{
	int tablica[100];

	srand(time( NULL));

	for (int i = 0; i < 100; ++i)
	{
		tablica[i] = rand() % 21;
		std::cout << tablica[i] << std::endl;
	}

	for (int j = 1; j <= 20; ++j)

	{
		int licznik = 0;

		for (int k = 0; k < 100; ++k)
		{
			if (tablica[k] == j)
			{
				++licznik;
			}
		}
		std::cout << j << " wystapila " << licznik << std::endl;
	}

	return 0;
}

//void liczenie(int* tablica[])
//
//{
//
//	for (int j = 1; j <= 20; ++j)
//	{
//		int licznik = 0;
//
//		for (int i = 0; i < 100; ++i)
//		{
//			if (*tablica[i] == j)
//			{
//				++licznik;
//			}
//		}
//		std::cout << j << "wystapila" << licznik << std::endl;
//	}
//}
