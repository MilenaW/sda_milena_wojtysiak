#include <iostream>

void pokaz(float** m, int maxX, int maxY);

int main()
{
	const int stala[4][5] =
	{
	{ 1, 2, 3, 4, 5 },
	{ 6, 7, 8, 9, 10 },
	{ 11, 12, 13, 14, 15 },
	{ 16, 17, 18, 19, 20 } };

	std::cout << stala[1][4] << std::endl;

	float** macierz = NULL;

	// stworzy� dynamicznie dwuwymiarow� tablic� float�w
	macierz = new float*[4];
	for (int i = 0; i < 4; ++i)
	{
		macierz[i] = new float[5];
	}

	// przepisa� warto��i ze sta�ej tablicy tablic, mno��c ka�d� warto�� przez 1.5f
	const float f = 1.5;
	for (int i = 0; i < 4; ++i)
	{
		for (int j=0; j<5;++j)
		{
			macierz[i][j]=f*stala[i][j];
		}
	}



	// pokaza� przemno�on� tablic� za pomoc� funkcji pokaz()
	pokaz(macierz, 5, 4);

	// zwolni� tablic� dwuwymiarow�
	for (int i = 0; i < 4; ++i)
	{
		delete [] macierz [i];
	}
	pokaz(macierz, 5, 4);

delete [] macierz;
	return 0;
}

void pokaz(float** m, int maxX, int maxY)
{
	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			std::cout << m[y][x] << ' ';
		}
		std::cout << std::endl;
	}

}
