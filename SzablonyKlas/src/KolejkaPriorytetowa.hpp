/*
 * KolejkaPriorytetowa.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef KOLEJKAPRIORYTETOWA_HPP_
#define KOLEJKAPRIORYTETOWA_HPP_

#include <iostream>
template<typename T>

class KolejkaPriorytetowa
{
private:
	class Node
	{
	public:
		T value;
		int mPrio;
		Node * next;


		Node(T value, int prio)
		{
			this->value = value;
			mPrio = prio;
			next = 0;
		}
	};
	size_t mSize;
	Node*head;

public:
	KolejkaPriorytetowa()
{
		head = 0;

		mSize = 0;
}
	virtual ~KolejkaPriorytetowa()
	{

	}
	void push (const T& elem, int prio)
	{
		Node* tmp = new Node(elem,prio);

		if (mSize==0)
		{
			head=tmp;
			mSize++;
		}
		else if (mSize==1)
		{
			if (tmp->mPrio<head->mPrio)
			{
				tmp->next=head;
				head=tmp;
				mSize++;
			}
			else
			{	tmp->next=head->next;
				head->next=tmp;
				mSize++;
			}

		}
		else if(mSize>1)
		{
			Node*current=head;

			if(tmp->mPrio>head->mPrio)
			{
				while (current->next!=0 && tmp->mPrio>current->next->mPrio)
				{
					current=current->next;
				}

				tmp->next=current->next;
				current->next = tmp;
				mSize++;
			}
			else
			{
				tmp->next=head;
				head=tmp;
				mSize++;
			}
		}

	}
	T pop()// wyjatek, trzeba sprawdzic czy nie jest pusta!
	{
		if (mSize==0)
		{
			throw std::out_of_range("Lista jest pusta!");
		}
		else
		{
			T tmp = head->value;
			Node*tmpNode = head->next;
			delete head;
			head=tmpNode;
			mSize--;
			return tmp;
		}
	}
	size_t size()
	{
		return mSize;
	}
	bool ifempty()
	{
		mSize==0?true:false;
	}
};

#endif /* KOLEJKAPRIORYTETOWA_HPP_ */
