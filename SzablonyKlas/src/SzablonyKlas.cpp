//============================================================================
// Name        : SzablonyKlas.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Pojemnik.hpp"
#include "TablicaDynamicznaSzablon.hpp"
#include "ListaSzablon.hpp"
#include "KolejkaPriorytetowa.hpp"
using namespace std;

int main() {
//	Pojemnik<string> pojemnikNaStringa("tere fere!");
//	pojemnikNaStringa.wypisz();
//
//	Pojemnik<int> pojemnikNaKase(1000000000);
//	pojemnikNaKase.wypisz();
//
//	Pojemnik<float>* wskaznikNaPojmnik = new Pojemnik<float>(5);
//	wskaznikNaPojmnik->wypisz();

//	TablicaDynamicznaSzablon<int> tab;
//	tab.add(5);
//	tab.add(6);
//	tab.add(7);
//	tab.show();
//
//	TablicaDynamicznaSzablon<string> tab2;
//	tab2.add("a");
//	tab2.add("b");
//	tab2.add("c");
//	tab2.show();
//	tab2.remove(1);
//	tab2.show();
//	cout<<tab2.find("c")<<endl;
//	tab2.show();

//	CircularBuffer<int> c(3);
//	c.add(4);
//	cout<<c.next()<<endl;

	KolejkaPriorytetowa <int> k;
	k.push(1,5);
	k.push(2,2);
	k.push(3,3);
	k.push(4,1);
	cout<<k.pop()<<endl;
	cout<<k.pop()<<endl;
	cout<<k.pop()<<endl;
	cout<<k.pop()<<endl;


	return 0;
}
