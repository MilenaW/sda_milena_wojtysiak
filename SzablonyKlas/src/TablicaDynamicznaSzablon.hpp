/*
 * TablicaDynamicznaSzablon.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef TABLICADYNAMICZNASZABLON_HPP_
#define TABLICADYNAMICZNASZABLON_HPP_
#include <iostream>


template<typename T1>
class TablicaDynamicznaSzablon
{
private:
	int mSize;
	T1* mTablica;

	void resize()
	{
		if (mSize == 0)
		{
			mSize++;
			mTablica = new T1[mSize];
		}
		else if (mSize != 0)
		{
			mSize++;
			T1*tmp = new T1[mSize];

			for (int i = 0; i < mSize - 1; i++)
			{
				tmp[i] = mTablica[i];
			}

			delete[] mTablica;

			mTablica = tmp;
		}
	}

public:

	TablicaDynamicznaSzablon() :
			mSize(0), mTablica(0)
	{

	}

	virtual ~TablicaDynamicznaSzablon()
	{
		clear();
	}

	void show()
	{
		for (int i = 0; i < mSize; i++)
		{
			std::cout << mTablica[i] << std::endl;
		}
	}
	void add(const T1& elem)
	{
		resize();
		mTablica[mSize - 1] = elem;
	}
	int find(const T1& elem) //zwraca pozycje
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mTablica[i] == elem)
			{
				return i;
			}
		}
		return -1;
	}

	size_t size()
	{
		return mSize;
	}
	void removeElement(T1& elem)
	{
		int pos = find(elem);

		if (pos > -1)
		{
			remove(pos);
		}
		else
		{

		}

	}
	void remove(size_t pos) //usuwa spod pozycji
	{

		if (pos < mSize && pos >= 0)
		{
			T1*tmp = new T1[mSize - 1];

			for (int i = 0; i < mSize - 1; i++)
			{
				if (i < pos)
				{
					tmp[i] = mTablica[i];
				}
				else if (i >= pos)
				{
					tmp[i] = mTablica[i + 1];
				}
			}

			delete[] mTablica;
			mTablica = tmp;
			mSize--;
		}
		else
		{
			throw std::out_of_range ("indeks poza zakresem");
		}

	}
	bool ifEmpty()
	{
		if (mSize==0)
		{
			return true;
		}
		else if (mSize!=0)
		{
			return false;
		}
	}
	T1
get(int pos) //zwraca element na pozycji podanej
{
	return mTablica[pos];
}
void clear()
{
	delete mTablica;
	mSize = 0;
	mTablica = 0;
}
}
;

#endif /* TABLICADYNAMICZNASZABLON_HPP_ */
