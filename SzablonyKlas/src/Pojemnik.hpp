/*
 * Pojemnik.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef POJEMNIK_HPP_
#define POJEMNIK_HPP_
#include <iostream>
template<typename T1>
class Pojemnik
{
private:
	T1 mZawartosc;
public:
	Pojemnik(T1 zawartosc)
	:mZawartosc(zawartosc)
	{}
	~Pojemnik(){}

	void wypisz()
	{
		std::cout<<getZawartosc()<<std::endl;
	}

	T1 getZawartosc() const
	{
		return mZawartosc;
	}

	void setZawartosc(T1 zawartosc)
	{
		mZawartosc = zawartosc;
	}
};



#endif /* POJEMNIK_HPP_ */
