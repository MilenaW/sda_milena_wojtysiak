/*
 * Lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_

#include <iostream>
template<typename T>
class CircularBuffer
{
private:
	class Node
	{
	public:
		T value;
		Node * next;

		Node(T value)
		{
			this->value = value;
			next = 0;
		}
	};

	Node* readPtr;
	Node* addPtr;

	size_t mSize;
	size_t mMaxSize;

//	Wezel* head; //wksaznik na pierwszy element
//	Wezel* tail; //wskaznik na ostatni element

public:

	CircularBuffer(size_t maxCapacity)
:readPtr(0)
,addPtr(0)
,mSize(0)
,mMaxSize(maxCapacity)
{
}

void add(const T& elem)
{
	if (mSize==0)
	{

		Node* newNode = new Node (elem);
		newNode->next = newNode;
		readPtr = newNode;
		addPtr = newNode;
		mSize++;
	}
	else if (mSize>0 && mSize<mMaxSize)
	{
		Node* newNode = new Node (elem);
		newNode->next = addPtr->next;
		addPtr->next = newNode;
		addPtr=newNode;
		mSize++;

		//wypelniamy kolejne elementy
	}
	else if (mSize==mMaxSize)
	{
		addPtr=addPtr->next;
		addPtr->value = elem;
		//rozmiar zostaje, rusza sie tylko wskaznik
	}
}
void clear()
{

}

size_t maxSixe()
{
	return -1;
}

size_t size()
{

}

T next()
{
	if (readPtr==0)
	{
		throw std::out_of_range(" ");
	}
	else
	{
		readPtr=readPtr->next;
		return readPtr->value;
	}
}
};

#endif /* LISTA_HPP_ */
