//============================================================================
// Name        : 03.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

double liczIloczyn(int iloscLiczb, const double* tabela, double* iloczyn)

{
	*iloczyn=1;
	for (int i=0; i<iloscLiczb; ++i)
	{
		*iloczyn*=tabela [i];
	}

	return *iloczyn;
}

int main()
{
	std::cout << "podaj ilosc liczb: " << std::endl;
	int iloscLiczb;
	std::cin >> iloscLiczb;

	double* tabela = new double[iloscLiczb];

	for (int i = 0; i < iloscLiczb; ++i)
	{
		std::cout << "podaj " << i + 1 << " liczbe " << std::endl;
		int liczba;
		std::cin >> liczba;

		if (liczba == 0)
		{
			std::cout << "Zero, serio?" << std::endl;
			break;
		}

		else if (liczba != 0)
		{
			tabela[i] = liczba;
		}
	}
	double iloczyn;
	liczIloczyn(iloscLiczb, tabela, &iloczyn);
	std::cout << "iloczyn " <<iloczyn<< std::endl;

	delete[] tabela;
	return 0;
}
