//============================================================================
// Name        : Gra.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <vector>
#include "Console.hpp"
#include "Actor.hpp"
#include "Tile.hpp"
#include "Map.hpp"
#include "Player.hpp"
#include "Enemy.hpp"
#include "TheGame.hpp"

using namespace std;
using namespace Game;



int main() {

	TheGame g;

	Game::Key key = Game::Key::INVALID;

//Game.loadLevel();

	g.init();

	while (key != Game::Key::QUIT)
	{
		g.gameLoop(key);
	}

	return 0;
}
