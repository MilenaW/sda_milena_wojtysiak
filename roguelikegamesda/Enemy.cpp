/*
 * Enemy.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#include "Enemy.hpp"

Enemy::Enemy(int x, int y, char symbol, Game::FgColour::Colour fgColour, Game::BgColour::Colour bgColour, Game::TheGame* ptr)
: Actor (x,y,symbol,fgColour, bgColour, ptr)
{
	// TODO Auto-generated constructor stub

}

Enemy::~Enemy() {
	// TODO Auto-generated destructor stub
}
void Enemy::render(Game::Console& console)
{
	console.drawChar(mSymbol, mX, mY, mFgColour,mBgColour);
}

//void Enemy::move()
//{
//	int key = rand()%4;
//	short int dx=rand()%48;
//	short int dy=rand()%18;
//
//	switch (key)
//	{
//	case 0:
//		dy=-1;
//		break;
//	case 1:
//		dy = 1;
//		break;
//	case 2:
//		dx = -1;
//		break;
//	case 3:
//		dx = 1;
//		break;
//	default:
//		//Nothing
//		break;
//	}
//
//	if (!map.isWall(Enemy enemy.getX()+dx, Enemy enemy.getY()+dy))
//	{
//		actor.move(dx, dy);
//	}
//}
