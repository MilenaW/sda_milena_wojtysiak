/*
 * TheGame.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#include "TheGame.hpp"
namespace Game
{

TheGame::TheGame()
{

}

TheGame::~TheGame()
{

}

void TheGame::init()
{
	Game::Console console;
	int x = 10;
	int y = 10;
	char ch = '$';
	short int w = 100;
	short int h = 40;


	mMap = Map(w/2,h/2);
	mPlayer = Player(x, y, ch, Game::FgColour::RED, Game::BgColour::BLACK, 3, this);


	std::vector<Enemy> enemies;

	console.setConsoleSize(w, h);
	mConsole=console;

		int xMax = mMap.getWidth();
	 	int yMax = mMap.getHeight();

	 	for (int i =0; i<xMax; i++)
	 	{
	 		for(int j = 0; j<yMax; j++)
	 		{
	 			if ( i==0 || j==0 || i == xMax-1 || j==yMax - 1)
	 			{
	 				mMap.setTile(i,j,1);
	 			}

	 		}
	 	}

	for (int i = 0; i<5; i++)
	{
		mEnemies.push_back(Enemy (x+i*rand()%5, y+i+rand()%5, '#', Game::FgColour::BLUE, Game::BgColour::BLACK, this));
	}

}

void TheGame::gameLoop(Game::Key key)
{
		draw();
		key = mConsole.getKey();
		update(key);
}

void TheGame::draw()
{
	mConsole.clear();
	mMap.render(mConsole);

	std::cout<<std::endl;
	std::cout<<"LIVES: "<<mPlayer.getHp()<<std::endl;

	mPlayer.render(mConsole);

	for (std::vector<Enemy>::iterator it = mEnemies.begin(); it!=mEnemies.end(); it++)
	{
	it->render(mConsole);
	}

}

void TheGame::update(Game::Key key)
{
	short int dx=0;
	short int dy=0;

	switch (key)
	{
	case Game::Key::MOVE_UP:
		dy=-1;
		break;
	case Game::Key::MOVE_DOWN:
		dy = 1;
		break;
	case Game::Key::MOVE_LEFT:
		dx = -1;
		break;
	case Game::Key::MOVE_RIGHT:
		dx = 1;
		break;
	case Game::Key::QUIT:
	case Game::Key::INVALID:
	default:
		//Nothing
		break;
	}

	if (!mMap.isWall(mPlayer.getX()+dx, mPlayer.getY()+dy))
	{
		mPlayer.move(dx, dy);
	}

		for (std::vector<Enemy>::iterator it = mEnemies.begin(); it!=mEnemies.end(); it++)
		{
			int ene = rand()%4;
			short int ex=0;
			short int ey=0;

			switch (ene)
			{
			case 0:
				ey=-1;
				break;
			case 1:
				ey = 1;
				break;
			case 2:
				ex = -1;
				break;
			case 3:
				ex = 1;
				break;
			default:
				//Nothing
				break;
			}

			if (!mMap.isWall(it->getX()+ex, it->getY()+ey))
			{
				it->move(ex, ey);
			}

			if (mPlayer.checkIfCollision(*it))
			{
				mPlayer.changeHP(-1);

				if(mPlayer.getHp() == 0)
				{
					mPlayer.setFgColour(Game::FgColour::BLACK);
				}

				mEnemies.erase(it);
				advance(it, -1);
			}


		}
}

}
