/*
 * Player.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#include "Player.hpp"

Player::Player()
{

}

Player::Player(int x, int y, char symbol, Game::FgColour::Colour fgColour, Game::BgColour::Colour bgColour, int hp, Game::TheGame* ptr)
: Actor (x,y,symbol,fgColour, bgColour, ptr)
, mHP(hp)
{


}

Player::~Player()
{


}

void Player::changeHP(int x)
{
	mHP+=x;
}

int Player::getHp() const {
	return mHP;
}

void Player::setHp(int hp) {
	mHP = hp;
}
