/*
 * Tile.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef TILE_HPP_
#define TILE_HPP_

namespace Game
{

struct Tile
{
	short int mX;
	short int mY;
	bool mIsWall;

	Tile(short int x, short int y, bool isWall)
	: mX(x)
	, mY(y)
	, mIsWall(isWall)
	{

	};

};

}

#endif /* TILE_HPP_ */
