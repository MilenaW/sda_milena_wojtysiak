/*
 * Enemy.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_

#include "Actor.hpp"


class Enemy: public Game::Actor {
public:
	Enemy(int x, int y, char symbol, Game::FgColour::Colour fgColour, Game::BgColour::Colour bgColour, Game::TheGame* ptr);
	virtual ~Enemy();
	void render(Game::Console& console);
//	void move();
};

#endif /* ENEMY_HPP_ */
