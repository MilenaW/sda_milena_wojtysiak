/*
 * TheGame.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef THEGAME_HPP_
#define THEGAME_HPP_

#include <vector>
#include "Console.hpp"
#include "Actor.hpp"
#include "Tile.hpp"
#include "Map.hpp"
#include "Player.hpp"
#include "Enemy.hpp"


namespace Game
{
class TheGame
{
private:
	Map mMap;
	Console mConsole;
	Player mPlayer;
	std::vector<Enemy> mEnemies;
	void draw();
	void update(Game::Key key);

public:
	TheGame();
	virtual ~TheGame();
	void init();
	void gameLoop(Game::Key key);
};
}

#endif /* THEGAME_HPP_ */
