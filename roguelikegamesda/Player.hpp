/*
 * Player.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "Actor.hpp"


class Player: public Game::Actor {
public:
	Player();
	Player(int x, int y, char symbol, Game::FgColour::Colour fgColour, Game::BgColour::Colour bgColour, int hp, Game::TheGame* ptr);
	virtual ~Player();
	void changeHP(int x);
	int getHp() const;
	void setHp(int hp);

private:
	int mHP;

};

#endif /* PLAYER_HPP_ */
