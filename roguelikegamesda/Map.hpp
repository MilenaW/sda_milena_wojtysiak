/*
 * Map.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef MAP_HPP_
#define MAP_HPP_
//kontener na tiles - vector
#include <vector>
#include <iostream>
#include <algorithm>
#include "Tile.hpp"
#include "Console.hpp"

namespace Game
{

class Map
{
private:
	int mWidth;
	int mHeight;
	std::vector <Tile> mTiles;



public:
	Map();
	Map(int width, int height);
	virtual ~Map();
	bool isWall (int x, int y);//czy jest sciana na danej pozycji
	void setTile(int x, int y, bool isWall);//ustawienie wlasciwosci kafelka na sciana
	void render(const Game::Console& console);
	void drawWall(const Game::Console& console, int x, int y);
	void drawFloor(const Game::Console& console, int x, int y);
	int getHeight() const;
	int getWidth() const;
};

}
#endif /* MAP_HPP_ */
