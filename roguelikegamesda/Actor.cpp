/*
 * Actor.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include "Actor.hpp"

namespace Game
{
Actor::Actor()
{

}

Actor::Actor(int x, int y, char symbol, Game::FgColour::Colour fgColour, Game::BgColour::Colour bgColour,Game::TheGame* ptr)
: mX(x)
, mY(y)
, mSymbol(symbol)
, mFgColour(fgColour)
, mBgColour(bgColour)
, mTheGamePtr(ptr)
{

}

void Actor::render(Game::Console& console)
{
	console.drawChar(mSymbol, mX, mY, mFgColour,mBgColour);
}

void Actor::setPos(int x, int y)
{
	setX(x);
	setY(y);
}

void Actor::setX(int x)
{
	mX = x;
}

void Actor::setY(int y)
{
	mY = y;
}

int Actor::getX() const {
	return mX;
}

int Actor::getY() const {
	return mY;
}

void Actor::move(short int x, short int y)
{
	mX+=x;
	mY+=y;
}

Actor::~Actor()
{

}

void Actor::setFgColour(Game::FgColour::Colour fgColour) {
	mFgColour = fgColour;
}

bool Actor::checkIfCollision(Actor& actor)
{
	if(mX==actor.mX && mY==actor.mY)
	{
		return true;
	}
	else
	{
		return false;
	}
}

}
