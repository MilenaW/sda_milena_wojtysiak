/*
 * Actor.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef ACTOR_HPP_
#define ACTOR_HPP_
#include "Console.hpp"



namespace Game
{
class TheGame;
class Actor {

protected:
	int mX;
	int mY;
	char mSymbol;
	Game::FgColour::Colour mFgColour;
	Game::BgColour::Colour mBgColour;
	Game::TheGame* mTheGamePtr;
public:
	Actor();
	Actor(int x, int y, char symbol, Game::FgColour::Colour fgColour, Game::BgColour::Colour bgColour, Game::TheGame* ptr);
	virtual ~Actor();
	void render(Game::Console& console);
	void setPos(int x, int y);
	void setX(int x);
	void setY(int y);
	void move(short int x, short int y);
	int getX() const;
	int getY() const;
	bool checkIfCollision(Actor& actor);
	void setFgColour(Game::FgColour::Colour fgColour);
};

}
#endif /* ACTOR_HPP_ */
