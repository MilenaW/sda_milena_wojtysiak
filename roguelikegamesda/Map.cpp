/*
 * Map.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include "Map.hpp"

namespace Game
{

class FindTile
{
private:
	int X;
	int Y;
public:
	FindTile(int x, int y)
	: X(x)
	, Y(y)
	{

	}
	bool operator() (const Tile& tile) const
		{
		if (X==tile.mX&&Y==tile.mY)
		{
			return true;
		}

		else
			return false;
		}
};

Map::Map()
{

}

Map::Map(int width, int height)
: mWidth(width)
, mHeight(height)
{
	for (int x = 0; x<mWidth;x++)
	{
		for( int y = 0; y<mHeight; y++)
		{
			mTiles.push_back(Tile(x, y, 0));
		}
	}
}

Map::~Map()
{

}

void Map::drawWall(const Game::Console& console, int x, int y)
{
	console.drawChar(219,x,y,Game::FgColour::GREEN, Game::BgColour::BLACK);
}

void Map::drawFloor(const Game::Console& console, int x, int y)
{
	console.drawChar(176,x,y,Game::FgColour::BLACK, Game::BgColour::BLACK);
}

bool Map::isWall (int x, int y)
{
	std::vector<Tile>::iterator it = std::find_if(mTiles.begin(), mTiles.end(), FindTile(x,y));

	return it->mIsWall;
}

void Map::setTile(int x, int y, bool isWall)
{
	std::vector<Tile>::iterator it = std::find_if(mTiles.begin(), mTiles.end(), FindTile(x,y));

	it->mIsWall = isWall;
}

void Map::render(const Game::Console& console)
{

for ( std::vector<Tile>::iterator it = mTiles.begin(); it != mTiles.end(); ++it)
{
	if(it->mIsWall)
	{
		drawWall(console, it->mX, it->mY);
	}
	else
	{
		drawFloor(console, it->mX,it->mY);
	}
}

}



}

int Game::Map::getHeight() const {
	return mHeight;
}

int Game::Map::getWidth() const {
	return mWidth;
}
