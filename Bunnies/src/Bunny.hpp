/*
 * Bunny.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef BUNNY_HPP_
#define BUNNY_HPP_
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class Bunny
{
public:
	enum Sex
	{
		Male,
		Female,
		lasts
	};

	enum Color
	{
		Black = 0,
		White,
		Spotted,
		Gray,
		lastc
	};

	Bunny(string name);

	Bunny(string name, Color color);

	~Bunny();

	bool happyBirthay();

	Color getColor() const;

	bool isMutant() const;

	void setMutant(bool mutant);

	const string& getName() const;

	Sex getSex() const;


private:
	Sex mSex;
	Color mColor;
	unsigned int mAge;
	string mName;
	bool mMutant;

	void randomizeSex();
	void randomizeMutant();
	void randomizeColor();
};


#endif /* BUNNY_HPP_ */
