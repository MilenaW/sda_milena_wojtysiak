/*
 * Bunny.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "Bunny.hpp"





Bunny::Bunny(string name)
:mAge(0)
,mName(name)
{
		randomizeSex();
		randomizeColor();
		randomizeMutant();
}

Bunny::Bunny(string name, Color color)
	: mColor(color)
	, mAge(0)
	, mName(name)
	{
		randomizeSex();
		randomizeMutant();
	}

Bunny::~Bunny()
{

}

bool Bunny::happyBirthay()
{
	if (mAge<10)
	{
		return false;
	}
	else if (mAge==10 && mMutant==false)
	{
		return true;
	}
	else if (mAge<50 && mMutant==true)
	{
		return false;
	}
	else if (mAge==50 && mMutant==true)
	{
		return true;
	}
}

Bunny::Color Bunny::getColor() const
{
	return mColor;
}

bool Bunny::isMutant() const
{
	return mMutant;
}

void Bunny::setMutant(bool mutant)
{
	mMutant = mutant;
}

const string& Bunny::getName() const
{
	return mName;
}

Bunny::Sex Bunny::getSex() const
{
	return mSex;
}

void Bunny::randomizeSex()
{
mSex=static_cast<Bunny::Sex>(rand() % lasts);
}

void Bunny::randomizeMutant()
{
	unsigned int tmp = rand()%100;

	if (tmp==1)
	{
		mMutant=true;
	}
	else
	{
		mMutant=false;
	}
}

void Bunny::randomizeColor()
{
	mColor=static_cast<Bunny::Color>(rand() % lastc);
}


