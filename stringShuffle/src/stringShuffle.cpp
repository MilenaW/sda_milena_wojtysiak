//============================================================================
// Name        : stringShuffle.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
using namespace std;



void print(char x)
{
	cout<<x<<" ";
}

int main() {

srand ( unsigned (time(0) ) );

vector <char> l;
string word = "edacb";


for(size_t i= 0; i<word.size(); i++ )
{
	l.push_back(word[i]);
}

for_each(l.begin(), l.end(), print);

cout<<endl;

random_shuffle(l.begin(),l.end());

for_each(l.begin(), l.end(), print);

	return 0;
}
