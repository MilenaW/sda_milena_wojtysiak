/*
 * Kalendarz.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef KALENDARZ_HPP_
#define KALENDARZ_HPP_



class Kalendarz
{
private:
	int mDzien;
	int mMiesiac;
	int mRok;

public:
	Kalendarz();
	Kalendarz(int dzien, int miesiac, int rok);
	virtual ~Kalendarz();
	bool czyMiesiacPoprawna();
	bool czyPrzestepny();


};

#endif /* KALENDARZ_HPP_ */
