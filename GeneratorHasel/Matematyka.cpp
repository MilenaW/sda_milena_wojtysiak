/*
 * Matematyka.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Matematyka.hpp"

Matematyka::Matematyka()
{
	// TODO Auto-generated constructor stub

}

Matematyka::Matematyka(int n) :
		mN(n)
{
	// TODO Auto-generated constructor stub

}

Matematyka::~Matematyka()
{
	// TODO Auto-generated destructor stub
}

int Matematyka::silnia(int m)
{
	if (m == 0)
	{
		return 1;
	}

	else
	{
		return m * silnia(m - 1);
	}

}

int Matematyka::Fibonacci(int l)
{

	if (l == 0)
	{
		return 0;
	}
	else if (l == 1)
	{
		return 1;
	}
	else
	{
		return Fibonacci(l - 1) + Fibonacci(l - 2);
	}
}

int Matematyka::NWD(int a, int b)
{

	int c;

	while (b != 0)
	{
		c = a % b;
		a = b;
		b = c;
	}
	return a;
}
