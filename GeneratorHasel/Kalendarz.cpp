/*
 * Kalendarz.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Kalendarz.hpp"

Kalendarz::Kalendarz()
{
	// TODO Auto-generated constructor stub

}

Kalendarz::Kalendarz(int dzien, int miesiac, int rok) :
		mDzien(dzien), mMiesiac(miesiac), mRok(rok)
{

}

Kalendarz::~Kalendarz()
{
	// TODO Auto-generated destructor stub
}

bool Kalendarz::czyMiesiacPoprawna()
{
	if (mRok >= 0 && mRok <= 2020)
	{
		if (mMiesiac >= 1 && mMiesiac <= 12)
		{

			switch (mMiesiac)
			{
			case 1:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			case 2:
				if (czyPrzestepny() == true)
				{
					if (mDzien >= 1 && mDzien <= 29)
					{
						return true;
					}
					else
						return false;
				}

				else if (mDzien >= 1 && mDzien <= 28)
					return true;
				else
					return false;
				break;
			case 3:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			case 4:
				if (mDzien >= 1 && mDzien <= 30)
					return true;
				else
					return false;
				break;
			case 5:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			case 6:
				if (mDzien >= 1 && mDzien <= 30)
					return true;
				else
					return false;
				break;
			case 7:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			case 8:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			case 9:
				if (mDzien >= 1 && mDzien <= 30)
					return true;
				else
					return false;
				break;
			case 10:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			case 11:
				if (mDzien >= 1 && mDzien <= 30)
					return true;
				else
					return false;
				break;
			case 12:
				if (mDzien >= 1 && mDzien <= 31)
					return true;
				else
					return false;
				break;
			}
		}
		else
			return false;
	}
	else
		return false;
}

bool Kalendarz::czyPrzestepny()
{
	if (mRok % 4 == 0 && mRok % 100 != 0)
	{
		return true;
	}
	else if (mRok % 400 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
