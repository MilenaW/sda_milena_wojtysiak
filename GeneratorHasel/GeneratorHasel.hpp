/*
 * GeneratorHasel.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef GENERATORHASEL_HPP_
#define GENERATORHASEL_HPP_
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GeneratorHasel
{
private:
	unsigned int mIloscZnakow;
	string mHasloPrzejsciowe;
	string mHaslo;
public:
	GeneratorHasel();
	GeneratorHasel(unsigned int iloscZnakow);
	virtual ~GeneratorHasel();
	char dajCyfre();
	char dajDuzaLitere();
	char dajMalaLitere();
	string hasloDocelowe();
	string hasloPrzejsciowe();
	string przemieszaj(string& hasloPrzejsciowe);
	const string& getHaslo() const;
};

#endif /* GENERATORHASEL_HPP_ */
