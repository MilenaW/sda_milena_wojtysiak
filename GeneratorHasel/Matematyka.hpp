/*
 * Matematyka.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef MATEMATYKA_HPP_
#define MATEMATYKA_HPP_

class Matematyka
{
private:
	int mN = 0;

public:
	Matematyka();
	Matematyka(int n);
	virtual ~Matematyka();
	int silnia(int m);
	int Fibonacci(int l);
	int NWD (int a, int b);
};

#endif /* MATEMATYKA_HPP_ */
