/*
 * Sensor.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef SENSOR_HPP_
#define SENSOR_HPP_


class Sensor
{
public:
	virtual bool check() = 0;
	virtual ~Sensor()
	{
	}
};


#endif /* SENSOR_HPP_ */
