/*
 * Machine.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef MACHINE_HPP_
#define MACHINE_HPP_

class Machine
{
public:
	virtual void turnOn(){}
	virtual void turnOff(){}
	virtual ~Machine()
	{
	}

};



#endif /* MACHINE_HPP_ */
