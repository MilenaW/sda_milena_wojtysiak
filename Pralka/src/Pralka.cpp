//============================================================================
// Name        : Pralka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "WashOption.hpp"
#include "Sensor.hpp"
#include "WaterSensor.hpp"
#include "WaszingMachine.hpp"
#include "Machine.hpp"
#include "Engine.hpp"
#include "Timer.hpp"
#include "DoorSensor.hpp"
#include "TempSensor.hpp"

using namespace std;



int main() {
	WashingMachine wm;
	wm.main();
	return 0;
}
