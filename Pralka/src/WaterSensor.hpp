/*
 * WaterSensor.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */
#include "Sensor.hpp"
#ifndef WATERSENSOR_HPP_
#define WATERSENSOR_HPP_

class WaterSensor: public Sensor
{
public:
	bool check();
	int currentLevel;
	int desiredLevel;
};



#endif /* WATERSENSOR_HPP_ */
