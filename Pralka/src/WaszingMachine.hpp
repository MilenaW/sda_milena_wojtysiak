/*
 * WaszingMachine.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */
#include "Sensor.hpp"
#include "WashOption.hpp"
#include "Sensor.hpp"
#include "WaterSensor.hpp"
#include "Machine.hpp"
#include "Engine.hpp"
#include "Timer.hpp"
#include "DoorSensor.hpp"
#include "TempSensor.hpp"
#ifndef WASZINGMACHINE_HPP_
#define WASZINGMACHINE_HPP_

class WashingMachine: public Machine
{
private:
	int washTime;
	int rinseTime;
	int spinTime;

	void wash(){}
	void rinse(){}
	void spin()
	{
		Engine en;
		en.turnOn();
		Timer timer;
		timer.setDuration(60*60);
		timer.start();
		int time = timer.getValue();
		int duration = timer.getDuration();

		while (time!=duration)
		{
			time = timer.count();
		}

		en.turnOff();
	}
	void fill(){}
	void empty(){}
	void standardWash(){}
	void twiceRinse(){}

public:
	void turnOn(){}
	void turnOff(){}
	void main()
	{
		WashOption wo;
		switch (wo.getWashSelection())
		{
		case 1:
			standardWash();
			break;
		case 2:
			twiceRinse();
			break;
		case 3:
			spin();
			break;
		default:
			break;
		}

	}
};

#endif /* WASZINGMACHINE_HPP_ */
