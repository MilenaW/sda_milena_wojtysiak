/*
 * Przyklad.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef PRZYKLAD_HPP_
#define PRZYKLAD_HPP_
#include <string>
using namespace std;

extern int i;
extern string s;
extern float f;

void zmienString(string ss);
void zmienFloat(float ff);

#endif /* PRZYKLAD_HPP_ */
