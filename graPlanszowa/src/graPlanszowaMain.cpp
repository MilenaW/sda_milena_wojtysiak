#include <iostream>
#include <cmath>
#include "graPlanszowa.hpp"
#include "StrzelecGraPlanszowa.hpp"
#include "Kawaleria.hpp"
#include "Wojownik.hpp"

using namespace std;

int main()
{
CWojownik wojownik;
CStrzelec strzelec;
CKawaleria kawalarz;

wojownik.atak(1,1);
strzelec.atak(1,1);
kawalarz.atak(1,1);

wojownik.CJednostka::atak(1,1);
strzelec.CJednostka::atak(1,1);
kawalarz.CJednostka::atak(1,1);
	return 0;
}
