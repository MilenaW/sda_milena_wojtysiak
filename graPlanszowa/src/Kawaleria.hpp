
#ifndef KAWALERIA_HPP_
#define KAWALERIA_HPP_

#include "graPlanszowa.hpp"
using namespace std;
class CKawaleria : public CJednostka
{
public:
	CKawaleria(char symbol='$', int zycie=60, int atak=4, int szybkosc=5);
	bool atak(int x, int y);

};
#endif /* CKAWALERIA_HPP_ */
