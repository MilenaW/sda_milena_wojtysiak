/*
 * CWojownik.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef WOJOWNIK_HPP_
#define WOJOWNIK_HPP_
using namespace std;
#include "graPlanszowa.hpp"

class CWojownik : public CJednostka
{
public:
	CWojownik(char symbol = '@', int zycie=100, int atak=2, int szybkosc=5);
	bool atak(int x, int y);

};


#endif /* WOJOWNIK_HPP_ */
