/*
 * CStrzelec.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CSTRZELEC_HPP_
#define CSTRZELEC_HPP_

#include "graPlanszowa.hpp"
using namespace std;
class CStrzelec : public CJednostka
{
public:
	CStrzelec(char symbol='%', int zycie=200, int atak=10, int szybkosc=7);
	bool atak(int x, int y);

};


#endif /* CSTRZELEC_HPP_ */
