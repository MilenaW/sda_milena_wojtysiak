//============================================================================
// Name        : sumaKwadratow.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

void print(float x)
{
	cout<<x<<" ";
}

void power(float& x)
{
	x=x*x;
}

int main() {

	vector <float> range;
	vector <float> final;
	float min = 3;
	float max = 10;

	for(size_t i= min; i<=max; i++ )
	{
		range.push_back(i);
	}

	for_each(range.begin(), range.end(), print);
	cout<<endl;

	for_each(range.begin(), range.end(), power);
	for_each(range.begin(), range.end(), print);
	cout<<endl;
	float sum = accumulate(range.begin(), range.end(), 0);

	cout<<sum<<endl;


	return 0;
}

