/*
 * Lista.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include <iostream>
#include <string>
using namespace std;
class Lista
{
	class Node
	{
	public:
		Node(string dane, Node* nastepny)
	: mDane(dane)
	, mNastepny(nastepny)
		{
		}

		string getDane()
		{
			return mDane;
		}

		void setDane(string dane)
		{
			mDane = dane;
		}

	 Node* getNastepny()
		{
			return mNastepny;
		}

		void setNastepny( Node*nastepny)
		{
			mNastepny = nastepny;
		}

	private:
		string mDane;
		Node* mNastepny;
	};

	Node* mPierwszy;
	int mRozmiar;

public:
	void dodaj(string lancuch);
	void wypisz ();
	bool czyPusta();
	Lista();
};

#endif /* LISTA_HPP_ */
