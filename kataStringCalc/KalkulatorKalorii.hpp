/*
 * KalkulatorKalorii.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef KALKULATORKALORII_HPP_
#define KALKULATORKALORII_HPP_

class KalkulatorKalorii
{

public:
	enum Sex
	{
		kobieta, mezczyzna, nieznana
	};
	KalkulatorKalorii();
	KalkulatorKalorii(Sex sex, float waga, float wzrost, int wiek);
	virtual ~KalkulatorKalorii();

	float returnBMI();
	int calculateCalories();
	Sex getSex() const;
	void setSex(Sex sex);
	float getWaga() const;
	void setWaga(float waga);
	int getWiek() const;
	void setWiek(int wiek);
	float getWzrost() const;
	void setWzrost(float wzrost);

private:

	float mWaga = 0.00;
	float mWzrost = 0.00;
	int mWiek = 0.00;
	Sex mSex = nieznana;
};

#endif /* KALKULATORKALORII_HPP_ */
