/*
 * KostkaTesty.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Kostka.hpp"

TEST(KostkaTest, DefaultFieldsTest)
{
	Kostka k;
	EXPECT_EQ(0, k.getMax());
}

TEST(KostkaTest, GetSetTest)
{
	Kostka k;
	k.setMax(6);
	EXPECT_EQ(6, k.getMax());
}

TEST(KostkaTest, ParameterTest)
{
	Kostka k(7);
	EXPECT_EQ(7, k.getMax());
}

TEST(KostkaTest, LosujTest)
{
	Kostka k(7);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
	EXPECT_LE(k.losuj(), k.getMax());
	EXPECT_GE(k.losuj(), 0);
}
//
//TEST(KalkulatorKaloriiTest, BMITest)
//{
//	KalkulatorKalorii k (KalkulatorKalorii::mezczyzna, 100, 200, 20);
//	EXPECT_EQ(25, k.returnBMI());
//}
//
//TEST(KalkulatorKaloriiTest, calculateCaloriesTest)
//{
//	KalkulatorKalorii k (KalkulatorKalorii::mezczyzna, 100, 200, 20);
//	EXPECT_EQ(2301, k.calculateCalories());
//}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


