/*
 * Kostka.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef KOSTKA_HPP_
#define KOSTKA_HPP_
#include <cstdlib>
#include <ctime>

class Kostka
{
private:
	int mMax = 0;

public:
	Kostka();
	Kostka(int max);
	virtual ~Kostka();
	int losuj ();
	int getMax() const;
	void setMax(int max = 0);
};

#endif /* KOSTKA_HPP_ */
