/*
 * KalkulatorKalorii.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "KalkulatorKalorii.hpp"

KalkulatorKalorii::KalkulatorKalorii()
{

}

KalkulatorKalorii::KalkulatorKalorii(Sex sex, float waga, float wzrost, int wiek)
: mSex(sex)
, mWaga(waga)
, mWzrost(wzrost)
, mWiek(wiek)
{
	// TODO Auto-generated constructor stub

}

KalkulatorKalorii::~KalkulatorKalorii()
{
	// TODO Auto-generated destructor stub
}

float KalkulatorKalorii::returnBMI()
{
	return mWaga/((mWzrost/100)*(mWzrost/100));
}
int KalkulatorKalorii::calculateCalories()
{
	float x = 0.0;
	switch (mSex)
	{
	case kobieta:
		x = 655.1 + (9.567*mWaga) + (1.85*mWzrost) - (4.68*mWiek);
	break;
	case mezczyzna:
		x = 66.47 + (13.7 * mWaga) + (5.0 * mWzrost) - (6.76*mWiek);
	break;
	case nieznana:
		x=0;
	break;
	}
	return x;
}

KalkulatorKalorii::Sex KalkulatorKalorii::getSex() const
{
	return mSex;
}

void KalkulatorKalorii::setSex(Sex sex)
{
	mSex = sex;
}

float KalkulatorKalorii::getWaga() const
{
	return mWaga;
}

void KalkulatorKalorii::setWaga(float waga)
{
	mWaga = waga;
}

int KalkulatorKalorii::getWiek() const
{
	return mWiek;
}

void KalkulatorKalorii::setWiek(int wiek)
{
	mWiek = wiek;
}

float KalkulatorKalorii::getWzrost() const
{
	return mWzrost;
}

void KalkulatorKalorii::setWzrost(float wzrost)
{
	mWzrost = wzrost;
}
