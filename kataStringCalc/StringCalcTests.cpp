///*
// * CalculatorTest.cpp
// *
// *  Created on: 20.04.2017
// *      Author: RENT
// */
//
//#include "gtest/gtest.h"
//#include "StringCalculator.hpp"
//
////TEST(CalculatorTest, AddTest) {
////      Calculator c;
////      EXPECT_EQ(4, c.add(2,2));
////      EXPECT_EQ(1, c.add(0,1));
////      EXPECT_EQ(0, c.add(-2,2));
////      EXPECT_EQ(-4, c.add(-2,-2));
////}
//
//TEST(StringCalculator, EmptyString)
//{
//	StringCalculator c;
//	EXPECT_EQ(0, c.Add(""));
//}
//
//TEST(StringCalculator, OneNumber)
//{
//	StringCalculator c;
//	EXPECT_EQ(1, c.Add("1"));
//	EXPECT_EQ(121, c.Add("121"));
//}
//
//TEST(StringCalculator, TwoNumbers)
//{
//	StringCalculator c;
//	EXPECT_EQ(3, c.Add("1,2"));
//	EXPECT_EQ(33, c.Add("11,22"));
//	EXPECT_EQ(333, c.Add("111,222"));
//}
//
//TEST(StringCalculator, MoreNumbers)
//{
//	StringCalculator c;
//	EXPECT_EQ(6, c.Add("1,2,3"));
//	EXPECT_EQ(77, c.Add("11,22,33,11"));
//	EXPECT_EQ(666, c.Add("111,222,111,111,111"));
//}
//int main(int argc, char **argv) {
//      ::testing::InitGoogleTest(&argc, argv);
//      return RUN_ALL_TESTS();
//}
//
//
