/*
 * Kostka.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "Kostka.hpp"

Kostka::Kostka()
{

}

Kostka::Kostka(int max)
:mMax(max)
{
	// TODO Auto-generated constructor stub

}

Kostka::~Kostka()
{
	// TODO Auto-generated destructor stub
}

int Kostka::getMax() const
{
	return mMax;
}

void Kostka::setMax(int max)
{
	mMax = max;
}

int Kostka::losuj()
{
	srand( time( NULL ) );
	int x = rand()%mMax;
	return x;
}
