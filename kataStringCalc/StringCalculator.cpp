/*
 * StringCalculator.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */
#include <cstdlib>
#include "StringCalculator.hpp"

int StringCalculator::Add(std::string number)
{
	if(number.size()==0)
	{
		return 0;
	}
	else if (number.find(",")== std::string::npos)
	{
		return std::atoi(number.c_str());
	}

	else
	{
		int comaPosition = number.find(",");
		std::string numberOne = number.substr(0, comaPosition);
		std::string numberTwo = number.substr(comaPosition+1, std::string::npos);
		return std::atoi(numberOne.c_str())+std::atoi(numberTwo.c_str());
	}
	return -1;
}

