///*
// * KalkulatorKaloriiTest.cpp
// *
// *  Created on: 22.04.2017
// *      Author: RENT
// */
///*
// * CalculatorTest.cpp
// *
// *  Created on: 20.04.2017
// *      Author: RENT
// */
//
//#include "gtest/gtest.h"
//#include "KalkulatorKalorii.hpp"
//
//TEST(KalkulatorKaloriiTest, DefaultFieldsTest)
//{
//	KalkulatorKalorii k;
//	EXPECT_EQ(0, k.getWaga());
//	EXPECT_EQ(KalkulatorKalorii::nieznana, k.getSex());
//	EXPECT_EQ(0, k.getWiek());
//	EXPECT_EQ(0, k.getWzrost());
//}
//
//TEST(KalkulatorKaloriiTest, GetSetTest)
//{
//	KalkulatorKalorii k;
//	EXPECT_EQ(0, k.getWaga());
//	EXPECT_EQ(KalkulatorKalorii::nieznana, k.getSex());
//	EXPECT_EQ(0, k.getWiek());
//	EXPECT_EQ(0, k.getWzrost());
//
//	k.setWaga(100.11);
//	k.setSex(KalkulatorKalorii::kobieta);
//	k.setWiek(55);
//	k.setWzrost(111.111);
//
//	EXPECT_NEAR(100.11, k.getWaga(), 0.01);
//	EXPECT_EQ(KalkulatorKalorii::kobieta, k.getSex());
//	EXPECT_EQ(55, k.getWiek());
//	EXPECT_NEAR(111.111, k.getWzrost(), 0.01);
//
//}
//
//TEST(KalkulatorKaloriiTest, BMITest)
//{
//	KalkulatorKalorii k (KalkulatorKalorii::mezczyzna, 100, 200, 20);
//	EXPECT_NEAR(25, k.returnBMI(), 0.01);
//}
//
//TEST(KalkulatorKaloriiTest, calculateCaloriesTest)
//{
//	KalkulatorKalorii k (KalkulatorKalorii::mezczyzna, 100, 200, 20);
//	EXPECT_EQ(2301, k.calculateCalories());
//}
//
//int main(int argc, char **argv)
//{
//	::testing::InitGoogleTest(&argc, argv);
//	return RUN_ALL_TESTS();
//}
//
