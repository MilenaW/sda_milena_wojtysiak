//============================================================================
// Name        : PalindromInt.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <iterator>

using namespace std;

vector<int> intToVec(int i)
	{
	int tmp = i;
	vector<int> v;

	do
	{
		v.push_back(tmp%10);
		tmp/=10;
	}
	while (tmp>0);

	reverse(v.begin(), v.end());

	return v;
	}

int vecToInt (vector<int> v)
{
int tmp = 0;

int j=1;
	for (vector<int>::reverse_iterator it = v.rbegin(); it!=v.rend(); it++)
	{
		tmp+=*it*j;
		j=j*10;
	}
return tmp;
}


void print(int x)
{
	cout<<x<<" ";
}
int main() {


vector<int> v = intToVec(113);


for_each(v.begin(), v.end(), print);

cout<<endl;
vector<int> tmpv;
vector<int> tmpr;
tmpv = v;
do
{
	int tmpi = vecToInt(tmpv);
	tmpi+=1;
	tmpv = intToVec(tmpi);
	tmpr = tmpv;
	reverse(tmpr.begin(), tmpr.end());
}
while (tmpv!=tmpr);
for_each(tmpv.begin(), tmpv.end(), print);

	return 0;
}
