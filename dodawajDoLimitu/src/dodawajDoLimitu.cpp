//============================================================================
// Name        : 03_5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void dodawaj(int limit, int& suma, int& iloscLiczb)
{
	suma = 0;
	iloscLiczb = 0;
	for (int i = 1; suma < limit; ++i, ++iloscLiczb)
	{
		suma += i;
		std::cout << i << std::endl;
	}
}

int main()
{

	std::cout << "podaj limit: " << std::endl;
	int limit;
	std::cin >> limit;

	int suma;
	int iloscLiczb;

	dodawaj(limit, suma, iloscLiczb);

	std::cout << "suma " << suma << " ilosc Liczb " << iloscLiczb << std::endl;

	return 0;
}
