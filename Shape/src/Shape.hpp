/*
 * Shape.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_
#include <string>
using namespace std;

class Shape
{
public:
	Shape();
	virtual ~Shape();
	float pole();
	string getNazwa;
};

 /* namespace std */

#endif /* SHAPE_HPP_ */
