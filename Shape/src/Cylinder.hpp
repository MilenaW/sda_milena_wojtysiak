/*
 * Cylinder.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CYLINDER_HPP_
#define CYLINDER_HPP_
#include <string>
#include <iostream>
#include "Circle.hpp"
#include "Shape.hpp"

using namespace std;


class Cylinder: public Circle
{
private:
	float mR;
	float mH;
	string mNazwa = "cylinder";

public:
	Cylinder();
	Cylinder(float r, float h);
	virtual ~Cylinder();
	float pole();

};

 /* namespace std */

#endif /* CYLINDER_HPP_ */
