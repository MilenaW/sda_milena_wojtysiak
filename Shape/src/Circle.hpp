/*
 * Circle.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#include "Shape.hpp"
#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_

using namespace std;


class Circle: public Shape
{
protected:
	float mR;
	static float const PI = 3.14;
	string mNazwa="circle";
public:
	Circle();
	Circle(float r);
	virtual ~Circle();
	float pole();
	string getNazwa();
};

/* namespace std */

#endif /* CIRCLE_HPP_ */
