//============================================================================
// Name        : 03.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void szescian(double x, double* wynik)
{
	*wynik = x * x * x;
}

int main()
{
	std::cout << "podaj liczbe rzeczywista: " << std::endl;
	double liczba;
	std::cin >> liczba;

	double* wynik = new double;
	szescian(liczba, wynik);
	std::cout << *wynik << std::endl;
	delete wynik;
	return 0;
}
