/*
 * Professor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include <iostream>
#include  "Person.hpp"
#ifndef PROFESSOR_HPP_
#define PROFESSOR_HPP_

using namespace std;


class Professor: public Person
{
public:
	Professor();
	virtual ~Professor();
};

 /* namespace std */

#endif /* PROFESSOR_HPP_ */
