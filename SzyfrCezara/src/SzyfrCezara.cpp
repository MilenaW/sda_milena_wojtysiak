//============================================================================
// Name        : SzyfrCezara.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <cstring>
using namespace std;

class Wypisywalny
{
protected:
	string mLancuch;

public:
	Wypisywalny() :
			mLancuch("brak tekstu")
	{
	}
	Wypisywalny(string lancuch) :
			mLancuch(lancuch)
	{
		cout << "Tworze " << mLancuch << endl;
	}
	~Wypisywalny()
	{
		cout << "Niszcze " << mLancuch << endl;
	}

	const string& getLancuch() const
	{
		return mLancuch;
	}

	void setLancuch(const string& lancuch)
	{
		mLancuch = lancuch;
	}

	void wypisz()
	{
		cout << mLancuch << endl;
	}
};

class SzyfrowanyTekst: public Wypisywalny
{
protected:
	bool czyZaszyfrowany;
	string mSzyfrowanyTekst;
public:
	SzyfrowanyTekst()
	{
		cout << "konstruktor SzyfrowanyTekst" << endl;
	}
	~SzyfrowanyTekst()
	{
		cout << "destruktor SzyfrowanyTekst" << endl;
	}
	virtual void szyfruj()=0;
	virtual void wypisz()=0;
};

class CezarTekst: public virtual SzyfrowanyTekst
{
protected:
	char alfabet[26] =
	{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
			'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	int mPrzesuniecie;
	string mZaszyfrowanyTekst;
public:

	CezarTekst()
	{
		cout << "konstruktor CezarTekst" << endl;
	}
	CezarTekst(string szyfrowanyTekst, int przesuniecie) :
			mPrzesuniecie(przesuniecie)
	{
		mSzyfrowanyTekst = szyfrowanyTekst;
		cout << "konstruktor CezarTekst" << endl;
	}
	~CezarTekst()
	{
		cout << "destruktor CezarTekst" << endl;
	}

	void szyfruj()
	{

		for (int i = 0; i < mSzyfrowanyTekst.length(); i++)
		{
			int z = 0;
//			mZaszyfrowanyTekst += 'a'
//					+ (mSzyfrowanyTekst[i] - 'a' + mPrzesuniecie) % 26;

			for (int k = 0; k < 26; k++)
			{
				if (mSzyfrowanyTekst[i] == alfabet[k])
				{
					int zmienna;
					if ((k + 1) < 24)
						zmienna = k + 1;
					else if ((k + 1) >= 24)
						zmienna = k - 23;
					mZaszyfrowanyTekst += alfabet[zmienna];
					z++;
				}
				else
					continue;
			}
		}
	}
	void wypisz()
	{

//		cout << "Cezar Tekst zaszyfrowany ";
//		for (int i = 0; i < mSzyfrowanyTekst.length(); i++)
//		{
//			cout << mZaszyfrowanyTekst[i];
//		}
//		cout << endl;

		cout << "ZaSzyfrowany tekst: " << mZaszyfrowanyTekst << endl;
	}

	void wypiszSzyfrowany()
	{
		cout << "Szyfrowany tekst: " << mSzyfrowanyTekst << endl;
	}

	const string& getZaszyfrowanyTekst() const
	{
		return mZaszyfrowanyTekst;
	}

	void setZaszyfrowanyTekst(const string& zaszyfrowanyTekst)
	{
		this->mZaszyfrowanyTekst = zaszyfrowanyTekst;
	}
};

class PodstawieniowyTekst: public virtual SzyfrowanyTekst
{
protected:
	string mSzyfr = "qwertyuiopasdfghjklzxcvbnm";

public:
	PodstawieniowyTekst()
	{
		cout << "konstruktor PodstawieniowyTekst" << endl;
	}

	PodstawieniowyTekst(string lancuch)

	{
		mLancuch = lancuch;
		cout << "konstruktor PodstawieniowyTekst" << endl;
	}

	~PodstawieniowyTekst()
	{
		cout << "destruktor PodstawieniowyTekst" << endl;
	}
	void szyfruj()
	{
		for (int i = 0; i < mLancuch.length(); i++)
		{
			int pos = mLancuch[i] - 'b';
			mLancuch[i]= mSzyfr[pos];
		}
	}
	void wypisz()
	{
		cout <<"podst"<< mLancuch << endl;
	}

};
int main()
{

//	CezarTekst cezar("abcdefgxyz", 13);
//	cezar.wypiszSzyfrowany();
//	cezar.szyfruj();
//	cezar.wypisz();
//	PodstawieniowyTekst tekst("qwerty");
//	tekst.szyfruj();
//	tekst.wypisz();

	SzyfrowanyTekst*  tablica[4];
	CezarTekst cezar1("qwerty",1);
	CezarTekst cezar2("qwerty",2);
	PodstawieniowyTekst podst1 ("qwerty");
	PodstawieniowyTekst podst2("abcdefghijkl");
	tablica[0] = &cezar1;
	tablica[1] = &cezar2;
	tablica[2] = &podst1;
	tablica[3] = &podst2;

	for (int i = 0; i<4; ++i)
	{
		tablica[i]->szyfruj();
		tablica[i]->wypisz();
	}

	return 0;
}
