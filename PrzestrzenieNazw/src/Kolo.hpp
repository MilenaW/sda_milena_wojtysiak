/*
 * Kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_

#include <iostream>
#include "Figura.hpp"
#include "Kolor.hpp"
using namespace Kolor;
class Kolo: public Figura
{
private:
	Kolory mKolor;
	int mPromien;

public:
	Kolo(Kolory kolor, int promien);
	virtual ~Kolo();
	void wypisz();
};

#endif /* KOLO_HPP_ */
