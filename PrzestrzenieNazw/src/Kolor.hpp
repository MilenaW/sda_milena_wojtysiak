/*
 * Kolor.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLOR_HPP_
#define KOLOR_HPP_
#include <string>

namespace Kolor
{
enum Kolory
{
	czerwony, zielony, niebieski
};

std::string convertToString(Kolory kolor);
}


#endif /* KOLOR_HPP_ */
