/*
 * Kolor.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kolor.hpp"

std::string Kolor::convertToString(Kolory kolor)
{
	std::string ret = " ";

	switch (kolor)
	{
	case czerwony:
		ret= "czerwony";
		break;
	case zielony:
		ret= "zielony";
		break;
	case niebieski:
		ret= "niebieski";
		break;
	default:
		ret= "bezbarwny";
		break;
	}

return ret;
}

