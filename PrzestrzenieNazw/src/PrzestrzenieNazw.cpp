//============================================================================
// Name        : PrzestrzenieNazw.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Kolor.hpp"
#include "Kwadrat.hpp"
#include "Kolo.hpp"
#include "Figura.hpp"
//using namespace Kolor;

int main() {
	std::cout << "!!!Hello World!!!" << std::endl; // prints !!!Hello World!!!

	std::cout<<Kolor::convertToString(Kolor::czerwony)<<std::endl;
	Kwadrat kwa(niebieski,3);
	kwa.wypisz();
	Kolo kolo(czerwony,5);
	kolo.wypisz();

	Figura *figura=&kwa;
	figura->wypisz();
	figura=&kolo;
	figura->wypisz();
	return 0;
}
