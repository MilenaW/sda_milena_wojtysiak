/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_
#include <iostream>
#include "Kolor.hpp"
#include "Figura.hpp"
using namespace Kolor;

class Kwadrat: public Figura
{
private:
	Kolory mKolor;
	int mBok;
public:
	Kwadrat(Kolory kolor, int bok);
	virtual ~Kwadrat();
	void wypisz();
};

#endif /* KWADRAT_HPP_ */
