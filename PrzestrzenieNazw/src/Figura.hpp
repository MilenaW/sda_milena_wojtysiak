/*
 * Figura.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef FIGURA_HPP_
#define FIGURA_HPP_

class Figura
{
public:
	Figura();
	virtual ~Figura();
	void virtual wypisz()=0;
};

#endif /* FIGURA_HPP_ */
