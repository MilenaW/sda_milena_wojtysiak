/*
 * Logger.cpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#include "Logger.hpp"

Logger::Logger(string filename) :
		file(filename.c_str(), ofstream::out)
{


}
Logger::Logger(string filename, int logLevel) :
		file(filename.c_str(), ofstream::out)
, level (logLevel)
{

}

Logger::~Logger()
{
	file.close();
}

void Logger::Log(string message, int logLevel)
{
	if (level<logLevel)return;
	time_t t;
	t = time(NULL);
	struct tm* current = localtime(&t);

//	file<< "["<<t<<"]"<<message<<endl;

	file<<"[ "<<current->tm_year +1900<<"-"
			<<current->tm_mon+1<<"-"
			<<current->tm_mday<<" "
			<<current->tm_hour<<":"
			<<current->tm_min<<":"
			<<current->tm_sec<<" ] "<<message<<endl;




}
