/*
 * Logger.hpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#include <string>
#include <fstream>
#include <ctime>
using namespace std;
#ifndef LOGGER_HPP_
#define LOGGER_HPP_

class Logger
{
private:
	ofstream file;
	int level;


public:
	Logger(string filename);
	Logger(string filename, int logLevel);
	virtual ~Logger();
	void Log(string message, int logLevel);
};

#endif /* LOGGER_HPP_ */
