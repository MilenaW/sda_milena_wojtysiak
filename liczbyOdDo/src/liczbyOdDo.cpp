//============================================================================
// Name        : liczbyOdDo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void print(int x)
{
	cout<<x<<" ";
}

int main() {
	vector <int> range;
	vector <int> multi2;
	vector <int> multi3;

	int min = 1;
	int max = 10;

	for(int i= min; i<=max; i++ )
	{
		range.push_back(i);
	}

	for_each(range.begin(), range.end(), print);
	cout<<endl;

	for (vector<int>::iterator it = range.begin(); it!=range.end();++it)
	{
		if(*it%2==0)
		{
			multi2.push_back(*it);
		}
		if(*it%3==0)
		{
			multi3.push_back(*it);
		}
	}

	for_each(multi2.begin(), multi2.end(), print);
	cout<<endl;
	for_each(multi3.begin(), multi3.end(), print);
	cout<<endl;

	vector<int> v_intersection;

	set_intersection(multi2.begin(), multi2.end(),
	                 multi3.begin(), multi3.end(),
	                back_inserter(v_intersection));

	for_each(v_intersection.begin(), v_intersection.end(), print);
	cout<<endl;

	return 0;
}
