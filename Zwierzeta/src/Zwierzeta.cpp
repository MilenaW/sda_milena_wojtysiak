//============================================================================
// Name        : Zwierzeta.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

class DzikieZwierze
{
protected:
	string mImie;
public:
	DzikieZwierze()
	{
	}
	virtual ~DzikieZwierze()
	{
	}
	virtual void dajGlos()=0;
};

class Kot: public virtual DzikieZwierze
{
public:
	Kot()
	{
	}
	Kot(string imie)
	{
		mImie = imie;
	}
	~Kot()
	{
	}
	void dajGlos()
	{
		cout << "miau!" << endl;
	}
};

class Pies: public virtual DzikieZwierze
{
public:
	Pies()
	{
	}
	Pies(string imie)
	{
		mImie = imie;
	}
	~Pies()
	{
	}
	void dajGlos()
	{
		cout << "hauu!!!" << endl;
	}
};

class Kotopies:  public Kot, public Pies
{
public:
	Kotopies()
	{

	}
	Kotopies(string imie)
	{
		Kot::mImie = imie;
	}
	~Kotopies()
	{

	}

	void dajGlos()
	{
		cout << "miauhauu!!!" << endl;
	}
};

int main()
{
	Kot kot1;
	Kot kot2;
	Pies pies1;
	Pies pies2;
	Kotopies kotopies1;
	Kotopies kotopies2;

	DzikieZwierze* tablica[6];



	tablica[0] = &kot1;
	tablica[1] = &kot2;
	tablica[2] = &pies1;
	tablica[3] = &pies2;
	tablica[4] = &kotopies1;
	tablica[5] = &kotopies2;

	for (int i = 0; i < 6; ++i)
	{
		tablica[i]->dajGlos();
	}
	return 0;
}
