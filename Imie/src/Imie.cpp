//============================================================================
// Name        : Imie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

class Data
{
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)
	{
	setDzien( dzien);
	setMiesiac(miesiac);
	setRok(rok);
	}



unsigned int getDzien() const
{
	return mDzien;
}

void setDzien(unsigned int dzien)
{
	if (dzien < 1)
		mDzien = 1;
	else if (dzien > 31)
		mDzien = 31;
	else
		mDzien = dzien;

}

unsigned int getMiesiac() const
{

	return mMiesiac;
}

void setMiesiac(unsigned int miesiac)
{
	if (miesiac < 1)
		mMiesiac = 1;
	else if (miesiac > 12)
		mMiesiac = 12;
	else
		mMiesiac = miesiac;

}

unsigned int getRok() const
{
	return mRok;
}

void setRok(unsigned int rok)
{
	if (rok < 1970)
		mRok = 1970;
	else if (rok > 2017)
		mRok = 2017;
	else
		mRok = rok;

}
void pokazDate()
{
	cout<<getDzien()<<"-"<<getMiesiac()<<"-"<<getRok()<<endl;
}
};

class Osoba
{
private:
	string mImie;
	string mNazwisko;

public:
	Osoba (string imie, string nazwisko, Data data)
: mNazwisko(nazwisko)
, mImie(imie)
{
}

};

int main()
{
	Osoba osoba ("mddd","dddm",Data(1,2,1999));
	return 0;
}
