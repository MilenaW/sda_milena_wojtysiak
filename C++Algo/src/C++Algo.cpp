//============================================================================
// Name        : C++Algo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void print(int x)
{
	cout<<x<<" ";
}




int main() {

	vector<int> v(201);
	iota(v.begin(), v.end(), -100);
	for_each(v.begin(), v.end(),print);
	cout<<endl;

	cout<<"wszystkie >0? "<<all_of(v.begin(), v.end(),[](int i){if(i>0)return true; else return false;})<<endl;
	cout<<"jakas podzielna przez 3 i 5? "<<any_of(v.begin(), v.end(),[](int i){if(i%3==0&&i%5==0)return true; else return false;})<<endl;
	remove_if(v.begin(), v.end(),[](int i){if(i==0)return true; else return false;} );
	for_each(v.begin(), v.end(),print);
	cout<<endl;

	cout<<"sorted? "<<is_sorted(v.begin(), v.end())<<endl;
	vector<int> d;
	copy_if(v.begin(), v.end(),back_inserter(d),[](int i){if(i<-90||i>90)return true; else return false;});
	for_each(d.begin(), d.end(),print);
	cout<<endl;

	vector<int> s;
	auto szukana = find(v.begin(), v.end(), 78);
	copy_n(szukana, 10, back_inserter(d));
	for_each(d.begin(), d.end(),print);
	cout<<endl;

	cout<<v.size()<<endl;








	return 0;
}
