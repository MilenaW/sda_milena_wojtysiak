/*
 * Animal.hpp
 *
 *  Created on: 01.06.2017
 *      Author: RENT
 */

#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_



class Animal
{
protected:
	int mSerial;

public:
	Animal(int serial);
	virtual ~Animal();
	virtual void whoAreYou()=0;
	int getSerial() const;
	void setSerial(int serial);
};

#endif /* ANIMAL_HPP_ */
