/*
 * Bird.hpp
 *
 *  Created on: 01.06.2017
 *      Author: RENT
 */

#ifndef BIRD_HPP_
#define BIRD_HPP_

#include <iostream>

#include "Animal.hpp"

using namespace std;

class Bird: public Animal
{
public:
	Bird(int serial);
	virtual ~Bird();
	void whoAreYou();
};

#endif /* BIRD_HPP_ */
