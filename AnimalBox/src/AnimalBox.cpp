//============================================================================
// Name        : AnimalBox.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>
#include <list>
#include <cstdlib>
#include "Animal.hpp"
#include "Dog.hpp"
#include "Bird.hpp"
using namespace std;

Animal* randomAnimal(int serial)
{
	int number = rand()%2;

	if (number==0)
	{
		return new Dog(serial);
	}
	else
	{
		return new Bird(serial);
	}
}

int main() {


	srand(1);
	list< Animal* > box;

	for (int i =1; i<201; i++)
	{
		box.push_back(randomAnimal(i));
	}

	for (list< Animal*>::iterator it = box.begin(); it!=box.end(); it++)
	{
		cout<<(*it)->getSerial()<<" ";
		(*it)->whoAreYou();
	}

	Dog* d;
	Bird* b;

	list<Bird> birds;
	list<Dog> dogs;

	for (list< Animal*>::iterator it = box.begin(); it!=box.end(); it++)
	{
		d= dynamic_cast<Dog*>(*it);

		if(d)
		{
			dogs.push_back(*d);
			continue;
		}
		else
		{
			b= dynamic_cast<Bird*>(*it);
			if(b)
			{
				birds.push_back(*b);
			}
		}
	}

	for(list<Bird>::iterator it=birds.begin();it!=birds.end(); it++)
	{
		cout<<it->getSerial()<<" ";
		it->whoAreYou();
	}

	for(list<Dog>::iterator it=dogs.begin();it!=dogs.end(); it++)
	{
		cout<<it->getSerial()<<" ";
		it->whoAreYou();
	}

	for (list< Animal*>::iterator it = box.begin(); it!=box.end(); it++)
	{
		delete *it;
	}

	return 0;
}
