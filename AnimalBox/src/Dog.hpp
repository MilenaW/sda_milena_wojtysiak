/*
 * Dog.hpp
 *
 *  Created on: 01.06.2017
 *      Author: RENT
 */

#ifndef DOG_HPP_
#define DOG_HPP_

#include <iostream>

#include "Animal.hpp"

using namespace std;

class Dog: public Animal
{
public:
	Dog(int serial);
	virtual ~Dog();
	void whoAreYou();
};

#endif /* DOG_HPP_ */
