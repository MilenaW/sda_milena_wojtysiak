/*
 * Player.hpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include "Song.hpp"


class Player
{
private:
	std::shared_ptr<Song> mCurrent;
	std::vector<std::shared_ptr<Song>> mPlaylist;


public:
	Player();
	virtual ~Player();
	void next()
	{

	}
	void prev()
	{

	}
	void addSong(Song& song)
	{

		mPlaylist(std::shared_ptr<Song> (song));
	}
	void shuffle()
	{

	}
	bool features(std::string author)
	{
		return true;
	}




};

#endif /* PLAYER_HPP_ */
