/*
 * Song.hpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */

#ifndef SONG_HPP_
#define SONG_HPP_

#include <iostream>

class Song
{
private:
	std::string mTitle;
	std::string mName;
	int mTime;
public:
	Song(std::string title, std::string name, int time)
	: mTitle(title)
	, mName(name)
	, mTime(time)
	{

	}
	virtual ~Song();
};

#endif /* SONG_HPP_ */
