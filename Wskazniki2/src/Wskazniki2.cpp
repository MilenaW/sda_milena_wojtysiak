#include <iostream>
#include <string>

void zapytaj(std::string pytanie, int& i)
{
	std::cout<<pytanie<<std::endl;
	std::cin>>i;
}

void moduloDziesiec(int& j, int* tab)
{
	tab [0] = j%10;
}

void bityDrugiejCzworki(int& k, int tab[])
{
	tab [1] = k>>4 & 0xF;
}

int main(int argc, char* argv[])
{
	int liczba;
	zapytaj("Podaj liczbe:", liczba);

	int liczby[2];
	moduloDziesiec(liczba, liczby);
	bityDrugiejCzworki(liczba, liczby);
	std::cout << "x            = " << liczba << std::endl;
	std::cout << "x % 10       = " << liczby[0] << std::endl;
	std::cout << "x >> 4 & 0xF = " << liczby[1] << std::endl;

	return 0;
}
