//============================================================================
// Name        : 03_4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void srednia(int iloscLiczb, const double* tabela, double* suma)
{
	*suma = 0;
	for (int i = 0; i < iloscLiczb; ++i)
	{
		*suma += tabela[i];
	}
	*suma /= iloscLiczb;
}

int main()
{

	std::cout << "podaj ilosc liczb: " << std::endl;
	int iloscLiczb;
	std::cin >> iloscLiczb;

	double* tabela = new double[iloscLiczb];

	for (int i = 0; i < iloscLiczb; ++i)
	{
		std::cout << "podaj " << i + 1 << " liczbe " << std::endl;
		int liczba;
		std::cin >> liczba;
		tabela[i] = liczba;
	}
	double suma;
	srednia(iloscLiczb, tabela, &suma);
	std::cout << "srednia to " << suma << std::endl;

	delete[] tabela;

	return 0;
}
