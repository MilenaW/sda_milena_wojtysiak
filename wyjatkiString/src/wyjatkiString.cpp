//============================================================================
// Name        : wyjatkiString.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <typeinfo>
using namespace std;

int main() {
	string strg1("Tekst");
	string strg2("ing");
	try
	{
	strg1.append(strg2, 4, 2);
	}
	catch(std::out_of_range& ex)
	{
		std::cout<<"exception: "<<ex.what()<<std::endl;
	}
	catch (exception& ex)
	{
	cerr<<"Typu"<<typeid(ex).name()<<endl;
	cerr<<"Mam:"<<ex.what()<<endl;
	}

	cout<<strg1<<endl;
	return 0;
}
