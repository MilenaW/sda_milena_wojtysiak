//============================================================================
// Name        : wektorCw.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <iterator>
#include <deque>
#include <vector>
#include <algorithm>
using namespace std;

int main() {

deque<int> maly;
deque<int> duzy;

for(int i = 1;i<6;i++)
{
	maly.push_back(i);
	duzy.push_back(i*10);
}

//front_insert_iterator<deque<int> >frontIt(maly);
//back_insert_iterator<deque<int> >backIt(maly);
//copy(duzy.rbegin(), duzy.rend(), frontIt);
//copy(duzy.begin(), duzy.end(), backIt);
copy(duzy.begin(), duzy.end(), back_inserter(maly));


for(size_t s = 0; s<maly.size(); s++)
{
	cout<<maly[s]<<" ";
}

cout<<endl;

for (deque<int>::iterator it = maly.begin(); it!=maly.end(); advance (it,1))
{
	cout<<*it<<" ";
}

//copy(maly.begin(), maly.end(), ostreambuf_iterator<int>(cout,";"));
//
	return 0;
}
