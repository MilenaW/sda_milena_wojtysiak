//============================================================================
// Name        : Szablony.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

template<typename T>
void wypisz (T zmienna)
{
	cout<<zmienna<<endl;
}

template <int N, typename T>
void wypisz2 (T zmienna)
{
	for (int i = 0; i<N;i++)
	{
		cout<<zmienna;
	}
	cout<<endl;
}

//template<>
//void wypisz <10, string>(string zmienna)
//{
//	cout<<"Bailando!"<<endl;
//}

int main() {

	wypisz<int>(7);
	wypisz2<5,int>(68999);
	return 0;
}
