#include <iostream>
class Figura
{
public:
	virtual float pole() const= 0;

};

class Prostokat: public Figura
{
public:
	Prostokat(const float a, const float b) :
			mA(a), mB(b)
	{
	}

	float pole() const
	{
		return mA * mB;
	}
private:
	float mA;
	float mB;
};

class Trojkat: public Figura
{
public:
	Trojkat(const float a, const float h) :
			mA(a), mB(h)
	{
	}

	float pole() const
	{
		return 0.5 * mA * mB;
	}
private:
	float mA;
	float mB;
};

class Kolo: public Figura
{
public:
	Kolo(const float promien) :
			mR(promien)
	{
	}

	float pole() const
	{
		return pi * mR * mR;
	}
private:
	float mR; // promien kola
	const float pi = 3.14159;
};

void wyswietlPole(Figura &figura)
{
	std::cout << figura.pole() << std::endl;
}

int main()
{
	// deklaracje obiektow:
//	Figura jakasFigura;
	Prostokat jakisProstokat(5, 10);
	Kolo jakiesKolo(3);
	Trojkat jakisTrojkat (1,2);

	Figura *wskJakasFigura = 0; // deklaracja wska�nika

	// obiekty -------------------------------
//	std::cout << jakasFigura.pole() << std::endl;
	std::cout << jakisProstokat.pole() << std::endl;
	std::cout << jakiesKolo.pole() << std::endl;
	std::cout << jakisTrojkat.pole() << std::endl;

	// wskazniki -----------------------------
//	wskJakasFigura = &jakasFigura;
//	std::cout <<"pole figury "<< wskJakasFigura->pole() << std::endl;
	wskJakasFigura = &jakisProstokat;
	std::cout <<"pole prostokata "<< wskJakasFigura->pole() << std::endl;
	wskJakasFigura = &jakiesKolo;
	std::cout <<"pole kola "<< wskJakasFigura->pole() << std::endl;
	wskJakasFigura = &jakisTrojkat;
		std::cout <<"pole trojkata "<< wskJakasFigura->pole() << std::endl;

	// referencje -----------------------------
//	wyswietlPole(jakasFigura);
	wyswietlPole(jakisProstokat);
	wyswietlPole(jakiesKolo);
	wyswietlPole(jakisTrojkat);
	return 0;
}

