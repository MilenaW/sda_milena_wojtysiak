//============================================================================
// Name        : klasyCwiczenia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

class A
{
public:

	A()
	{
		std::cout << "Jestem A(), hello!" << std::endl;
	}
	~A()
	{
		std::cout << "Jestem ~A(), bye!" << std::endl;
	}
};

class B: public A
{
public:

	B()
	{
		std::cout << "Jestem B(), hello!" << std::endl;
	}
	~B()
	{
		std::cout << "Jestem ~B(), bye!" << std::endl;
	}
};

class Licznik
{
public:
	int licznik;

	Licznik()
	{
		licznik = 1;
		std::cout << "Jestem Licznik, hello!\nmoja wartosc to " <<licznik<< std::endl;
	}

	Licznik(const Licznik& other)
	{
		licznik = other.licznik +1;
		std::cout << "Jestem Licznik skopiowany, hello!\nmoja wartosc to " <<licznik<< std::endl;
	}

	~Licznik()
	{
		std::cout << "Jestem Licznik, bye!\nmoja wartosc to " <<licznik<< std::endl;
	}

};

class Pamiec
{
public:
	float* dane;
	int rodzajPamieci;

	Pamiec(int rodzajPamieci)
	: dane (new float),
	  rodzajPamieci (rodzajPamieci)
	{
		std::cout << "Jestem Pamiec, hello!" << std::endl;
	}
	Pamiec (const Pamiec& other )
	: rodzajPamieci (other.rodzajPamieci*2)
	{
		std::cout << "Jestem kopia Pamiec(), hello!" << rodzajPamieci<< std::endl;
		dane = new float;
		*dane = *other.dane;
	}

	Pamiec& operator=(const Pamiec& other)
	{
		delete dane;
		dane = new float;
		*dane = *other.dane;

		std::cout << "A& operator=(const A&)" << std::endl;
		rodzajPamieci = other.rodzajPamieci;
		return *this;
	}

	~Pamiec()
	{
		std::cout << "Jestem ~Pamiec(), bye!" << std::endl;
		delete dane;
	}
};

void funkcja (Licznik)
{
	std::cout << "****\npoczatek funkcji" << std::endl;
//	Licznik licznikFunkcja;
	std::cout << "koniec funkcji\n****" << std::endl;
}

void funkcjaPamiec (Pamiec pamiec)
{
	std::cout << "****\npoczatek funkcji" << std::endl;
	Pamiec pamiecWfunkcji = pamiec;
	std::cout << "koniec funkcji\n****" << std::endl;
}
int main() {
//	A a;
//	B b;
//	Licznik licznik;
	Pamiec pamiec(123);
	std::cout << "-------------------------------" << std::endl;
//	Licznik licznik2 = licznik;
//	Licznik licznik3 = licznik2;
//	funkcja (licznik);
	funkcjaPamiec(pamiec);
	Pamiec pamiec2 = pamiec;
	std::cout << "-------------------------------" << std::endl;
//	A *aa = new A();
//	B *bb = new B();
//	std::cout << "-------------------------------" << std::endl;
//	delete aa;
//	delete bb;
	Pamiec pamiec3(1);
	pamiec3=pamiec;
	std::cout << "-------------------------------" << std::endl;

	return 0;
}
