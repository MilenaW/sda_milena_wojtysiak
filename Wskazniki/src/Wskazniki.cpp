#include <iostream>

void zwiekszOPiec(int* i)
{

	*i+=5;
}

void kwadrat(int& j)
{
 j=j*j;

}
int main(int argc, char* argv[])
{
	std::cout << "Podaj liczbe: " << std::endl;
	int liczba;
	std::cin >> liczba;

	zwiekszOPiec(&liczba);
	kwadrat(liczba);

	std::cout << "(liczba+5)^2 = " << liczba << std::endl;

	return 0;
}

//	int a = 62;
//	int b = 81;
//	int* x = NULL;
//	x = &a;
//
//
//	cout << "a " << a << endl;
//	cout << "x " << x << endl;
//	cout << "*x " << *x << endl;
//	cout << "&a " << &a << endl;
//	cout << "x+1 " << x+1 << endl;
//	cout << "*x+1 " << *x+1 << endl;
//	cout << "&x+1 " << &x+1 << endl;
//	cout << "x[0]" << x[0] << endl;
//	cout << "x[1]" << x[1] << endl;
//	char k = 'k';
//	char l = 'l';
//	char m = 'm';
//	char n = 'n';
//	char*ch = NULL;
//
//	ch = &l;

//
//	return 0;
//}
