#include <iostream>
#include <exception>

using namespace std;

//enum AircraftError
//{
//  WingsOnFire = 1,
//  WingBroken = 2,
//  NoRunway = 3,
//  Crahed = 4
//};

class AircraftException: public exception
{
public:
	AircraftException(const char* errMessage) :
			m_ErrMessage(errMessage)
	{
	}
	// overriden what() method from exception class
	const char* what() const throw ()
	{
		return m_ErrMessage;
	}

protected:
	const char* m_ErrMessage;
};

class WingsOnFire: public AircraftException
{
public:
	WingsOnFire(const char* errMessage) :
			AircraftException(errMessage)
	{
	}
};

class WingBroken: public AircraftException
{
public:
	WingBroken(const char* errMessage) :
			AircraftException(errMessage)
	{
	}
};

int main()
{
	try
	{
		throw WingsOnFire("crashed");
	}
	catch (WingsOnFire& e)
	{
		cout << e.what() << '\n';
	}
	try
	{
		throw WingBroken("pray!!!");
	}
	catch (WingBroken&e)
	{
		cout << e.what() << '\n';
	}

	return 0;
}
//  catch (AircraftException& e)
//  {
//    cout << e.what() << '\n';
//    if (e.GetError() == WingsOnFire)
//    {
//      // Fire extinguishers
//    }
//    else if (e.GetError() == WingBroken)
//    {
//      // Cannot do anything in flight - pray and rethrow
//    }
//    else if(e.GetError()== NoRunway)
//    {
//      //Call Air Traffic control to clear up runway
//    }
//    else
//    {
//      // We have crashed - throw
//      throw;
//    }
//  }

