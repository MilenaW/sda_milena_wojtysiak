#ifndef DZIEDZICZENIE_HPP_
#define DZIEDZICZENIE_HPP_

#include <iostream>
#include <string>
using namespace std;

class Poczatek
{
protected:
	int mX;
	string mTekst;

public:
	void przedstawSie()
	{
		cout<<"Jestem Poczatek \n";
	}

	Poczatek()
	: mX(0)
	, mTekst("Litwo")
	{
		cout<<"Poczatek\n";
	}

	Poczatek(int x)
	: mX(x)
	, mTekst("Aurea")
	{
		cout<<"Poczatek(x)\n";
	}

	~Poczatek()
	{
		cout<<"~Poczatek\n";
	}

	void ustawX()
	{
		mX = 1;
	}

	void wypiszX()
	{
		std::cout<<"Poczatek::X="<<mX<<endl;
	}

	void ustawTekst(std::string tekst)
	{
		mTekst=tekst;
	}
	void wypiszTekst()
	{
		std::cout<<"Poczatek::Tekst= "<<mTekst<<endl;
	}

};

class Srodek : public Poczatek
{
protected:
	double mX;
	string mTekst;

public:
	void przedstawSie()
	{
		cout<<"Jestem Srodek \n";
	}

	Srodek()
	: Poczatek(99)
	, mX(10.31)
	, mTekst("Ojczyzno")
	{
		cout<<"Srodek\n";
	}

	Srodek(double x)
	: Poczatek(33)
	, mX(x)
	, mTekst("prima")
	{
		cout<<"Srodek(x)\n";
	}

	~Srodek()
	{
		cout<<"~Srodek\n";
	}

	void ustawX()
	{
		Poczatek::mX = 11;
		mX = 12.31;
	}

	void wypiszX()
	{
		std::cout<<"Poczatek::X= "<<Poczatek::mX<<" Srodek::X= "<<mX<<endl;
	}

	void ustawTekst(std::string tekst)
	{
		Poczatek::mTekst = "Poczatek "+tekst;
		mTekst=tekst;
	}
	void wypiszTekst()
	{
		std::cout<<"Poczatek::Tekst= "<<Poczatek::mTekst<<" Srodek::Tekst= "<<mTekst<<endl;
	}

};

class Koniec : public Srodek
{
protected:
	int mX;
	string mTekst;

public:
	void przedstawSie()
	{
		cout<<"Jestem Koniec \n";
	}

	Koniec()
	: Srodek(3213.333)
	, mX(100)
	, mTekst("Moja")
	{
		cout<<"Koniec\n";
	}

	Koniec(int x)
	: Srodek(3213.333)
	, mX(x)
	, mTekst("sata")
	{
		cout<<"Koniec(x)\n";
	}

	~Koniec()
	{
		cout<<"~Koniec\n";
	}

	void ustawX()
	{
		Poczatek::mX = 111;
		Srodek::Poczatek::mX = 112.432;
		mX = 113;
	}

	void wypiszX()
	{
		std::cout<<"Poczatek::X= "<<Poczatek::mX<<" Srodek::X= "<<Srodek::mX<<" Koniec::X= "<<mX<<endl;
	}

	void ustawTekst(std::string tekst)
	{
		Poczatek::mTekst = "Poczatek "+ tekst;
		Srodek::mTekst = "Srodek " + tekst;
		mTekst=tekst;
	}
	void wypiszTekst()
	{
		std::cout<<"Poczatek::Tekst= "<<Poczatek::mTekst<<" Srodek::Tekst= "<<Srodek::mTekst<<" Koniec::Tekst "<<mTekst<<endl;
	}

};

#endif /* DZIEDZICZENIE_HPP_ */
