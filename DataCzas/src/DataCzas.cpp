//============================================================================
// Name        : DataCzas.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Data.cpp"
#include "Czas.cpp"
using namespace std;

class DataCzas: public Data, public Czas
{
public:
	DataCzas()
	{
		setData(1, 1, 1900);
		setCzas(0, 0, 0);
	}
	DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok,
			unsigned int god, unsigned int min, unsigned int sek)
	{
		setData(dzien, miesiac, rok);
		setCzas(god, min, sek);
	}
	DataCzas(Data data, Czas czas)
	{
		setData(data.getDzien(),data.getMiesiac(), data.getRok());
		setCzas(czas.getGod(), czas.getMin(), czas.getSek());
	}

	Data getData()
	{
		return Data(mDzien, mMiesiac, mRok);
	}

	Data getCzas()
	{
		return Data(mGod, mMin, mSek);
	}

	void setData(unsigned int dzien, unsigned int miesiac, unsigned int rok)
	{
		setDzien(dzien);
		setMiesiac(miesiac);
		setRok(rok);
	}
	void setCzas(unsigned int god, unsigned int min, unsigned int sek)
	{
		setGod(god);
		setMin(min);
		setSek(sek);
	}
	void wypiszDataCzas()
	{
		DataCzas::wypiszData();
		DataCzas::wypiszCzas();
	}
	DataCzas roznicaDataCzas(DataCzas dataCzas1)
	{
		unsigned int roznicaDni = dataCzas1.getDzien() - getDzien();
		unsigned int roznicaMiesiecy = dataCzas1.getMiesiac() - getMiesiac();
		unsigned int roznicaLat = dataCzas1.getRok() - getRok();
		unsigned int roznicaGod=dataCzas1.getGod()-getGod();
		unsigned int roznicaMin=dataCzas1.getMin()-getMin();
		unsigned int roznicaSek=dataCzas1.getSek()-getSek();
		DataCzas dataCzas (roznicaDni, roznicaMiesiecy, roznicaLat,roznicaGod,roznicaMin,roznicaSek);
		return dataCzas;
	}
	void przesunDataCzas(DataCzas dataCzas1)
	{
		przesunSek (dataCzas1.getSek());
		przesunMin (dataCzas1.getMin());
		przesunGod (dataCzas1.getGod());
		przesunDzien (dataCzas1.getDzien());
		przesunMiesiac (dataCzas1.getMiesiac());
		przesunRok (dataCzas1.getRok());
	}
};

//int main()
//{
//	DataCzas dataCzas1;
//	dataCzas1.wypiszDataCzas();
//	DataCzas dataCzas2(Data(9,9,1999), Czas(9,9,9));
//	dataCzas2.wypiszDataCzas();
//
//	DataCzas roznica = dataCzas1.roznicaDataCzas(dataCzas2);
//	roznica.wypiszDataCzas();
//	dataCzas1.przesunDataCzas(dataCzas2);
//	dataCzas1.wypiszDataCzas();
//	return 0;
//}
