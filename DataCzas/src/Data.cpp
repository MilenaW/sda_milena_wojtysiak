//============================================================================
// Name        : Data.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;


class Data
{
protected:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data()
	{
		setDzien( 1);
			setMiesiac(1);
			setRok(1900);
	}

	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)
	{
	setDzien( dzien);
	setMiesiac(miesiac);
	setRok(rok);
	}



unsigned int getDzien() const
{
	return mDzien;
}

void setDzien(unsigned int dzien)
{
	if (dzien < 1)
		mDzien = 1;
	else if (dzien > 31)
		mDzien = 31;
	else
		mDzien = dzien;

}

unsigned int getMiesiac() const
{

	return mMiesiac;
}

void setMiesiac(unsigned int miesiac)
{
	if (miesiac < 1)
		mMiesiac = 1;
	else if (miesiac > 12)
		mMiesiac = 12;
	else
		mMiesiac = miesiac;

}

unsigned int getRok() const
{
	return mRok;
}

void setRok(unsigned int rok)
{
	if (rok < 1900)
		mRok = 1900;
	else if (rok > 2017)
		mRok = 2017;
	else
		mRok = rok;

}
void wypiszData()
{
	cout<<getDzien()<<"-"<<getMiesiac()<<"-"<<getRok()<<endl;
}

void przesunRok (unsigned int r)
{
	mRok+=r;
}

void przesunMiesiac (unsigned int m)
{
	mRok+=m/12;
	mMiesiac+=m%12;
}

void przesunDzien (unsigned int d)
{
	mRok+=d/365;
	mMiesiac += (d%365)/31;
	mDzien+=(d%365)%31;
}

Data roznicaDat(Data data1)
{
	unsigned int roznicaDni=data1.getDzien()-getDzien();
	unsigned int roznicaMiesiecy=data1.getMiesiac()-getMiesiac();
	unsigned int roznicaLat=data1.getRok()-getRok();
	Data data(roznicaDni,roznicaMiesiecy,roznicaLat);
	return data;
}
};

//int main() {
//	Data data1(2,3,1999);
//	Data data2(1,2,1998);
//	data1.wypiszData();
//	data2.wypiszData();
//
//	Data roznica = data2.roznicaDat(data1);
//	roznica.wypiszData();
//	data1.przesunDzien(31);
//	data1.wypiszData();
//	return 0;
//}
