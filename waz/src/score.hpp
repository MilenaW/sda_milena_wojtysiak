#ifndef _SCORE_HPP_
#define _SCORE_HPP_

struct score {
  int score;
};
/**
 * Reset score to zero.
 */
void score_clear(score *score);

/**
 * Update score by adding value to it.
 */
void score_update(score *score, int value);

/**
 * Return the current score.
 */
int score_current(const score *score);

#endif /* _SCORE_HPP_ */
