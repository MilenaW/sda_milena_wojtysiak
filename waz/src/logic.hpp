#ifndef _LOGIC_HPP_
#define _LOGIC_HPP_

#include "board.hpp"
#include "gobject.hpp"
#include "score.hpp"
#include "globals.hpp"

void logic(board *board, gobject *snake, gobject *fruit, score *score,
		state *state);

#endif /* _LOGIC_HPP_ */
