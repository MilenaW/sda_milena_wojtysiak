/*
 * CardGame_Test.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */
#include "gtest/gtest.h"
#include "Card.hpp"
#include "Deck.hpp"

TEST(CardClassTest, cardCreation)
{
	Card card;
	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::colourEnd, card.getColour());
	EXPECT_EQ(Card::figureEnd, card.getFigure());
}

TEST(CardClassTest, cardSetValues)
{
	Card card;
	card.setValues(Card::hearts,Card::two);
	EXPECT_EQ(2, card.getValue());
	EXPECT_EQ(Card::hearts, card.getColour());
	EXPECT_EQ(Card::two, card.getFigure());
}

TEST(DeckClassTest, deckGetNextCard)
{
	Deck deck;
	Card card = deck.getNextCard();

	EXPECT_NE(0, card.getValue());
	EXPECT_NE(Card::colourEnd, card.getColour());
	EXPECT_NE(Card::figureEnd, card.getFigure());
}
TEST(DeckClassTest, deckGetNextCard52)
{
	Deck deck;
	Card card;
	for (int i=0; i<52;i++)
	{
		card = deck.getNextCard();
	}
	EXPECT_NE(0, card.getValue());
	EXPECT_NE(Card::colourEnd, card.getColour());
	EXPECT_NE(Card::figureEnd, card.getFigure());
}

TEST(DeckClassTest, deckGetNextCardMore53)
{
	Deck deck;
	Card card;
	for (int i=0; i<=53;i++)
	{
		card = deck.getNextCard();
	}
	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::colourEnd, card.getColour());
	EXPECT_EQ(Card::figureEnd, card.getFigure());
}

int main(int argc, char **argv)
{
::testing::InitGoogleTest(&argc, argv);
return RUN_ALL_TESTS();
}
