/*
 * Card.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef CARD_HPP_
#define CARD_HPP_

class Card
{

public:
	enum Colour
	{
		hearts = 0, tiles, clovers, Pikes, colourEnd
	};
	enum Figure
	{
		two = 0,
		three,
		four,
		five,
		six,
		seven,
		eight,
		nine,
		ten,
		jack,
		queen,
		king,
		ace,
		figureEnd
	};

	Card();

	Card(Colour colour, Figure figure);

	virtual ~Card();
	Colour getColour() const;
	Figure getFigure() const;
	int getValue() const;
	void setValues(Colour color, Figure figure);


private:

	Colour mColour;
	Figure mFigure;
	int mValue;

	int convertFigureToValue(Figure figure);

};

#endif /* CARD_HPP_ */
