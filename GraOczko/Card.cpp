/*
 * Card.cpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "Card.hpp"

	Card::Card()
	: mColour(colourEnd)
	, mFigure(figureEnd)
	, mValue(0)
	{
	}

Card::Card(Colour colour, Figure figure) :
		mColour(colourEnd), mFigure(figureEnd), mValue(0)
{
	// TODO Auto-generated constructor stub

}

Card::~Card()
{
	// TODO Auto-generated destructor stub
}

Card::Colour Card::getColour() const
{
	return mColour;
}

Card::Figure Card::getFigure() const
{
	return mFigure;
}

int Card::getValue() const
{
	return mValue;
}

void Card::setValues(Colour color, Figure figure)
{
	mColour = color;
	mFigure = figure;
	mValue = convertFigureToValue(figure);
}

int Card::convertFigureToValue(Figure figure)
{
	int value = 0;

	switch (figure)
	{
	case two:
	case three:
	case four:
	case five:
	case six:
	case seven:
	case eight:
	case nine:
	case ten:
		value = 2 + figure;
		break;
	case jack:
	case queen:
	case king:
		value = 10;
		break;
	case ace:
		value = 11;
		break;

	}

	return value;
}
