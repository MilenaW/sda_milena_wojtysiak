/*
 * Deck.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef DECK_HPP_
#define DECK_HPP_
#include <cstdlib>
#include <ctime>
#include "Card.hpp"
class Deck
{
private:
	Card* mCards;
	int mCardCount;
	int mCurrentCard;
public:
	Deck();
	virtual ~Deck();
	void shuffle();
	Card getNextCard();
};

#endif /* DECK_HPP_ */
