/*
 * Deck.cpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "Deck.hpp"
#include "Card.hpp"

Deck::Deck() :
		mCards(0), mCardCount(0), mCurrentCard(0)
{
	srand(time( NULL));
	mCardCount = Card::colourEnd * Card::figureEnd;
	mCards = new Card[mCardCount];
	for (int c = 0; c < Card::colourEnd; c++)
	{
		for (int f = 0; f < Card::figureEnd; f++)
		{
			mCards[c * Card::figureEnd + f].setValues((Card::Colour) c,
					(Card::Figure) f);
		}
	}

}

Deck::~Deck()
{
	delete[] mCards;
}

void Deck::shuffle()
{
	for (int i = mCardCount; i > 0; i--)
	{
		int pos = rand() % mCardCount;
		Card tmp = mCards[i - 1];
		mCards[i - 1] = mCards[pos];
		mCards[pos] = tmp;
	}
}

Card Deck::getNextCard()
{
	Card chosenCard;
	if (mCurrentCard < mCardCount)
	{
		mCurrentCard++;
		chosenCard = mCards[mCurrentCard - 1];
	}
	else
	{
		//nic, bo b��d
	}
	return chosenCard;
}
