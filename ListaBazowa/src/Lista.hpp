/*
 * Lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_

class Lista
{
private:
	class Wezel
	{
	public:
		int mWartosc;
		Wezel * nast;

		Wezel(int wartosc)
		{
			mWartosc = wartosc;
			nast = 0;
		}

		int getWartosc() const;
		void setWartosc(int wartosc);
	};

	Wezel*head;
	Wezel*tail;

public:
	Lista();
	~Lista();


	void wypisz();

	void dodajPoczatek(int wartosc);
	void dodajKoniec(int wartosc);
	int pobierz (int indeks);
	bool czyPusta();
	void wyczysc();
	void usun (int indeks);
	int znajdzPozycje(int wartosc);
};

#endif /* LISTA_HPP_ */
