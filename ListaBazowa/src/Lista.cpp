/*
 * Lista.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "Lista.hpp"

Lista::Lista()
{
	head = 0;
	tail = 0;
}

Lista::~Lista()
{
}

void Lista::wypisz()

{
	Wezel*tmp = head;
	while (tmp != 0)
	{
		std::cout << "[" << tmp->mWartosc << "]";
		tmp = tmp->nast;
	}
	std::cout << std::endl;
}

void Lista::dodajPoczatek(int wartosc)
{
	if (head != 0)
	{
		Wezel*tmp = new Wezel(wartosc);
		tmp->nast = head;
		head = tmp;
	}
	else
	{
		head = new Wezel(wartosc);
		tail = head;
	}
}
void Lista::dodajKoniec(int wartosc)
{
	if (head != 0)
	{
		Wezel*tmp = new Wezel(wartosc);
		tail->nast = tmp;
		tail = tmp;
		tmp->nast = 0;
	}
	else
	{
		head = new Wezel(wartosc);
		tail = head;
	}
}
int Lista::pobierz(int indeks) //funkcja zwracajaca wartosc elemetnu pod pozycja n
{
	Wezel*tmp = head;
	for (int i = 0; i < indeks - 1; ++i)
	{
		tmp = tmp->nast;
	}
	std::cout << "wartosc " << indeks << " elementu: " << tmp->mWartosc
			<< std::endl;

	return tmp->mWartosc; //ma zwracac wartosc wezla
}

bool Lista::czyPusta() // funkcja zwracajaca true jesli lista jest pusta
{
	if (head != 0)
		return false;
	else
		return true; //ma zwracac faktyczna wartosc
}

void Lista::wyczysc() //funkcja usuwajace wszystkie Wezly z listy
{

	while (head != tail)
	{
		Wezel*tmp = head;
		head = tmp->nast;
		delete tmp;
	}
	delete tail;
	head=tail=0;
}

void Lista::usun(int indeks) //funkcja ususwajace element na podanej pozycji od head
{

	Wezel*tmp = head;
	for (int i = 0; i < indeks - 2; ++i)
	{
		tmp = tmp->nast;

	}
	std::cout << "wartosc " << indeks << " elementu: " << tmp->mWartosc
			<< "do usuniecia" << std::endl;

	Wezel*poprzedni = tmp;

	tmp = head;
	for (int i = 0; i < indeks - 1; ++i)
	{
		tmp = tmp->nast;

	}

	Wezel*doUsuniecia = tmp;

	tmp = head;
	for (int i = 0; i < indeks; ++i)
	{
		tmp = tmp->nast;

	}

	Wezel*nastepny = tmp;
	poprzedni->nast = nastepny;
	delete doUsuniecia;
}

int Lista::znajdzPozycje(int wartosc) //znajdz indeks elementu o podanej wartosci
{
	int pozycja = 0; //0 jesli element nie zostal znaleziony

	//Praca domowa - podobnie do metody pobierz
	Wezel* tmp = head;
	int licznikOdwiedzin = 0;
	while (tmp != 0)  //dopoki nie odwiedzilismy ostatniego wezla
	{
		licznikOdwiedzin++;
		if (wartosc == tmp->mWartosc)
		{
			return licznikOdwiedzin;
		}
		else
		{
			tmp = tmp->nast; //jesli nie to pobieram nastpeny wezel
		}
	}
	return pozycja;
}

int Lista::Wezel::getWartosc() const
{
	return mWartosc;
}

void Lista::Wezel::setWartosc(int wartosc)
{
	mWartosc = wartosc;
}
