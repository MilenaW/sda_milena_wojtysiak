/*
 * Lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_

#include <iostream>

class Lista
{
private:
	class Wezel
	{
	public:
		int mWartosc;
		Wezel * nast;

		Wezel(int wartosc)
		{
			mWartosc = wartosc;
			nast = 0;
		}
	};

	Wezel* head; //wksaznik na pierwszy element
	Wezel* tail; //wskaznik na ostatni element

public:
	Lista()
	{
		head = 0;
		tail = 0;
	}

	void wypisz()
	{
		Wezel* tmp = 0; //tmo b�dzie wskaznikiem na aktualnie przegladany wezel
		tmp = head; //ustaw aktualny przegladany wezel na poczatek listy

		std::cout << "{ ";
		while (tmp != 0) //test czy nie jest to ostatni element
		{
			std::cout << "[" << tmp->mWartosc << "] ";
			tmp = tmp->nast; //pobierz nastepny element z listy
		}
		std::cout << " }" << std::endl;
	}

	void dodajPoczatek(int wartosc)
	{
		if (head != 0) //sprawdz czy nasza lista jest pusta
		{ //jak nie jest pusta
			Wezel* nowyWezel = new Wezel(wartosc); //stw�rz nowy w�zel a nast�pnie przypisz go do tymczasowego wskaznika
			nowyWezel->nast = head; //Niech nowy wezel wskazuje na to na co wskazuje czo�o listy
			head = nowyWezel; //Niech czo�o listy wskazuje teraz na nasz nowy Wezel
		}
		else
		{ //jak jest pusta
			head = new Wezel(wartosc); //stw�rz nowy Wezel, nastepnie niech czo�o na niego wskazuje
			tail = head; //ustaw ogon na nowy element (czyli na ten ktory wskazuje head)_
		}
	}

	void dodajKoniec(int wartosc)
	{
		if (head != 0) //sprawdz czy nasza lista jest pusta
		{ //jak nie jest pusta
			Wezel* nowyWezel = new Wezel(wartosc); //stw�rz nowy w�zel a nast�pnie przypisz go do tymczasowego wskaznika
			tail->nast = nowyWezel; //Niech to na cooogn listy wskazuje teraz na nasz nowy Wezel
			tail = nowyWezel;
		}
		else
		{ //jak jest pusta
			head = new Wezel(wartosc); //stw�rz nowy Wezel, nastepnie niech czo�o na niego wskazuje
			tail = head; //ustaw ogon na nowy element (czyli na ten ktory wskazuje head)_
		}
	}

	int pobierz (int indeks) //funkcja zwracajaca wartosc elemetnu pod pozycja n
	{
		Wezel* tmp = head; //Stworz tymczasowy wezel wskauzjacy na pierwszy element

		int licznikOdwiedzin = 0; //Stw�rz pomocnicz� zmienn� zliczaj�c� liczb� odwiedzonych wez��w

		while(tmp!=0)  //dopoki nie odwiedzilismy ostatniego wezla
		{
			licznikOdwiedzin++; //zwiekszamy licznik odwiedzonych element�w
			if(licznikOdwiedzin==indeks) //sprawdzam czy odwiedzilem wymagana liczbe wezlow
			{
				return tmp->mWartosc; //jesli tak to zwracam jego wartosc
			}
			else
			{
				tmp=tmp->nast; //jesli nie to pobieram nastpeny wezel
			}
		}

		return 0; //ma zwracac wartosc wezla
	}

	int znajdzPozycje(int wartosc) //znajdz indeks elementu o podanej wartosci
	{
		int pozycja = 0; //0 jesli element nie zostal znaleziony

		//Praca domowa - podobnie do metody pobierz

		return pozycja;
	}

	bool czyPusta() // funkcja zwracajaca true jesli lista jest pusta
	{
		if (head != 0 ) //moze byc tez tail
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	void wyczysc() //funkcja usuwajace wszystkie Wezly z listy
	{
		Wezel* tmp = 0; //tmo b�dzie wskaznikiem na aktualnie przegladany wezel
		tmp = head; //ustaw aktualny przegladany wezel na poczatek listy

		while (tmp != 0) //test czy nie jest to ostatni element
		{
			head = tmp->nast;
			delete tmp;
			tmp = head; //pobierz nastepny element z listy
		}

		head = 0;
		tail = 0;
	}

	void usun (int indeks) //funkcja ususwajace element na podanej pozycji od head
	{
		Wezel* tmp = head; //Stworz tymczasowy wezel wskauzjacy na pierwszy element
		Wezel* pop = 0;
		int licznikOdwiedzin = 0; //Stw�rz pomocnicz� zmienn� zliczaj�c� liczb� odwiedzonych wez��w

		while(tmp!=0)  //dopoki nie odwiedzilismy ostatniego wezla
		{
			licznikOdwiedzin++; //zwiekszamy licznik odwiedzonych element�w
			if(licznikOdwiedzin==indeks) //sprawdzam czy odwiedzilem wymagana liczbe wezlow
			{
				pop->nast = tmp->nast;
				delete tmp;
			}
			else
			{
				pop = tmp;
				tmp=tmp->nast; //jesli nie to pobieram nastpeny wezel
			}
		}

	}

};

#endif /* LISTA_HPP_ */
