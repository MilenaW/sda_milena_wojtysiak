#include <iostream>
#include "Ser.hpp"
#include "SerBialy.hpp"
#include "SerZolty.hpp"
using namespace std;

int policz_literke(const char* napis, char literka) {

    int n = 0;//dlugosc napisu
    int i = 0;
//    char* c = const_cast<char*>(napis);
    while (napis[i]) {

        if (napis[i++] == literka)
        	{
        	n++;

        	}
    }

    return n;
}

void sertuj(Ser** tab, int rozmiar)
	{
		SerBialy** bialaTab = new SerBialy*[rozmiar];
		SerZolty** zoltaTab = new SerZolty*[rozmiar];

		int ileBialych=0;
		int ileZoltych=0;

		SerBialy* bialyTmp;
		SerZolty* zoltyTmp;

		for(int i =0; i<rozmiar; i++)
		{
			//rodziela wszystkie sery do odpwiendich tablic

			zoltyTmp = dynamic_cast<SerZolty*>(tab[i]);
			if (zoltyTmp)
			{
				zoltaTab[ileZoltych++]=zoltyTmp;
				continue;
			}

			bialyTmp = dynamic_cast<SerBialy*>(tab[i]);

			if (bialyTmp)
			{
				bialaTab[ileBialych++]= bialyTmp;

			}

		}

		for(int i =0; i<ileBialych; i++)
		{
			cout<<"Bialy!"<<endl;
				bialaTab[i]->podajCene();
				bialaTab[i]->podajRodzaj();
				cout<<endl;
	//		wypisujemy cene i rodzaj serow bialych
		}

		for(int i =0; i<ileZoltych; i++)
		{
			cout<<"Zolty!"<<endl;
			zoltaTab[i]->podajCene();
			zoltaTab[i]->podajWiek();
			cout<<endl;
	//		wypisujemy cene i wiek serow zoltych
		}
		delete[] bialaTab;
		delete[] zoltaTab;
	}

int main() {
	cout<<"alabama"<<policz_literke("alabama", 'a')<<endl;
//	 int liczba, liczba2;
//	  std::cin >> liczba >> liczba2;
//	  double wynik = static_cast<double>(liczba) /static_cast<double>(liczba2);
//	  std::cout << wynik << std::endl;

	Ser* sery[8];

	sery[0] = new SerBialy(10, SerBialy::chudy);
	sery[1] = new SerZolty(301, 23);
	sery[2] = new SerZolty(10, 1);
	sery[3] = new SerBialy(8, SerBialy::tlusty);
	sery[4] = new SerBialy(9, SerBialy::tlusty);
	sery[5] = new SerBialy(15, SerBialy::poltlusty);
	sery[6] = new SerBialy(13, SerBialy::chudy);
	sery[7] = new SerZolty(2000, 33);

	sertuj(sery, 8);

	return 0;
}
