/*
 * SerZolty.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERZOLTY_HPP_
#define SERZOLTY_HPP_
#include <iostream>
#include "Ser.hpp"

class SerZolty: public Ser
{
private:
	int mWiek=-1;
public:
	SerZolty(int cena, int wiek);
	virtual ~SerZolty();
	void podajCene();
	void podajWiek();
};

#endif /* SERZOLTY_HPP_ */
