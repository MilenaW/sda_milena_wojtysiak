/*
 * SerBialy.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERBIALY_HPP_
#define SERBIALY_HPP_
#include <iostream>
#include "Ser.hpp"
#include "SerZolty.hpp"



class SerBialy: public Ser
{
public:
	enum rodzaj
	{
		chudy,
		poltlusty,
		tlusty
	};
	SerBialy(int cena, rodzaj rodz);
	virtual ~SerBialy();
	void podajCene();
	void podajRodzaj();




private:
	rodzaj mRodzaj;


};



#endif /* SERBIALY_HPP_ */
