/*
 * Ser.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_HPP_
#define SER_HPP_

class Ser
{
protected:
	int mCena;

public:
	Ser(int cena);
	virtual ~Ser();
	virtual void podajCene() = 0;
};

#endif /* SER_HPP_ */
