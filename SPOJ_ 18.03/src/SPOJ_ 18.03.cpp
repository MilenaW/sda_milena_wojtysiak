//============================================================================
// Name        : 03.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int podstaw(string s, char *tabLit, int *tabCyf)
{
	int result = 0;

	for (unsigned int i = 0; i < s.length(); ++i)
	{
		for (int unsigned k = 0; k < 23; ++k)
		{
			if (s[i] == tabLit[k])
			{
				result += tabCyf[k];
			}
			else
				continue;
		}
	}

	return result;
}

int main()
{

	char tablicaLiter[] =
	{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p',
			'q', 'r', 's', 't', 'v', 'x', 'y', 'z' };
	int tablicaCyfr[] =
	{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200,
			300, 400, 500 };

	string slowo;
	cin>>slowo;

	cout<<podstaw(slowo, tablicaLiter, tablicaCyfr)<<endl;


	return 0;
}
