/*
 * Pieniadz.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PIENIADZ_HPP_
#define PIENIADZ_HPP_

class Pieniadz
{
public:
	double mWartosc;
	int mIlosc;
	Pieniadz();
	Pieniadz(int ilosc, double wartosc);
	virtual ~Pieniadz();
};

#endif /* PIENIADZ_HPP_ */
