/*
 * Osoba.hpp
 *
 *  Created on: 25.05.2017
 *      Author: RENT
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_
#include <iostream>
#include <vector>

using namespace std;

class Osoba
{

public:
	Osoba(string imie, int wiek);
	virtual ~Osoba();
	string mImie;
	int mWiek;
};

#endif /* OSOBA_HPP_ */
