#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

//ZAD for_each liczacy sredni wiek (functor)
//ZAD dodac kilka innych pol innego typu
struct Person
{
    Person(string name, int age)
    : mName(name)
    , mAge(age)
    {}

    string mName;
    int mAge;
};

int main()
{
    std::vector<Person> persons; //vector przechowujacy dane z pliku

    ifstream ifs; // obiekt streamu wczytujace z pliku
    ifs.open("qwerty.txt"); //otwarcie pliku

    if (!ifs.good()) //sprawdzenie czy plik jest poprawny
    {
        cerr << "Error reading file"<<endl; //komunikat o bledzie
        return 1;
    }


    string name; // zmienna pomocnicza
    int age; //zmienna pomocnicza
    while(!ifs.eof())
    {
        //ZAD modyfikacja tak aby wczytywa� z separatorem
        ifs>>name>>age; //wczytawanie operatorem >>
        persons.push_back(Person(name,age)); // wrzucenie do kontenera
    }

    ifs.close(); // zamkniecie pliku

    for(vector<Person>::iterator it = persons.begin(); it!= persons.end(); ++it)
    {
        cout<<it->mName<<" - "<<it->mAge<<endl;
    }

    return 0;
}
