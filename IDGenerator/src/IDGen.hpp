/*
 * IDGen.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef IDGEN_HPP_
#define IDGEN_HPP_

class IDGen
{
private:
	static int id;

public:

	IDGen();
	virtual ~IDGen();
	static int getNextID();

};

#endif /* IDGEN_HPP_ */
