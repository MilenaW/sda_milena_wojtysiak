//============================================================================
// Name        : stringMirror.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

void print(char x)
{
	cout<<x;
}

int main() {

	string word = "alamakota";
	list<char> l;

	for(size_t i=0; i<word.size(); i++)
	{
		l.push_back(word[i]);
	}



	list<char> r = l;
	reverse(r.begin(), r.end());
	for_each(l.begin(), l.end(), print);
	cout<<endl;
	for_each(r.begin(), r.end(), print);
	cout<<endl;
	l.splice(l.end(), r);
	for_each(l.begin(), l.end(), print);
	return 0;
}
