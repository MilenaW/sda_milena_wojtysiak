/*
 * Szklanka.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SZKLANKA_HPP_
#define SZKLANKA_HPP_

class Szklanka
{
private:
	static int cena;
	int pojemnosc;
public:
	Szklanka();
	Szklanka(int poj);
	virtual ~Szklanka();
	static int getCena();
	static void setCena( int cena);
	int getPojemnosc() const;
	void setPojemnosc(int pojemnosc);
};

#endif /* SZKLANKA_HPP_ */
