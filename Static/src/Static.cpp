//============================================================================
// Name        : Static.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Szklanka.hpp"

using namespace std;

int licznik()
{
	int static a;
	a++;
	return a;
}
void static fff()
{
	cout<<"cos"<<endl;
}

void static pop()
{
	fff();
}

int main()
{
fff();
pop();


	cout << "Cena bez obiektu " << Szklanka::getCena() << endl;

	Szklanka::setCena(1000);

	cout << "Cena bez obiektu po zmianie " << Szklanka::getCena() << endl;

	Szklanka szkl1;
	Szklanka szkl2;

	cout << "Cena przed 1 " << szkl1.getCena() << " 2 " << szkl2.getCena()
			<< endl;

	szkl1.setCena(10);
	cout << "Cena po 1 " << szkl1.getCena() << " 2 " << szkl2.getCena() << endl;

	Szklanka szkl3;
	cout << "Cena po 1 " << szkl1.getCena() << " 3 " << szkl3.getCena() << endl;


	return 0;
}
