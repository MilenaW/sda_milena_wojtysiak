//============================================================================
// Name        : Fabryka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Computer.hpp"
#include "AbstractFactory.hpp"
#include "AMDFactory.hpp"
#include "IntelFactory.hpp"
#include "Processor.hpp"
#include "ProcessorIntel.h"
#include "ProcessorAMD.hpp"
#include "Cooler.hpp"
#include "CoolerAMD.hpp"
#include "CoolerIntel.hpp"

using namespace std;

int main() {
	IntelFactory intelFactory;
	AMDFactory amdFactory;

	Computer intelPC("PC1", intelFactory);
	Computer amdPC("PC2", amdFactory);

	intelPC.run();
	amdPC.run();
	return 0;
}
