/*
 * Processor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESSOR_HPP_
#define PROCESSOR_HPP_

class Processor
{
public:

	Processor();
	virtual ~Processor();
	virtual void process()=0;
};

#endif /* PROCESSOR_HPP_ */
