/*
 * AMDFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef AMDFACTORY_HPP_
#define AMDFACTORY_HPP_

#include "AbstractFactory.hpp"

class AMDFactory: public AbstractFactory
{
public:
	AMDFactory();
	virtual ~AMDFactory();
	Processor * createProcessor();
	Cooler * createCooler();
};

#endif /* AMDFACTORY_HPP_ */
