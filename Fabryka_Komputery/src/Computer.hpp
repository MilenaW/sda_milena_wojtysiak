/*
 * Computer.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#include "Processor.hpp"
#include "Cooler.hpp"
#include "AbstractFactory.hpp"
#include "IntelFactory.hpp"
#include "AMDFactory.hpp"
using namespace std;

#ifndef COMPUTER_HPP_
#define COMPUTER_HPP_

class Computer
{
private:
	string mName;
	Processor* mProcessor;
	Cooler* mCooler;

public:
	Computer();
	Computer(string name, AbstractFactory& factory);
	virtual ~Computer();
	void run();
};

#endif /* COMPUTER_HPP_ */
