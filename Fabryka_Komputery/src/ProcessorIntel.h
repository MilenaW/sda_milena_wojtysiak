/*
 * ProcessorIntel.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESSORINTEL_H_
#define PROCESSORINTEL_H_

#include "Processor.hpp"

class ProcessorIntel: public Processor
{
public:
	ProcessorIntel();
	virtual ~ProcessorIntel();
	void process();
};

#endif /* PROCESSORINTEL_H_ */
