/*
 * IntelFactory.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "IntelFactory.hpp"
#include "Processor.hpp"
#include "Cooler.hpp"
#include "ProcessorIntel.h"
#include "CoolerIntel.hpp"

IntelFactory::IntelFactory()
{
	// TODO Auto-generated constructor stub

}

IntelFactory::~IntelFactory()
{
	// TODO Auto-generated destructor stub
}

Processor * IntelFactory::createProcessor()
{
	return new ProcessorIntel;
}
Cooler * IntelFactory::createCooler()
{
	return new CoolerIntel;
}
