/*
 * Computer.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "Computer.hpp"
#include <string>
#include "Processor.hpp"
#include "Cooler.hpp"
#include "AbstractFactory.hpp"
#include "IntelFactory.hpp"
#include "AMDFactory.hpp"
using namespace std;

Computer::Computer()
{
	// TODO Auto-generated constructor stub

}
Computer::Computer(string name, AbstractFactory& factory) :
		mName(name)

{
	mProcessor = factory.createProcessor();
	mCooler = factory.createCooler();
}

Computer::~Computer()
{
	delete mProcessor;
	delete mCooler;
}

void Computer::run()
{
	cout<<"computer runs"<<endl;
	cout<<mName<<endl;
	mProcessor->process();
	mCooler->cool();
}
