/*
 * ProcessorAMD.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESSORAMD_HPP_
#define PROCESSORAMD_HPP_

#include "Processor.hpp"

class ProcessorAMD: public Processor
{
public:
	ProcessorAMD();
	virtual ~ProcessorAMD();
	void process();
};

#endif /* PROCESSORAMD_HPP_ */
