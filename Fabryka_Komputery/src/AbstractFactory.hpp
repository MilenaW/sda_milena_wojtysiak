/*
 * AbstractFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "Processor.hpp"
#include "Cooler.hpp"
#ifndef ABSTRACTFACTORY_HPP_
#define ABSTRACTFACTORY_HPP_

class AbstractFactory
{
public:
	AbstractFactory();
	virtual ~AbstractFactory();
	virtual Processor * createProcessor()=0;
	virtual Cooler * createCooler()=0;
};

#endif /* ABSTRACTFACTORY_HPP_ */
