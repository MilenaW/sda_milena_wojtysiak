/*
 * Cooler.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COOLER_HPP_
#define COOLER_HPP_

class Cooler
{
public:
	Cooler();
	virtual ~Cooler();
	virtual void cool()=0;
};

#endif /* COOLER_HPP_ */
