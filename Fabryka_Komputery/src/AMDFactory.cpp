/*
 * AMDFactory.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "IntelFactory.hpp"
#include "AMDFactory.hpp"
#include "Processor.hpp"
#include "Cooler.hpp"
#include "ProcessorAMD.hpp"
#include "CoolerAMD.hpp"

AMDFactory::AMDFactory()
{
	// TODO Auto-generated constructor stub

}

AMDFactory::~AMDFactory()
{
	// TODO Auto-generated destructor stub
}

Processor * AMDFactory::createProcessor(){return new ProcessorAMD;}
Cooler * AMDFactory::createCooler(){return new CoolerAMD;}
