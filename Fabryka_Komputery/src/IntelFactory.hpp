/*
 * IntelFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef INTELFACTORY_HPP_
#define INTELFACTORY_HPP_

#include "AbstractFactory.hpp"

class IntelFactory: public AbstractFactory
{
public:
	IntelFactory();
	virtual ~IntelFactory();
	Processor * createProcessor();
	Cooler * createCooler();
};

#endif /* INTELFACTORY_HPP_ */
