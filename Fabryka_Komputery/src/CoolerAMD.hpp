/*
 * CoolerAMD.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COOLERAMD_HPP_
#define COOLERAMD_HPP_

#include "Cooler.hpp"

class CoolerAMD: public Cooler
{
public:
	CoolerAMD();
	virtual ~CoolerAMD();
	void cool();
};

#endif /* COOLERAMD_HPP_ */
