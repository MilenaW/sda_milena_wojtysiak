//============================================================================
// Name        : Klasy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include "Klasy.hpp"

using namespace std;

CKostka::CKostka(unsigned int iloscScian) :
		mIloscScian(iloscScian), mOstatniaWartosc()
{
	srand(time(NULL));
	this->losowanie();
}

void CKostka::losowanie()
{
	mOstatniaWartosc = rand() % mIloscScian + 1;
}

unsigned int CKostka::getWartosc()
{
	return mOstatniaWartosc;
}
