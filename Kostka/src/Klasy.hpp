/*
 * Klasy.hpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef KLASY_HPP_
#define KLASY_HPP_

class CKostka
{
private:
	unsigned int const mIloscScian;
	unsigned int mOstatniaWartosc;
public:
	CKostka(unsigned int iloscScian);

	void losowanie();
	unsigned int getWartosc();
};

#endif /* KLASY_HPP_ */
