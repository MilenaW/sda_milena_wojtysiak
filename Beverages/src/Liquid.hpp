/*
 * Liquid.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef LIQUID_HPP_
#define LIQUID_HPP_

class Liquid
{
protected:
	int mAmount;
public:
	Liquid();
	virtual ~Liquid();
	void virtual add(int amount)=0;
	void virtual remove(int amount)=0;
	void virtual removeAll()=0;
	int getAmount() const;
	void setAmount(int amount);
};

#endif /* LIQUID_HPP_ */
