/*
 * Coffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef COFFEE_HPP_
#define COFFEE_HPP_

#include "Liquid.hpp"

class Coffee: public Liquid
{
protected:
	int mCaffeine;
public:
	Coffee();
	Coffee(int amount, int caffeine);
	virtual ~Coffee();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* COFFEE_HPP_ */
