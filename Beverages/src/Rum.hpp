/*
 * Rum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef RUM_HPP_
#define RUM_HPP_

#include "Liquid.hpp"

class Rum: public Liquid
{
private:
	enum Colour
	{
		Light,
		Dark
	};
	Colour mColour;

public:
	Rum();
	virtual ~Rum();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* RUM_HPP_ */
