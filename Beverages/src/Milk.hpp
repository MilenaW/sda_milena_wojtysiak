/*
 * Milk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef MILK_HPP_
#define MILK_HPP_

#include "Liquid.hpp"

class Milk: public Liquid
{
private:
	float mFat;
public:
	Milk();
	Milk(int amount, float fat);
	virtual ~Milk();
	void add(int amount);
	void remove(int amount);
	void removeAll();
};

#endif /* MILK_HPP_ */
