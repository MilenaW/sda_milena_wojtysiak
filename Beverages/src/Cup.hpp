/*
 * Cup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include <iostream>
#include "Liquid.hpp"
class Cup
{
private:
	Liquid** mContent;
	int mVolume;
	int mIloscRodzajowPlynow;
public:
	Cup();
	virtual ~Cup();
	void resize();
	void add(Liquid* liquid);
	int calculateLiquidAmount();
	void takeSip(int sipAmount);
	void spill();
	void clear();
	void wypisz();


};

#endif /* CUP_HPP_ */
