//============================================================================
// Name        : Beverages.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Liquid.hpp"
#include "Rum.hpp"
#include "Coffee.hpp"
#include "Espresso.hpp"
#include "Decaff.hpp"
#include "Milk.hpp"
#include "Cup.hpp"

using namespace std;

int main() {
	Cup kubekKawyZMlekiem;

	kubekKawyZMlekiem.add(new Decaff(250));
	kubekKawyZMlekiem.add(new Milk(100,2));

	kubekKawyZMlekiem.wypisz();
	kubekKawyZMlekiem.takeSip(50);
	kubekKawyZMlekiem.wypisz();
	return 0;
}
