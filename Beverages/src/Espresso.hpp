/*
 * Espresso.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef ESPRESSO_HPP_
#define ESPRESSO_HPP_

#include "Coffee.hpp"

class Espresso: public Coffee
{
public:
	Espresso();
	Espresso(int amount);
	virtual ~Espresso();
};

#endif /* ESPRESSO_HPP_ */
