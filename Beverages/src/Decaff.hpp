/*
 * Decaff.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef DECAFF_HPP_
#define DECAFF_HPP_

#include "Coffee.hpp"

class Decaff: public Coffee
{
public:
	Decaff();
	Decaff(int amount);
	virtual ~Decaff();
};

#endif /* DECAFF_HPP_ */
