/*
 * Cup.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Cup.hpp"

Cup::Cup()
{
	mContent = 0;
	mVolume = 0;
	mIloscRodzajowPlynow=0;

}

Cup::~Cup()
{
	// TODO Auto-generated destructor stub
}

void Cup::resize()
{
	if (mIloscRodzajowPlynow == 0)
	{
		mContent = new Liquid*[++mIloscRodzajowPlynow];

	}
	else
	{
		Liquid**tmp = new Liquid*[++mIloscRodzajowPlynow];
		for (int i = 0; i < mIloscRodzajowPlynow - 1; i++)
		{
			tmp[i] = mContent[i];
		}
		delete[] mContent;
		mContent = tmp;
	}
//	mVolume = mVolume + liquid->getAmount();
}

void Cup::add(Liquid* liquid)
{
	resize();
	mContent[mIloscRodzajowPlynow-1] = liquid;//TODO poprawic bo niezgodne z RAII
}

int Cup::calculateLiquidAmount()
{
	int liquidAmount = 0;
	if (mIloscRodzajowPlynow == 0)
	{
		//Nic sie nie dzieje, nuda...
	}
	else
	{
	for (int i = 0; i < mIloscRodzajowPlynow; i++)
	{
		liquidAmount += mContent[i]->getAmount();
	}
	}
	return liquidAmount;
}

void Cup::takeSip(int sipAmount)
{
	int liquidAmount = calculateLiquidAmount();
	if (liquidAmount>0 && sipAmount>0)
	{
		if(liquidAmount<=sipAmount)
		{
			spill();
		}
		else
		{
			for (int i = 0; i < mIloscRodzajowPlynow; i++)
			{
				float currentLiquidAmount = mContent[i]->getAmount();
				float liquidRatio =currentLiquidAmount /(float)liquidAmount;
				mContent[i]->remove(liquidRatio*sipAmount);
			}
		}
	}
	else
	{
		//nuda...nic sie nie dzieje...
	}


}
void Cup::spill()
{
	for (int i = 0; i < mIloscRodzajowPlynow - 1; i++)
		{
			mContent[i]->removeAll();
		}
	clear();
}

void Cup::clear()
{
	for (int i = 0; i < mIloscRodzajowPlynow - 1; i++)
		{
			delete mContent[i];
		}
	delete [] mContent;
	mContent = 0;
	mIloscRodzajowPlynow = 0;
}
void Cup::wypisz()
{
	for (int i = 0; i < mIloscRodzajowPlynow ; i++)
		{
			std::cout<<mContent[i]->getAmount()<<std::endl;
		}
}
