//============================================================================
// Name        : 2017.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <cstdlib>

//zad 1
//int main() {
//	std::cout << "*******************************" << std::endl;
//	std::cout << " C++ jest super!" << std::endl;
//	std::cout << "*******************************" << std::endl;
//	return 0;
//}

//zad 2
//
//void funkcja (double a, double b, double c)
//{
//	double delta, rownanie, rownanie2;
//
//	delta  = b*b - 4*a*c;
//	if (a==0)
//	{
//		std::cout<<"Zweryfikuj dane"<<std::endl;
//	}
//	else if (delta<0)
//	{
//		std::cout<<"Pierwiastek z 0? serio?"<<std::endl;
//	}
//	else if (delta>=0)
//	{
//		rownanie = (-b + sqrt(delta))/(2*a);
//		rownanie2 = (-b - sqrt(delta))/(2*a);
//
//		std::cout << "wynik:" << rownanie <<std::endl;
//		std::cout << "wynik2:" << rownanie2 <<std::endl;
//	}
//}
//
//int main() {
//
//	std::cout << "a" << std::endl;
//	funkcja (1,1,0);
//	std::cout << "*******************************" << std::endl;
//	std::cout << "b" << std::endl;
//	funkcja (3,-6,3);
//	std::cout << "*******************************" << std::endl;
//	std::cout << "c" << std::endl;
//	funkcja (0.5,2,-4);
//	std::cout << "*******************************" << std::endl;
//	std::cout << "d" << std::endl;
//	funkcja (0,1,1);
//	std::cout << "*******************************" << std::endl;
//	std::cout << "e" << std::endl;
//	funkcja (1,1,1);
//	std::cout << "*******************************" << std::endl;
//
//	return 0;
//}
int NWD(int m, int n)
{

	if (m == 0)
	{
		return n;
	}
	else
	{

		int r = n % m;
		return NWD(r, m);

	}

}

int main()
{

	std::cout << NWD(12, 8) << std::endl;

	return 0;
}

