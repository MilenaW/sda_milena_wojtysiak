//============================================================================
// Name        : struktury.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

struct Czlowiek
{
	int dzienUr;
	int miesiacUr;
	int rokUr;

	Czlowiek(int dzien,int miesiac,int rok)
	:dzienUr(dzien)
	,miesiacUr(miesiac)
	,rokUr(rok)
	{

	}
	int podajWiek (int rok)
	{
		return (rok - rokUr<0) ? 0 : rok - rokUr;
	}
};
struct Pracownik : Czlowiek
{
	float pensja;
	int staz;
	Pracownik(int dzien,int miesiac,int rok, int sta)
	:Czlowiek(dzien, miesiac,rok)
	,staz(sta)
	{
		wyliczPensje();
	}

	void wyliczPensje()
	{
		pensja = staz*0.05;
	}

};

class Student : Czlowiek
{
private:
	int nrIndeksu;
public:
	Student(int dzien,int miesiac,int rok, int nr)
	:Czlowiek(dzien, miesiac,rok)
	,nrIndeksu(nr)
	{

	}
	void bawSie()
	{
		cout<<"sie bawie"<<endl;
	}
};

struct UserInputData
{
	int iloscLiczb;
	int min;
	int max;
	bool czyRosnaca;

};

int main() {

	Czlowiek cz(11,11,1911);

	cout << cz.podajWiek(2000) << endl;

	Pracownik pracus (33,33,1900,999);

	cout << pracus.podajWiek(2000) << endl;

	pracus.dzienUr = 22;
	pracus.miesiacUr = 2;
	pracus.rokUr = 2222;

	cout << pracus.podajWiek(2000) << endl;
	return 0;
}
