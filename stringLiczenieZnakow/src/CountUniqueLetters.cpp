/*
 * CountUniqueLetters.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "CountUniqueLetters.hpp"

void print(char x)
{
	cout<<x<<" ";
}

CountUniqueLetters::CountUniqueLetters(string text)
: mText(text)
{
	stringToVec();
	printVector();
	countLetters();
	countWords();

}

CountUniqueLetters::~CountUniqueLetters()
{
	// TODO Auto-generated destructor stub
}

void CountUniqueLetters::stringToVec()
{
	for(size_t i= 0; i<mText.size(); i++ )
	{
		mVecText.push_back(mText[i]);
	}
}

void CountUniqueLetters::countLetters()
{
	vector<char>tmp = mVecText;
	sort(tmp.begin(), tmp.end());
	vector<char>::iterator it = unique (tmp.begin(), tmp.end());


	for(vector<char>::iterator itt = tmp.begin(); itt!=it; itt++)
	{
		mTextMap.insert(pair<char,int>(*itt,0));
	}

	for (map<char, int>::iterator itm = mTextMap.begin(); itm !=mTextMap.end(); itm++)
	{
		for(vector<char>::iterator itt = mVecText.begin(); itt!=mVecText.end(); itt++)
		{
			if (itm->first==*itt)
			{
				itm->second++;
			}
		}
	}

	printMap();

}

void CountUniqueLetters::printVector()
{
	for_each(mVecText.begin(), mVecText.end(), print);
	cout<<endl;
}

void CountUniqueLetters::printMap()
{
	for (map<char, int>::iterator itf=mTextMap.begin(); itf!=mTextMap.end(); ++itf)
	{
		cout << itf->first << " => " << itf->second << '\n';
	}
}

void CountUniqueLetters::countWords()
{
	cout<<"text is composed of "<<(mTextMap.find(' ')->second)+1<<" words"<<endl;
}
