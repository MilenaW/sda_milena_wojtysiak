/*
 * CountUniqueLetters.hpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef COUNTUNIQUELETTERS_HPP_
#define COUNTUNIQUELETTERS_HPP_
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;

class CountUniqueLetters
{
private:
	map<string, map<char,int> > mCacheMap;

public:
	CountUniqueLetters(string text);
	virtual ~CountUniqueLetters();
	string mText;
	vector<char> mVecText;
	map<char,int> mTextMap;
	void stringToVec();
	void countLetters();
	void printVector();
	void printMap();
	void countWords();

};

#endif /* COUNTUNIQUELETTERS_HPP_ */
