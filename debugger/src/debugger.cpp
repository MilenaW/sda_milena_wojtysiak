#include <iostream>
#include <cstdlib>    // std::srand, std::rand
#include <ctime>      // std::time
#include <cmath>      // std::floor
#include <algorithm>  // std::sort

// do zadania 4 i 7
void wczytajTablice(int* tablica, const int rozmiar)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		std::cout << "Podaj liczb� " << i + 1 << ": "<<std::endl;
		std::cin >> tablica[i];
	}
}

// do zadania 4
void policzSrednia(const int* tablica, const int rozmiar, double* wynik)
{
	*wynik = 0.0; // litera� typu double
	for (int i = 0; i < rozmiar; ++i)
	{
		*wynik += tablica[i];
	}
	*wynik /= rozmiar; // to dzielenie dzia�a, poniewa� *wynik jest typu double
}

// do zadania 5
void sumujDoOporu(const int limit, int& suma, int& liczebnosc)
{
	suma = 0;
	liczebnosc = 0;
	for (int i = 0; suma < limit; ++i, ++liczebnosc)
	{
		suma += i;
	}
}

// do zadania 6
void wypelnijLosowo(int* tablica, const int rozmiar, const int maks)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		tablica[i] = rand() % maks;
	}
}

// do zadania 6
void zlicz(const int* tablica, const int rozmiar, int* wystapienia)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		// sztuczka dzia�a, poniewa� zakres warto�ci zapisanych w tablicy
		// wej�ciowej zgadza si� z rozmiarem tablicy wyst�pie�
		wystapienia[tablica[i]] += 1;
	}

// wersja alternatywna, bez sztuczek
//	for (int w = 0; w < 21; ++w)
//	{
//		wystapienia[w] = 0;
//		for (int l = 0; l < 100; ++l)
//		{
//			if (tablica[l] == w)
//			{
//				wystapienia[w] += 1;
//			}
//		}
//	}

// wersja inna
//	for (int w = 0; w < 21; ++w)
//	{
//		int licznik = 0;
//		for (int l = 0; l < 100; ++l)
//		{
//			if (tablica[l] == w)
//			{
//				licznik += 1;
//			}
//		}
//		std::cout << licznik << " wyst�pie� liczby " << w << std::endl;
//	}
}

// do zadania 6
void pokazWystapienia(const int* wystapienia, const int rozmiar)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		std::cout << "Liczba " << i << " wyst�pi�a " << wystapienia[i] << " razy" << std::endl;
	}
}

// do zadania 7
int indeksPercentylu(const double percentyl, const int maksIndeks)
{
	double przyblizonyIndeks = maksIndeks * percentyl;
	double indeksAleWciazWDouble = std::floor(przyblizonyIndeks);

	// ten w�a�ciwy indeks
	// static_cast<int>() - rzutowanie typu - jaka� liczba na typ int
	// musz� mie� typ int, �eby indekstowa� tablic�
	return static_cast<int>(indeksAleWciazWDouble);
}

// do zadania 7
void znajdzPercentyle(int* wartosci, const int ilosc, int& percentyl50, int& percentyl95)
{
	std::sort(wartosci, wartosci + ilosc);

	const int indeks50 = indeksPercentylu(0.5, ilosc - 1);
	const int indeks95 = indeksPercentylu(0.95, ilosc - 1);

	percentyl50 = wartosci[indeks50];
	percentyl95 = wartosci[indeks95];
}

// do zadania 8
void wylosujISumujCyfry(int& liczba, int& sumaCyfr)
{
	liczba = rand();
	sumaCyfr = 0;

	int robocza = liczba;
	while (robocza > 0)
	{
		int jednosci = robocza % 10;
		sumaCyfr += jednosci;
		robocza /= 10;
	}
}

int main()
{
	std::cout << "--- Zadanie 4 ----------------------" << std::endl;
	std::cout << "Podaj ilo�� liczb: " << std::endl;
	int rozmiar;
	std::cin >> rozmiar;

	int* liczby = new int[rozmiar];

	wczytajTablice(liczby, rozmiar);
	double srednia;
	policzSrednia(liczby, rozmiar, &srednia);
	delete[] liczby;

	std::cout << "Wynik: " << srednia << std::endl;

	std::cout << "--- Zadanie 5 ----------------------" << std::endl;
	std::cout << "Podaj limit: " <<std::endl;
	int limit;
	std::cin >> limit;

	int suma, liczebnosc;
	sumujDoOporu(limit, suma, liczebnosc);

	std::cout << "Suma wynios�a " << suma << ",\nna co z�o�y�o si� " << liczebnosc << " liczb." << std::endl;

	std::cout << "--- Zadanie 6 ----------------------" << std::endl;
	srand(time(NULL));
//	srand(1); -- zawsze te same "losowe" liczby

	const int ZAKRES_WARTOSCI = 21;

	int* losowe = new int[100];
	wypelnijLosowo(losowe, 100, ZAKRES_WARTOSCI);

	int* wystapienia = new int[ZAKRES_WARTOSCI];
	zlicz(losowe, 100, wystapienia);
	delete[] losowe;

	pokazWystapienia(wystapienia, ZAKRES_WARTOSCI);
	delete[] wystapienia;
//	std::cout << "Timestamp: " << time(NULL) << std::endl;

	std::cout << "--- Zadanie 7 ----------------------" << std::endl;
	std::cout << "Podaj ilo�� liczb: " <<std::endl;
	int ilosc;
	std::cin >> ilosc;

	int* wartosci = new int[ilosc];

	wczytajTablice(wartosci, ilosc);

	int percentyl50, percentyl95;
	znajdzPercentyle(wartosci, ilosc, percentyl50, percentyl95);
	delete[] wartosci;

	std::cout << "50. percentyl wynosi: " << percentyl50 << ",\na 95. percentyl wynosi: " << percentyl95 << std::endl;

	std::cout << "--- Zadanie 8 ----------------------" << std::endl;
//	srand(time(NULL));
	int liczba, sumaCyfr;
	wylosujISumujCyfry(liczba, sumaCyfr);

	std::cout << "Wylosowano liczb�: " << liczba << ",\na sume jej cyfr to: " << sumaCyfr << std::endl;

	return 0;
}
