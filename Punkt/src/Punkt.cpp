//============================================================================
// Name        : Punkt.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <math.h>
using namespace std;
enum Color
{
	red,
	blue,
	green
};


class Punkt
{
protected:
	int mX;
	int mY;
public:
	Punkt()
	{cout<<"Tworzy sie punkt...\n";
	}

	Punkt(int x, int y)
	: mX(x)
	, mY(y)
	{cout<<"Tworzy sie punkt z argumentami...\n";
	}
int getX() const
	{
		return mX;
	}

	void setX(int x)
	{
		mX = x;
	}

	int getY() const
	{
		return mY;
	}

	void setY(int y)
	{
		mY = y;
	}
	void wypisz()
	{
		cout<<"["<<getX()<<","<<getY()<<"]"<<endl;
	}


	void przesun(int x,int y)
	{
		przesunX(x);
		przesunY(y);
	}
	void przesun(Punkt punkt)
	{
		przesun(punkt.getX(),punkt.getY());
	}

	void przesunX(int x)
	{
		mX+=x;
	}
	void przesunY(int y)
	{
		mY+=y;
	}
	int obliczOdleglosc(int x, int y)
	{
		return sqrt(pow((mX-x),2)+pow((mY-y),2));
	}
	int obliczOdleglosc(Punkt punkt)
	{
		return obliczOdleglosc(punkt.getX(),punkt.getY());
	}
};

class KolorowyPunkt: public Punkt
{
private:

	int mX;
	int mY;
	Color mColor;
public:


	KolorowyPunkt(int x, int y, Color color)
: mX(x)
, mY(y)
, mColor(color)
	{
		cout<<"Tworzy sie kolorowy punkt z argumentami...\n";
	}



	Color getColor() const
	{


		return mColor;
	}

	void setColor(Color color)
	{
		mColor = color;
	}


	int getX() const
	{
		return mX;
	}

	void setX(int x)
	{
		mX = x;
	}

	int getY() const
	{
		return mY;
	}

	void setY(int y)
	{
		mY = y;
	}

	void wypiszKolor()
	{
		Color kolor = getColor();
		switch (kolor)
		{
		case red:
			cout<<"red\n";
			break;
		case blue:
			cout<<"blue\n";
			break;
		case green:
			cout<<"green\n";
			break;
		default:
			cout<<"unknown\n";
			break;
		}
	}
	void wypisz()
		{
			cout<<"["<<getX()<<","<<getY()<<"] kolor ";
			wypiszKolor();
			cout<<endl;
		}
};

int main()
{
	Punkt punkt (1,1);
	Punkt punkt2 (2,2);
//	punkt.wypisz();
//	punkt.przesun(1,1);
//	punkt.wypisz();
//	punkt.przesun(punkt2);
	punkt.wypisz();
	punkt2.wypisz();
	cout<<punkt.obliczOdleglosc(punkt2)<<endl;
	KolorowyPunkt kolorowy (9,9,red);
	kolorowy.wypisz();

	return 0;
}
