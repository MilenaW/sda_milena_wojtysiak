//============================================================================
// Name        : pierwiastkiZZakresu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
using namespace std;

void print(float x)
{
	cout<<x<<" ";
}

void square(float& x)
{
	x=sqrt(x);
}

int main() {

	vector <float> range;

	float min = 3;
	float max = 10;

	for(size_t i= min; i<=max; i++ )
	{
		range.push_back(i);
	}

	for_each(range.begin(), range.end(), print);
	cout<<endl;

	for_each(range.begin(), range.end(), square);
	for_each(range.begin(), range.end(), print);
	cout<<endl;


	return 0;
}
