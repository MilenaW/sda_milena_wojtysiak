//============================================================================
// Name        : Czas.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

class Czas
{
protected:
	unsigned int mGod;
	unsigned int mMin;
	unsigned int mSek;
public:
	unsigned int getGod() const
	{
		return mGod;
	}

	void setGod(unsigned int god)
	{
		if (god<0) mGod = 0;
		else if (god>24) mGod = 24;
		else mGod = god;
	}

	unsigned int getMin() const
	{
		return mMin;
	}

	void setMin(unsigned int min)
	{
		if (min<0) mMin = 0;
		else if (min>59) mMin = 59;
		else mMin = min;
	}

	unsigned int getSek() const
	{
		return mSek;
	}

	void setSek(unsigned int sek)
	{
		if (sek<0) mSek = 0;
		else if (sek>59) mSek = 59;
		else mSek = sek;
	}
	Czas ()
	{
		setGod(0);
		setMin(0);
		setSek(0);
	}
	Czas (unsigned int god, unsigned int min, unsigned int sek )
	{
		setGod(god);
		setMin(min);
		setSek(sek);
	}
	void wypiszCzas()
	{
		cout<<getGod()<<":"<<getMin()<<":"<<getSek()<<endl;
	}
	void przesunGod (int g)
	{
		mGod+=g;
	}

	void przesunMin (int m)
	{
		mGod+=m/60;
		mMin+=m%60;
	}

	void przesunSek (int s)
	{
		mGod+=s/3600;
		mMin+=(s%3600)/60;
		mSek+=(s%3600)%60;;
	}

	Czas roznicaCzasu(Czas czas1)
	{
		unsigned int roznicaGod=czas1.getGod()-getGod();
		unsigned int roznicaMin=czas1.getMin()-getMin();
		unsigned int roznicaSek=czas1.getSek()-getSek();
		Czas czas (roznicaGod,roznicaMin,roznicaSek);
		return czas;
	}

};




//int main() {
//	Czas czas(20,32,15);
//	Czas czas2(19,30,10);
//	czas.wypiszCzas();
//	czas2.wypiszCzas();
//	Czas roznica = czas2.roznicaCzasu(czas);
//	roznica.wypiszCzas();
//	czas.przesunSek(3605);
//	czas.wypiszCzas();
//
//
//	return 0;
//}
