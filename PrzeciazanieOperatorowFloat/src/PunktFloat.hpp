/*
 * PunktFloat.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKTFLOAT_HPP_
#define PUNKTFLOAT_HPP_
#include <iostream>

class PunktFloat
{

private:
	float mX;
	float mY;

public:
	PunktFloat();
	PunktFloat(float x, float y);
	PunktFloat(const PunktFloat& pkt);
	virtual ~PunktFloat();
	PunktFloat operator+ (const float plus) const;
	PunktFloat operator+ (PunktFloat &pkt) const;
	PunktFloat operator* (const float multi) const;
	PunktFloat operator*(PunktFloat &pkt) const;
	float getX() const;
	void setX(float x);
	float getY() const;
	void setY(float y);
	void wypisz();
	PunktFloat& operator++();
	PunktFloat operator++(int);
};

PunktFloat  operator +( const float plus, PunktFloat pkt );
PunktFloat  operator *( const float multi, PunktFloat pkt );

#endif /* PUNKTFLOAT_HPP_ */
