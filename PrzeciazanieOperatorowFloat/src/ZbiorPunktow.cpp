/*
 * ZbiorPunktow.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "ZbiorPunktow.hpp"

ZbiorPunktow::ZbiorPunktow()
{
	mSize = 0;
	mPtr = NULL;
}
void ZbiorPunktow::resize()
{
	if (mSize == 0)
	{
		mPtr = new PunktFloat*[++mSize];
	}
	else
	{
		PunktFloat**tmp = new PunktFloat*[++mSize];
		for (size_t i = 0; i < mSize - 1; i++)
		{
			tmp[i] = mPtr[i];
		}
		delete[] mPtr;
		mPtr = tmp;
	}
}

size_t ZbiorPunktow::getSize() const
{
	return mSize;
}

void ZbiorPunktow::wypisz()
{
	for (size_t i = 0; i < this->getSize(); i++)
	{
		std::cout << "P" << i + 1 << "{";
		mPtr[i]->wypisz();
		std::cout << "}";
	}
	std::cout << std::endl;
}

void ZbiorPunktow::operator+(const PunktFloat& pkt)
{
	resize();
	mPtr[mSize - 1] = new PunktFloat(pkt);
}

void ZbiorPunktow::operator+(const ZbiorPunktow& zbior)
{
	for (size_t i = 0; i < zbior.getSize(); i++)
	{
		*this + zbior[i];
	}
}
PunktFloat ZbiorPunktow::operator[](size_t idx) const
{
	if (idx < mSize)
	{
		return PunktFloat(*mPtr[idx]);
	}
	else
	{
		return PunktFloat(0, 0);
	}
}

ZbiorPunktow::operator bool()
{
	return (this->getSize() != 0);
}

ZbiorPunktow::~ZbiorPunktow()
{
	if (mSize != 0)
	{
		for (size_t i = 0; i < mSize; i++)
		{
			delete mPtr[i];
		}

		delete[] mPtr;
		mSize = 0;
	}
}

