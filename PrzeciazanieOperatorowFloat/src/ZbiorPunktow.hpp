/*
 * ZbiorPunktow.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIORPUNKTOW_HPP_
#define ZBIORPUNKTOW_HPP_
#include <iostream>
#include "PunktFloat.hpp"

class ZbiorPunktow
{
private:
	size_t mSize;
	PunktFloat** mPtr;
	void resize();

public:
	ZbiorPunktow();
	virtual ~ZbiorPunktow();
	void operator+(const PunktFloat& pkt);
	void wypisz();
	size_t getSize() const;
	void operator+(const ZbiorPunktow& zbior);
	PunktFloat operator[](size_t idx) const;
	operator bool();
};

#endif /* ZBIORPUNKTOW_HPP_ */
