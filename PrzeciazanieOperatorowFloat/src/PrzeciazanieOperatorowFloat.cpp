//============================================================================
// Name        : PrzeciazanieOperatorowFloat.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "PunktFloat.hpp"
#include "ZbiorPunktow.hpp"

using namespace std;

int main()
{
	PunktFloat p1(1, 1);
	PunktFloat p2(2, 2);
	++p2;
	p1.wypisz();
	p2.wypisz();

	PunktFloat p3 = p1 + p2;
	p3.wypisz();

	p3 = p1 + 5;
	p3.wypisz();

	p3 = 1 + p2;
	p3.wypisz();

	p3 = 5 * p2;
	p3.wypisz();

	ZbiorPunktow z1;

//	if (z1)
//	{
//		cout << "prawidlowy" << endl;
//	}
//	else
//	{
//		cout << "nieprawidlowy" << endl;
//	}

	z1 + p1;
	z1 + p2;
	z1 + p3;
//	if (z1)
//	{
//		cout << "prawidlowy" << endl;
//	}
//	else
//	{
//		cout << "nieprawidlowy" << endl;
//	}
	z1.wypisz();

	ZbiorPunktow z2;
	z2 + p3;
	z2.wypisz();
	z1 + z2;
	z1.wypisz();

	return 0;
}
