/*
 * PunktFloat.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "PunktFloat.hpp"

PunktFloat::PunktFloat() :
		mX(0), mY(0)
{

}

PunktFloat::PunktFloat(float x, float y) :
		mX(x), mY(y)
{

}
PunktFloat::PunktFloat(const PunktFloat& pkt) :
		mX(pkt.getX()), mY(pkt.getY())
{

}

float PunktFloat::getX() const
{
	return mX;
}

void PunktFloat::setX(float x)
{
	mX = x;
}

float PunktFloat::getY() const
{
	return mY;
}

void PunktFloat::setY(float y)
{
	mY = y;
}

PunktFloat PunktFloat::operator+(const float plus) const
{
	PunktFloat wynik(0, 0);
	wynik.setX(mX + plus);
	wynik.setY(mY + plus);
	return wynik;
}

PunktFloat PunktFloat::operator+(PunktFloat &pkt) const
{
	PunktFloat wynik(0, 0);
	wynik.setX(pkt.getX() + this->getX());
	wynik.setY(pkt.getY() + this->getY());
	return wynik;
}
PunktFloat PunktFloat::operator*(const float multi) const
{
	PunktFloat wynik(0, 0);
	wynik.setX(mX * multi);
	wynik.setY(mY * multi);
	return wynik;
}
PunktFloat PunktFloat::operator*(PunktFloat &pkt) const
{
	PunktFloat wynik(0, 0);
	wynik.setX(pkt.getX() * this->getX());
	wynik.setY(pkt.getY() * this->getY());
	return wynik;
}

void PunktFloat::wypisz()
{
	std::cout << "X= " << mX << " Y= " << mY << std::endl;
}

PunktFloat& PunktFloat::operator++()
{
this->setX(mX+1);
this->setY(mY+1);

return *this;
}

PunktFloat PunktFloat::operator++(int)
		{
	PunktFloat tmp(*this);
	this->operator++();
	return tmp;
		}
PunktFloat::~PunktFloat()
{

}

PunktFloat operator +(const float plus, PunktFloat pkt)
{
return pkt + plus;
}
PunktFloat operator *(const float multi, PunktFloat pkt)
{
return pkt * multi;
}
