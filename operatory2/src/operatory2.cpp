//============================================================================
// Name        : operatory2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
	int size = 10;
	int tab[size] =
	{ 1, 2, 3, 84, 5, 6, 7, 8, 9, 10 };
	int MAX = tab[0];
	for (int i = 1; i < size; i++)
	{
		MAX = (MAX > tab[i]) ? MAX : tab[i];
	}
	cout << MAX << endl;
	return 0;
}
