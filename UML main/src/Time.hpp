/*
 * Time.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#ifndef TIME_HPP_
#define TIME_HPP_

class Time
{
private:
	int mHour;
	int mMinute;
	int mSecond;

public:
	Time();
	Time (int hour, int minute, int second);
	virtual ~Time();
	int getHour() const;
	void setHour(int hour);
	int getMinute() const;
	void setMinute(int minute);
	int getSecond() const;
	void setSecond(int second);
	void setTime (int hour, int minute, int second);
	string toString();
	Time nextSecond();
	Time previousSecond();


};

class Triangle
{
private:
	MyPoint v1;
	MyPoint v2;
	MyPoint v3;
public:
	MyTriangle (int x1, int y1, int x2, int y2,int x3, int y3)

};




#endif /* TIME_HPP_ */
