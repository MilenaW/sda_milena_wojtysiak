/*
 * VW.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef VW_HPP_
#define VW_HPP_
#include <iostream>
#include <string>
#include "Sam.hpp"

class VW: public Samochod
{
public:
	void kolor();
	void marka();
	void pojemnoscSilnika();
	VW (string kolor, string marka, int pojemnosc);
~VW();
};







#endif /* VW_HPP_ */
