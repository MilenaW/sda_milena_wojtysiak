/*
 * Sam.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef SAM_HPP_
#define SAM_HPP_
#include <iostream>
#include <string>

using namespace std;

class Samochod
{
public:
	virtual void kolor()=0;
	virtual void marka()=0;
	virtual void pojemnoscSilnika()=0;
	Samochod (string kolor, string marka, int pojemnosc);
	virtual ~Samochod();
protected:
	string mKolor;
	string mMarka;
	int mPojemnosc;
};



#endif /* SAM_HPP_ */
