/*
 * Audi.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef AUDI_HPP_
#define AUDI_HPP_
#include <iostream>
#include <string>
#include "Sam.hpp"

class Audi: public Samochod
{
public:
	void kolor();
	void marka();
	void pojemnoscSilnika();
	Audi (string kolor, string marka, int pojemnosc);
	~Audi();
};



#endif /* AUDI_HPP_ */
