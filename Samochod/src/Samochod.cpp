//============================================================================
// Name        : Samochod.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Sam.hpp"
#include "Audi.hpp"
#include "VW.hpp"
using namespace std;

int main() {
	Samochod *tablica[2];
	tablica[0]= new Audi ("czerwony", "A6", 20);
	tablica[1]= new VW ("zielony", "Golf", 50);


	for (int i = 0; i<2; ++i)
	{
		tablica[i]->kolor();
		tablica[i]->marka();
		tablica[i]->pojemnoscSilnika();
	}
	for (int i = 0; i<2; ++i)
	{
		delete tablica[i];
	}
	return 0;
}
