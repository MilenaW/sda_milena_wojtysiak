/*
 * CalculatorTest.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "ASCIICalcClass.hpp"

TEST(CalculatorTest, CalcTest)
{
	ASCIICalcClass c("abc");
	EXPECT_EQ(294, c.ASCIISum());
}


int main(int argc, char **argv)
{
::testing::InitGoogleTest(&argc, argv);
return RUN_ALL_TESTS();
}

