/*
 * ASCIICalcClass.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef ASCIICALCCLASS_HPP_
#define ASCIICALCCLASS_HPP_
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class ASCIICalcClass
{
private:
	string mS;

public:
	ASCIICalcClass();
	ASCIICalcClass(string s);
	virtual ~ASCIICalcClass();
	int ASCIISum();
};

#endif /* ASCIICALCCLASS_HPP_ */
