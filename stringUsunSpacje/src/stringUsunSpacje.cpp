//============================================================================
// Name        : stringUsunSpacje.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void print(char x)
{
	cout<<x<<" ";
}

int main() {

	vector <char> w;
	string word = "ala ma kota";

	for(size_t i= 0; i<word.size(); i++ )
	{
		w.push_back(word[i]);
	}

	for_each(w.begin(), w.end(), print);
	cout<<endl;

	for (vector<char>::iterator it = w.begin(); it!=w.end();++it)
	{
		if(*it==' ')
		{
			w.erase(it);
		}
	}

	for_each(w.begin(), w.end(), print);
	cout<<endl;


	return 0;
}
