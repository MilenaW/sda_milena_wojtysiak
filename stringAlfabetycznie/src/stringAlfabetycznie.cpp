//============================================================================
// Name        : stringAlfabetycznie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <algorithm>
using namespace std;

void print(char x)
{
	cout<<x<<" ";
}

bool sortAlphabet (const char& first, const char& second)
{
	return first<second;
}

int main() {

list <char> l;
string word = "edacb";

for(size_t i= 0; i<word.size(); i++ )
{
	l.push_back(word[i]);
}

for_each(l.begin(), l.end(), print);

l.sort(sortAlphabet);

for_each(l.begin(), l.end(), print);

	return 0;
}
