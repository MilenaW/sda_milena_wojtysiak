#include <iostream>

void znajdzNieparzyste(int limit, int* wynik, int& iloscWynikow)
{
	iloscWynikow = 0;
//	for (int i = 0, iTablicy = 0; i < limit; ++i)
//	{
//		if (i % 2 == 1)
//		{
//			wynik[iTablicy] = i;
//			iTablicy += 1;
//			iloscWynikow += 1;
//		}
//	}

	for (int i = 1, iTablicy = 0; i < limit; i += 2, iTablicy += 1)
	{
			wynik[iTablicy] = i;
			iloscWynikow += 1;
	}
}

int main()
{
	std::cout << "Podaj granic�: " << std::flush;
	int granica;
	std::cin >> granica;

//	int* nieparzyste = new int[granica];
	// wiem, �e liczb nieparzystych jest 2x mniej, ni� wynosi granica
	int* nieparzyste = new int[granica / 2];
	int ileWyszlo;
	znajdzNieparzyste(granica, nieparzyste, ileWyszlo);

	std::cout << "Liczby nieparzyste (" << ileWyszlo << "):" << std::endl;
	for (int i = 0; i < ileWyszlo; ++i)
	{
		std::cout << nieparzyste[i] << ' ';
	}

	delete[] nieparzyste;
	return 0;
}
